---
layout: page
sequence: 12
title: Datenschutzerklärung
toctitle: Datenschutzerklärung
description: Eine echte Datenschutzerklärung
keywords: Datenschutz, wir speichern nicht
Time-stamp: "2018-02-18 17:34:32"
permalink: /Datenschutz.html
---

Im Gegensatz zu den oft üblichen Datenschatzerklärungen ist dies eine
echte Datenschutzerklärung.  Sie ist so kurz und einfach wie überhaupt
möglich:

> Wir speichern nicht.

Etwas länger:

> Für den Web-Auftritt
> [https://www.informationelle-selbstbestimmung-im-internet.de/](https://www.informationelle-selbstbestimmung-im-internet.de/)
> werden *weder* personenbezogene *noch* personenbeziehbare Daten erhoben.

Darüber hinaus werden auch keine „pseudonymisierten“ Daten erfasst.
IP-Adressen werden nicht protokolliert (Logging des Web-Servers ist
beim Web-Hoster, [schokokeks.org](https://schokokeks.org/),
deaktiviert), es werden keine Cookies gesetzt, und es werden weder
Werbung noch Social-Media-Plugins noch sonstige
Tracking-Funktionalität von Dritten eingebunden.  JavaScript wird gar
nicht verwendet.  Inhalte werden ausschließlich verschlüsselt
ausgeliefert (mit der Wertung A+ von
[Observatory by Mozilla](https://observatory.mozilla.org/analyze.html?host=www.informationelle-selbstbestimmung-im-internet.de)
und [Qualys SSL Labs](https://www.ssllabs.com/ssltest/analyze?d=www.informationelle-selbstbestimmung-im-internet.de)).

{% include labels.md %}
