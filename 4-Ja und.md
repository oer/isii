---
layout: page
sequence: 4
title: Ja … Und? Was geht mich Alice an?
toctitle: Ja … Und?
description: Warum informationelle Selbstbestimmung alle angeht.
keywords: Informationelle Selbstbestimmung, Privatsphäre, Überwachung, Orwell, Kafka, Zuboff, Überwachungskapitalismus, Prognose, Nudging, Terroranschläge, Terrorbekämpfung
Time-stamp: "2019-07-24 14:19:02"
permalink: /Ja_und.html
---

Lesen Sie den Roman [*Little Brother* von Cory
Doctorow](https://de.wikipedia.org/wiki/Little_Brother_%28Roman%29),
falls Sie nicht überzeugt sind, dass Überwachung im Internet, im
Internet der Dinge und in der Welt im Allgemeinen, auch und gerade unter
dem Vorwand der Terrorbekämpfung, eine wahnwitzige Idee ist. Lesen Sie
den Roman [*ZERO* von Marc
Elsberg](https://de.wikipedia.org/wiki/Zero_%E2%80%93_Sie_wissen,_was_du_tust),
wenn Sie wissen wollen, was Internetkonzerne mit unseren Daten machen
und wie sie uns zu steuern versuchen. [Weiter
unten][leseempfehlungen] komme ich auf diese
Buchempfehlungen zurück.

Informationelle Selbstbestimmung und Privatsphäre sind abstrakte und
komplexe Konzepte, deren Wert schwierig zu bemessen ist. Dies zeigt sich
etwa am beliebten Nothing-to-Hide-Argument, das besagt, dass Datenschutz
lediglich Täterschutz sei bzw. dass jemand, der nichts zu verbergen
habe, auch nichts gegen die automatisierte Sammlung und Analyse
personenbezogener Daten einwenden könne. Eine differenzierte Diskussion
dieses Arguments liefert Jura-Professor Daniel Solove in seinem 2007
veröffentlichten Aufsatz
[*“I’ve Got Nothing to Hide” and Other Misunderstandings of Privacy*](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=998565).

Insbesondere weist Solove darauf hin, dass das Nothing-to-Hide-Argument
nicht die Fülle an Problemen berücksichtigt, die aus einem Mangel an
Privacy resultieren können. (Es gibt im Englischen keine gängige
Übersetzung für den etwas sperrigen, aber dennoch starken deutschen
Begriff „informationelle Selbstbestimmung“; „Privacy“ bei Solove umfasst
sowohl Privatsphäre als auch informationelle Selbstbestimmung.)
Besonders überzeugend erscheint mir seine bereits ältere Unterscheidung
*orwellscher* und *kafkaesker* Probleme. Orwellsch sind in Anlehnung
an den Roman [*1984* von George
Orwell](https://de.wikipedia.org/wiki/1984_%28Roman%29) diejenigen
Probleme, die aus Überwachung resultieren, während die kafkaesken in
Anlehnung an den Roman [*Der Prozess* von
Franz Kafka](https://de.wikipedia.org/wiki/Der_Process) Hilflosigkeit
und Verwundbarkeit mit sich bringen, die aus fehlender Transparenz und
Kontrolle resultieren. Ich empfehle, beide Romane zu lesen und erläutere
im Folgenden, worum es geht.

## <a name="orwell"></a> Orwellsche Bedrohungen der Privatsphäre

Die orwellschen Bedrohungen für Privatsphäre sind diejenigen, die im
Zusammenhang mit Datenschutz wohl eher im Blickpunkt stehen, und sie
betreffen die Unterdrückung unerwünschten oder abweichenden Verhaltens
durch Überwachung und Bespitzelung. Während sofort einsichtig sein
dürfte, dass Überwachung unter undemokratischen Regimes, in denen
oppositionelle Meinungsäußerung zu Folter und Tod führt, unerwünschte
freie Meinungsäußerungen einschränken wird, sind derartige Effekte in
„gesunden“ Demokratien weniger offensichtlich. Dennoch sind
beispielsweise das deutsche Verfassungsgericht und der US Supreme Court
einig, dass durch Überwachung die individuelle Entfaltung,
Handlungsfähigkeit und Mitwirkungsfähigkeit eingeschränkt werden und
damit der Reichtum einer Demokratie gefährdet wird. (Im Englischen gibt
es den schönen Begriff „[chilling
effects](https://de.wikipedia.org/wiki/Chilling_effect)“ für dieses
Phänomen.) Dies zeigt sich im [Volkszählungsurteil des
Bundesverfassungsgerichts aus dem Jahre
1983](https://de.wikipedia.org/wiki/Volksz%C3%A4hlungsurteil), das als
Geburtsstunde unsere Grundrechts auf informationelle Selbstbestimmung
gilt, und im [Urteil des US Supreme Court aus dem Jahre
1995](https://supreme.justia.com/cases/federal/us/514/334/),
das anonyme Meinungsäußerungen als ehrenwerte Tradition für Zustimmung
*und* Widerspruch sowie als Schutz vor der
Tyrannei der Mehrheit ansieht.

Losgelöst von verfassungsrechtlichen Ansprüchen sei an einige
Situationen unerwünschter Überwachung erinnert: Zunächst sehe ich
Bereiche der Privatsphäre, auf die die meisten Menschen nicht von
Fremden auf der Straße angesprochen werden möchten: Suche nach Hilfe bei
Problemen mit Partner/in, in der Schule, im Familien- oder
Freundeskreis, im Berufsleben; Rat bei medizinischen Fragen,
Suchtproblemen oder in Missbrauchsfällen. Ich denke nicht, dass es
irgendjemanden geben sollte, der mich im Internet in solchen Fällen
beobachten kann, insbesondere weder der Internet-Provider noch der
werbewirtschaftlich ausgerichtete Betreiber eines sozialen Netzwerkes
oder unbekannte Dritte mit Zugriff auf deren Infrastrukturen.

Ich bin überzeugt, dass menschliche Kommunikation generell die
Privatangelegenheit der Beteiligten sein sollte, unabhängig vom Thema,
vom Kommunikationsmedium und der Anzahl der Beteiligten. So wie das
Gespräch mit meiner Frau in den eigenen vier Wänden unsere
Privatangelegenheit ist, wünsche ich mir auch dann Privatsphäre, wenn
wir uns während der Kommunikation an verschiedenen Orten aufhalten (wir
also per Telefon oder Internet kommunizieren) oder wenn ich mich in
einer größeren Gruppe befinde, beispielsweise bei Freunden oder im
Sportverein. Private, vergängliche, unaufgezeichnete Kommunikation muss
der Normalfall bleiben – sofern wir nicht bewusst darauf verzichten.

Die Realität sieht, wie wir wissen, völlig anders aus. Massenüberwachung
durch staatliche Stellen und durch Privatfirmen hat die orwellschen
Phantasien längst hinter sich gelassen, weshalb Alice und Bob digitale
Selbstverteidigung erlernen, um ihre Privatsphäre sowie ihre Grund- und
Menschenrechte zu verteidigen. (Im Abschnitt zu
[Messengern][alternative-messenger] habe ich darauf hingewiesen,
dass es mit OTR und OMEMO Protokolle gibt, die unsere Gespräche
schützen, wenn wir die richtigen Anwendungen einsetzen. Auf
[Anonymisierungsdienste][proxies]
und
[E-Mail-Verschlüsselung][gnupg]
gehe ich später ein.)


## <a name="kafka"></a> Kafkaeske Bedrohungen der Selbstbestimmung

Die bisher skizzierte orwellsche Ausgangssituation wird noch durch
kafkaeske Bedrohungen verschärft, die zu Hilflosigkeit und
Verwundbarkeit führen, weil Transparenz und Kontrolle in der
Verarbeitung personenbezogener Daten fehlen. Es werden mehr und mehr
Daten über uns gespeichert und verwendet, ohne dass wir uns dessen
bewusst wären. Problematisch wird dies, wenn versucht wird,
*Profile* zu erstellen, die
*Prognosen* über zukünftiges Verhalten erlauben
sollen, und dann basierend auf diesen Prognosen präventive Maßnahmen
ergriffen werden. Weil Prognosen naturgemäß ungenau sind ([besonders
wenn sie die Zukunft
betreffen](https://de.wikipedia.org/wiki/Prognose)), werden auf der
Basis von Profilen ergriffene Maßnahmen im Einzelfall völlig
unverhältnismäßig sein oder willkürlich erscheinen und zu nicht
nachvollziehbaren Effekten führen. Einige Beispiele mögen verdeutlichen,
dass unter diesen, heute schon existierenden Bedingungen jede/r Einzelne
um seine Freiheit fürchten darf – auch wer nichts Verbotenes macht.
Durch Vorratsspeicherung (mit dem Ziel der Terrorbekämpfung) wird sich
die Situation sicher nicht verbessern.

Betrachten Sie erstens den
[„Fall“ al-Masri](https://de.wikipedia.org/wiki/Khaled_al-Masri). Haben Sie den
noch in Erinnerung? Wenn nicht, lesen Sie den
[Wikipedia-Artikel über al-Masri](https://de.wikipedia.org/wiki/Khaled_al-Masri) oder diese
[Darstellung seines Schicksals in der Zeit](https://www.zeit.de/gesellschaft/zeitgeschehen/2014-12/cia-folter-bericht-khaled-el-masri),
und seien Sie dankbar, wenn Sie mit einem klassischen deutschen Namen
geboren wurden und wenn Sie auch sonst nicht zufällig im falschen Raster
landen.

Halten Sie sich zweitens vor Augen, dass in den USA
[Zehntausende von Flugpassagieren fälschlicherweise als Terrorverdächtige](https://www.heise.de/newsticker/meldung/Zehntausende-Flugpassagiere-faelschlicherweise-als-Terrorverdaechtigte-gelistet-155704.html)
behandelt werden; wer auf der No-Fly-List steht, darf zwar noch
Flugtickets kaufen, aber das Flugzeug dann überraschend nicht betreten.

Vergegenwärtigen Sie sich drittens, dass in Deutschland Datensammlungen
wie die im Jahr 2005 breiter diskutierte Datei
[„Gewalttäter Sport“](http://www.profans.de/gewalttater-sport) angelegt werden, in die auch unbescholtene Fans am Rechtsstaat vorbei
aufgenommen werden, was beispielsweise Ausreiseverbote nach sich zieht.
Derartige [Datensammlungen rund um Fußballspiele werden auch in 2016 noch fleißig ausgebaut](https://netzpolitik.org/2016/mehr-daten-als-tore-polizei-sammelt-fleissig-aber-oft-unrechtmaessig/).
Ähnlich werden offenbar auch beim BKA am Rechtsstaat vorbei
Datensammlungen aufgebaut, wie im Zusammenhang mit dem Entzug von
Akkreditierungen zum G20-Gipfel in 2017 bekannt wurde:
Journalisten durften ohne Angabe von Gründen ihrer Arbeit nicht mehr
nachkommen; im Nachhinein wurde festgestellt,
[„dass den Betroffenen Delikte vorgeworfen würden, die sie nachweislich nicht begangen haben“](https://www.heise.de/newsticker/meldung/G20-Akkreditierungsentzug-Kritik-an-rechtswidrigen-Eintraegen-in-BKA-Datei-3816862.html).

Fragen Sie sich viertens, wie
[Scoring](https://de.wikipedia.org/wiki/Kreditscoring) mit der Vergabe
von Krediten und der Höhe der zu zahlenden Zinsen zusammenhängt: Uns
unbekannte Daten entscheiden in unbekannter Weise, ob und wenn ja zu
welchen Konditionen wir Kredite oder Handy-Verträge erhalten.
[Im Jahre 2014 hat der Bundesgerichtshof in einem bestürzenden Urteil entschieden, dass Firmen wie die Schufa *nicht* erklären müssen, *wie* sie das Scoring berechnen](https://www.spiegel.de/wirtschaft/service/bgh-weist-klage-gegen-schufa-ab-was-verbraucher-wissen-muessen-a-945965.html),
was unsere kafkaeske Hilflosigkeit zementiert, wenn wir uns falsch
bewertet fühlen.

Fünftens sollen Personalverantwortliche zur Auswahl geeigneter
Bewerber/innen vermehrt auf Internet-Recherche setzen.  Die weiter
unten umrissenen Möglichkeiten zur Erstellung von
Persönlichkeitsprofilen ausgehend von Datenspuren im Internet deuten
die Ausmaße bestehender Möglichkeiten an.

In all diesen Beispielen werden auf der Grundlage von Datensammlungen,
die den Betroffenen unbekannt sind, Entscheidungen gefällt, die von den
Betroffenen weder nachvollzogen noch überprüft werden können. Dies
verkörpert kafkaeske Hilflosigkeit und Verwundbarkeit.

Neben dieser kurzen Nennung ausgewählter Fälle von Profilbildung seien
zwei Möglichkeiten der modernen Datenanalyse, die auch unter den
Begriffen Data Mining, Data Analytics, Predictive Analytics, Data
Science oder Big Data diskutiert wird, ausführlicher vorgestellt: Die
Kundenanalyse basierend auf dem mit Kunden- oder EC- bzw. Kreditkarten
zusammengeführten Kaufverhalten und die psychologische
Persönlichkeitsanalyse anhand von Daten sozialer Netzwerke.

[Im Jahre 2012 machte die US-Supermarktkette Target dank ihrer
ausgefeilten Kundenanalysen Schlagzeilen in der New York
Times](https://www.nytimes.com/2012/02/19/magazine/shopping-habits.html).
Für gezielte Werbeaktionen sind Firmen wie Target unter anderem an
Änderungen der Lebensumstände ihrer Kunden interessiert, die erhöhtes
Konsumverhalten erwarten lassen (z. B. Job-Wechsel, Hausbau, Hochzeit,
Geburt von Kindern). Besonders bei der Geburt von Kindern erschien es
Target vorteilhaft (und wir dürfen wohl annehmen, dass nicht nur Target
dieser Denkweise folgt), vor der Konkurrenz von Schwangerschaften zu
wissen, um Kunden frühzeitig passend umwerben zu können. Target gelang
es tatsächlich, Änderungen in den Mustern des Kaufverhaltens schwangerer
Frauen zu erkennen und so bevorstehende Geburten mit hoher
Wahrscheinlichkeit bereits im zweiten Drittel der Schwangerschaft
vorherzusagen. In einem Fall entdeckte der Vater einer Schülerin die von
Target verschickten Rabattgutscheine für Babykleidung und Krippen.
Empört wandte er sich an Target, ob man dort seine Tochter zu einer
Schwangerschaft ermuntern wolle. Wie sich nach familieninternen
Gesprächen herausstellte, war die Tochter tatsächlich schwanger, und
Target wusste dies vor dem Vater.

Vorhersage von Schwangerschaften durch Muster im Kaufverhalten. Ist das
akzeptabel oder gruselig? Es würde mich überraschen, wenn solche
Techniken nicht auch bei Allergien, chronischen Krankheiten, Mager- oder
Drogensucht, Depressionen oder der sexuellen Orientierung
funktionierten. Lassen Sie Ihrer Phantasie mal freien Lauf.

Sollten Sie öfter auf Bargeld ohne Kundenkarte zurückgreifen?

Der
[New-York-Times-Artikel](https://www.nytimes.com/2012/02/19/magazine/shopping-habits.html)
stellt klar, dass Target das eigene Verhalten als fragwürdig erkannt
hat, leider nicht aus ethischen, sondern aus kommerziellen Erwägungen:
Kunden reagierten verstört, wenn Target ohne erkennbare Grundlage um
ihre als privat angenommenen Lebensumstände wusste. Die Konsequenz bei
Target war dann nicht, auf derartige Analysemethoden zu verzichten.
Stattdessen wird Werbung, die auf derartigen Prognosen basiert, nicht
mehr plump in separaten Werbeaktionen versandt, sondern wie zufällig
unter „normale“ Werbung gemischt, damit Kunden nicht mehr so leicht
erkennen können, welche Fakten die Supermarktkette über ihre privaten
Lebensumstände prognostiziert hat. Dieser Artikel verdeutlicht also
nicht nur die Möglichkeiten moderner Konsumanalysen, sondern auch, dass
wir bewusst im Unklaren über diese Möglichkeiten gehalten werden.

Der
[New-York-Times-Artikel](https://www.nytimes.com/2012/02/19/magazine/shopping-habits.html)
hat zunächst nichts mit dem Internet zu tun. Die skizzierten Analysen
sind bereits dank der vergleichsweise eingeschränkten Datenspuren
möglich, die wir (bei Verzicht auf Bargeld) beim Einkaufen in der
physischen Welt hinterlassen. Online erzeugen wir prinzipiell bei jedem
Schritt und Klick deutlich umfangreichere Datenspuren, was Einkäufe
ebenso umfasst wie die vorangehende Recherche, unsere Kommunikation,
unsere Meinungsbildung und -äußerung, unser Denken.

Inzwischen existieren vielfältige wissenschaftliche Studien, welche die
heutigen Möglichkeiten demonstrieren und vorantreiben. So verdeutlichte
eine [wissenschaftliche Studie zur
Gesichtserkennung](https://www.heinz.cmu.edu/~acquisti/face-recognition-study-FAQ/)
im Jahre 2011 im Wesentlichen, dass es bereits damals möglich war, nur
mit dem Foto einer Smartphone-Kamera umfangreiche Internet-Dossiers von
Fremden zu erstellen.

Weitere Studien befassen sich mit der Analyse von Facebook-Likes. Im
Jahre 2013 wurde gezeigt, dass [Facebook-Likes mit hoher Genauigkeit
für Prognosen von Geschlecht und sexueller Orientierung, Intelligenz,
ethnischer Herkunft, politischen Einstellungen, Religionszugehörigkeit
und Drogenkonsum genutzt werden
können](https://www.pnas.org/content/110/15/5802.abstract)
([PDF](https://www.pnas.org/content/pnas/110/15/5802.full.pdf)).

Im Jahre 2015 wurde eine Studie zur Prognose psychologischer
Persönlichkeitsprofile ([Big
Five](https://de.wikipedia.org/wiki/Big_Five_(Psychologie)))
veröffentlicht, in der gezeigt wird, dass [Prognosen von Neurotizismus,
Extraversion, Offenheit für Erfahrungen, Gewissenhaftigkeit und
Verträglichkeit auf der Basis von Likes genauer sind als die
Einschätzungen von Freunden und
Kollegen](https://www.pnas.org/content/112/4/1036.full). Angesichts
dieses Ergebnisses spekulieren die Autoren, dass wir zukünftig zentrale
Entscheidungen zu Karrierepfaden oder zur Partnerwahl Computern
überlassen könnten, weil deren datengetriebene Vorschläge unsere Leben
verbessern werden. Andererseits warnen sie vor Möglichkeiten der
Manipulation und Einflussnahme, wenn Dritte uns besser kennen als unsere
nächsten Familienmitglieder.

Wenn Sie Facebook (regelmäßig) verwenden, können Sie Ihr
[Persönlichkeitsprofil mit Hilfe des Werkzeugs Apply Magic Sauce berechnen lassen](https://applymagicsauce.com/),
das von Forschern der Universität Cambridge bereitgestellt wird, unter denen
einige auch an obigen Studien beteiligt waren. (Da ich Facebook nicht
nutze, kann ich nichts zu den Ergebnissen sagen.)

Im Sommer 2016 veröffentlichte die [Washington Post eine Liste von 98 persönlichen Merkmalen, die Facebook verwendet, um Werbung zu personalisieren](https://www.washingtonpost.com/news/the-intersect/wp/2016/08/19/98-personal-data-points-that-facebook-uses-to-target-ads-to-you/).
Manches ist zu erwarten, aber schauen Sie mal rein, ob Sie das alles so
und so vorgeblich präzise erwartet hätten.

Was denken Sie, wer Daten für Prognose- und andere Zwecke von Facebook
kauft und was damit anstellt?

Natürlich machen Facebook Likes nur einen Baustein im Puzzle unseres
Kauf-, Lese- und Kommunikationsverhaltens im Internet aus. Wenig
diskutiert aber bereits praktiziert werden beispielsweise Analysen
unserer Sprache. So wurde im Jahre 2015 in der [FAZ die Firma Psyware
vorgestellt](https://www.faz.net/aktuell/gesellschaft/menschen/software-erkennt-persoenlichkeit-mit-sprachanalyse-13596216.html).
Diese Firma erstellt Persönlichkeitsprofile durch die automatisierte
Analyse von aufgezeichneten Telefonaten, die mit Sprachcomputern geführt
wurden. Die Auswertung 15-minütiger Gespräche erlaubt die Erstellung von
Persönlichkeitsprofilen, deren Genauigkeit fast derjenigen entspricht,
die von Psychologen mit aufwändigen Testverfahren erreicht werden kann.
Zu den Kunden von Psyware gehören Banken, Versicherer, die Polizei,
Online-Partnerbörsen.

Zudem gibt es diverse medizinische Studien, die sich mit der Erkennung
von Erkrankungen durch die Analyse von Sprache befassen. Dies
funktioniert etwa für Psychosen, Alzheimer und Parkinson.

Mir fällt spontan Skype ein, wenn ich mich frage, bei wem man wohl
Zugriff auf umfangreiche Gesprächsdaten erlangen kann. Freie,
dezentralisierte, verschlüsselte Alternativen erscheinen mir sehr
wertvoll, etwa die anderswo genannten Alternativen [Mumble oder
Spreed.ME][voip].

Die Konsequenzen der bisher skizzierten kafkaesken Bedrohungen sind für
uns überwiegend sichtbar, wenn auch unverständlich. Perfider sind
Szenarien, in denen wir gar nicht bemerken, dass wir individuell
manipuliert werden. Eine subtile Form der Manipulation menschlichen
Verhaltens und menschlicher Entscheidungen wird euphemistisch als
[Nudging](https://www.spektrum.de/news/nudging-darf-ein-staat-seine-buerger-zu-gesuenderem-verhalten-verhelfen/1349983)
oder [Big
Nudging](https://www.spektrum.de/kolumne/big-nudging-zur-problemloesung-wenig-geeignet/1375930)
(von engl. *to nudge*, anstupsen) bezeichnet.
Im positiven Sinne sollen Menschen durch äußere Anreize, die
typischerweise nur unbewusst wirken, zu besserem Verhalten geführt
werden, etwa bezüglich ihres Ernährungs- oder Umweltverhaltens. So wird
empfohlen, dass Süßigkeiten auf Buffets schwieriger zu greifen sein
sollten als Obst, um gesunde Ernährung zu fördern. Oder dass in Urinalen
angebrachte Fliegen die Zielfähigkeit der Männer erhöhen.

Werbung war schon immer eine Form von Nudging. Neu ist, dass Nudging
basierend auf Datenspuren personalisiert wird. Es erhält nicht mehr jede
Frau Schwangerschaftswerbung, sondern nur noch Schwangere (und deren
Umfeld, siehe Target-Beispiel oben), allerdings ohne dass dies für sie
erkennbar wäre.

Suchergebnisse und Nachrichten im Internet werden für unterschiedliche
Menschen unterschiedlich sortiert, was zum Phänomen der
[Filterblase](https://de.wikipedia.org/wiki/Filterblase) führt, wodurch
unser Denken geleitet wird, ohne dass wir dies bemerken könnten.

Noch unheimlicher finde ich folgende Erkenntnis aus dem Jahr 2014
(also weit vor den
[Enthüllungen rund um Cambridge Analytica](https://netzpolitik.org/2018/cambridge-analytica-was-wir-ueber-das-groesste-datenleck-in-der-geschichte-von-facebook-wissen/)):
[Facebook kann unsere Emotionen manipulieren und ausgewählte Gruppen anregen, an politischen Wahlen teilzunehmen](https://www.theguardian.com/technology/2014/jun/30/if-facebook-can-tweak-our-emotions-and-make-us-vote-what-else-can-it-do).

Nudging wird umso erfolgreicher ausfallen, je genauer es an uns und
unsere Vorlieben und Lebenssituationen angepasst werden kann. Wo immer
wir Dritten ermöglichen, Daten über uns zu sammeln, entstehen die
Grundlagen für Prognosen und Manipulationen, von denen wir nichts wissen
und die unsere Leben in unverständlichen Weisen beeinflussen werden.

In dem oben erwähnten Roman [ZERO von Marc
Elsberg](https://de.wikipedia.org/wiki/Zero_%E2%80%93_Sie_wissen,_was_du_tust)
wird eine Welt entworfen, in der eine Firma basierend auf Auswertungen
freiwillig bereitgestellter Daten Hilfe in allen Lebenslagen anbietet,
wobei neben den als solchen erkennbaren Hilfen auch subtile Formen
unbemerkter Manipulationen eingesetzt werden.


## <a name="nix-zu-verbergen"></a> Ich hab’ nix zu verbergen. Möglich, aber unwahrscheinlich. Und irrelevant.

Nach der Erörterung orwellscher und kafkaesker Bedrohungen möchte ich
auf das Nothing-To-Hide-Argument zurückkommen. Wenn Sie „normal“ und
wohlhabend und gesund sind und bleiben und dies auch auf Ihre Liebsten
zutrifft, haben Sie vielleicht wirklich nichts zu verbergen. Herzlichen
Glückwunsch!

Allerdings lohnt es sich zu bedenken, dass die Bewertung, ob etwas
„normal“ oder „verbergenswert“ ist, sowohl vom aktuellen Rechtssystem
als auch von der verfügbaren Technologie abhängt. Die
verdachtsunabhängige Überwachung der gesamten Bevölkerung (neben
Vorratsspeicherung in der Telekommunikation auch bei digitalen Fotos und
Fingerabdrücken für den ePass, Fotos aller Fahrzeuge und Fahrer [nicht
nur LKW!] an jeder Mautbrücke, Bundestrojaner zur Online-Durchsuchung,
Bargeldbegrenzungen zur Erzeugung zusätzlicher Datenspuren bei jeder
Zahlung usw.) zeigt in meiner Wahrnehmung ein tiefes Misstrauen der
Überwachenden gegenüber der Gesellschaft. Dieses Misstrauen kann ich bei
totalitären oder diktatorischen Staaten nachvollziehen, in
demokratischen Staaten in Europa jedoch nicht. Vor dem Hintergrund der
deutschen Geschichte frage ich mich allerdings, ob es selbstverständlich
ist, dass Deutschland auf ewig eine Demokratie bleibt. (Jahrelang
schrieb ich hier, der
[Rückgang der Wahlbeteiligung](https://de.wikipedia.org/wiki/Nichtw%C3%A4hler)
spräche nicht dafür; seit der Bundestagswahl 2017 dürfte klar sein,
dass größere Zunahmen in der Wahlbeteiligung nicht zu mehr Freiheit
führen müssen, sondern auch in Angst und selbstgewählte Abschottung
vor sowie Ausgrenzung von „den anderen“ führen können.)
Gab es bei uns nicht mal Gestapo, SA, SS und Stasi?
Was die wohl mit den unter Vorratsspeicherung und
[unten skizziertem Überwachungskapitalismus][ueberwachungskapitalismus]
erhobenen Daten gemacht hätten?
Wären Sie da „normal“ gewesen?

Die Frage, ob Bob etwas „Verbergenswertes“ oder gar „Verbotenes“ macht
oder nicht, hängt offenbar vom Standpunkt der Regierenden ab (Alice
denkt an den Widerstand im Dritten Reich, Oppositionelle in der DDR,
Tibeter,
[Falun-Gong](https://de.wikipedia.org/wiki/Falun_Gong)-Anhänger in
China oder den [Arabischen
Frühling](https://de.wikipedia.org/wiki/Arabischer_Fr%C3%BChling)).
[Eben Moglen prophezeite im Jahre
2013](https://blogs.fsfe.org/jens.lechtenboerger/2013/04/05/das-verworrene-web-das-wir-gewoben-haben/):

> „Unabhängig von Ihren politischen Präferenzen fängt irgendwo auf der
> Welt jetzt eine Regierung, deren Prinzipien Sie komplett ablehnen,
> damit an, das Netz zu nutzen, um Unterstützung zu finden, Einfluss auf
> die Bevölkerung zu nehmen und ihre Feinde zu entdecken. Überall auf
> der Welt werden von nun an die Regierungen, die tyrannische Züge
> annehmen, über ungeheuer leistungsfähige neue Werkzeuge verfügen, um
> dauerhaft an der Macht zu bleiben.“

Ich möchte in keiner Gesellschaft leben, deren Überwachungsmaßnahmen als
Vorbild und Rechtfertigung für tyrannische Regierungen dienen. Ich
träume, dass wir besser sein können.

Unabhängig von politischen Erwägungen hängt die Einschätzung, ob Bob
„normal“ ist, davon ab, was an welchem Maßstab mit welchen Methoden
gemessen wird. Wie im [vorigen Abschnitt][kafka] ausgeführt können mit
Facebook Likes im Speziellen bzw. mit Datenspuren im Netz im Allgemeinen
beispielsweise Persönlichkeitsprofile erstellt werden. Wenn Bob Pech
hat, sehen die ihn betreffenden Prognosen vor dem Hintergrund eines
Bewerbungsverfahrens schlecht für ihn aus (z. B. weil er unnormal ist
oder weil die Prognose einfach falsch ist). Ebenso erhält er vielleicht
keinen neuen Handy-Vertrag, weil sein Scoring schlecht ausfällt, obwohl
er den Vertrag locker hätte bezahlen können und wollen. Er hätte
vielleicht doch etwas verbergen sollen, weiß aber nicht, was. (Es ist
wenig verwunderlich, dass positiv wirkende Datenspuren gezielt gelegt
werden – etwa als Dienstleistungen spezialisierter Firmen oder durch
Studierende, die ihren Sport mit mehreren Fitness-Trackern verschiedener
Menschen absolvieren, aber das ist ein anderes Thema.)

In der Versicherungsbranche zeichnet sich zudem ein Trend zur
Entsolidarisierung ab, der durch technische „Fortschritte“ möglich wird.
[KFZ-Versicherungen bieten Rabatte für „gute“
Autofahrer](https://www.sueddeutsche.de/auto/telematik-tarife-bei-kfz-versicherungen-viel-ueberwachung-fuer-ein-bisschen-ersparnis-1.2486679),
die ihr Fahrverhalten mit einer Blackbox aufzeichnen und auswerten
lassen. Da die [überwältigende Mehrheit der Autofahrer glaubt, sie
gehöre zur besseren
Hälfte](https://www.zeit.de/2014/24/dunning-kruger-effekt-stimmts), wird
vermutlich auch die überwältigende Mehrheit glauben, sie könne bei
diesem Deal gewinnen. Offenbar kann das nicht stimmen. Gewinnen wird in
jedem Fall die Versicherung.

Lesen Sie unbedingt den [zugehörigen, kurzen
Artikel](https://www.zeit.de/2014/24/dunning-kruger-effekt-stimmts), der
dieses Phänomen mit dem
[Dunning-Kruger-Effekt](https://de.wikipedia.org/wiki/Dunning-Kruger-Effekt)
erklärt, nämlich der menschlichen Tendenz, eigenes Können im Vergleich
zu anderen zu überschätzen. Kurz gesagt: Die meisten von uns halten sich
nicht nur für normal, sondern sogar für besser als normal. Viele ahnen
demnach nicht einmal, dass sie etwas zu verbergen hätten.

In ähnlicher Weise bieten [Krankenversicherungen günstigere Tarife im
Austausch gegen die Vermessung von
Vitalwerten](https://www.tagesspiegel.de/weltspiegel/software-und-wearables-datenschuetzer-warnen-vor-fitness-apps/12162152.html),
heute vielleicht die täglich zurückgelegten Schrittzahlen (und nebenbei
die [Intensität und Häufigkeit des
Geschlechtsverkehrs](https://techcrunch.com/2013/07/05/how-health-trackers-could-reduce-sexual-infidelity/)),
morgen den genetischen Fingerabdruck und stündliche Blutwerte.

Waren Versicherungen nicht ursprünglich als Solidargemeinschaften
angelegt, wo Schwächeren geholfen wird? Wir müssen gar nicht wissen, was
„normal“ ist, damit das funktioniert. Dann müssen wir auch nicht die
verbergenswerten Daten erheben, mit denen die Schwächeren stigmatisiert
werden.

<a name="ueberwachungskapitalismus"></a>Diese Beispiele aus der
Versicherungswirtschaft passen zum von der Wirtschaftswissenschaftlerin
Shoshana Zuboff postulierten *Überwachungskapitalismus*. Ihr zufolge
befinden wir uns im neuen Zeitalter des maßgeblich von Google
vorangetriebenen [Überwachungskapitalismus (FAZ,
2016)](https://www.faz.net/aktuell/feuilleton/debatten/die-digital-debatte/shoshana-zuboff-googles-ueberwachungskapitalismus-14101816.html)
(engl. *surveillance capitalism*;
[wissenschaftliche Grundlagenarbeit als
PDF](http://www.palgrave-journals.com/jit/journal/v30/n1/pdf/jit20155a.pdf)):
Unsere aus digitalen Interaktionen mit kostenlosen Diensten gewonnenen
Verhaltensdaten werden als „Verhaltensüberschuss“ extrahiert und
analysiert, wodurch neue Produkte auf neuartigen Märkten entstehen, auf
denen wir weder als Käufer noch als Verkäufer beteiligt sind.
Stattdessen sind wir lediglich Quellen für kostenlose Rohstoffe in
neuartigen Produktionsprozessen.

Durch Big-Data-Algorithmen werden diese Rohstoffe in Vorhersageprodukte
umgewandelt, die auf Märkten für zukünftiges Verhalten gehandelt werden.
Nachdem zunächst hauptsächlich Werbetreibende als Kunden dieser Märkte
agierten, sieht Zuboff den Trend, „dass jeder Akteur, der Interesse
daran hat, probabilistische Informationen über unser Verhalten zu nutzen
und/oder zukünftiges Verhalten zu beeinflussen, als Käufer auf einem
Markt auftreten kann, auf dem das zukünftige Verhalten von Individuen,
Gruppen, Körpern und Dingen vorausgesagt und für die Erzielung von
Gewinnen genutzt wird.“

Zudem argumentiert Zuboff, dass unsere Entscheidungsrechte in Bezug auf
Privatsphäre im Überwachungskapitalismus umverteilt werden. Wir besitzen
diese Rechte nicht mehr, da sie auf Firmen wie Google übergehen, die
entscheiden können, welche Daten über uns sie wann zu welchen Zwecken
einsetzen, etwa für Belohnung und Strafe jenseits demokratischer
Kontrolle.

In der oben erwähnten Grundlagenarbeit führt Zuboff allgemeiner aus,
dass permanente Überwachung die Unsicherheiten ausräumt, die bisher
Ausgangspunkte für Verträge und Gesetze waren. Während wir uns bisher
bewusst entscheiden konnten, Verträge und Gesetze einzuhalten, wird es
angesichts eines allumfassenden „Big Other“ keine Entscheidungsfreiheit
mehr geben. Konformität wird Teil einer neuen Ordnung, in der Gesetze,
Verträge und Vertrauen abgelöst werden.

Überlegen Sie selbst, wie weit Sie den Einschätzungen von Zuboff folgen
wollen. Alice, Bob und ich sind uns einig, dass wir nicht als kostenlose
Rohstoffe ausgebeutet werden wollen.

Digitale Selbstverteidigung ist notwendig und möglich, sie ist
alternativlos.


## <a name="terror"></a> Aber die Terroristen!

Bei uns wird die elektronische Kommunikation nicht massenhaft und
grundgesetzwidrig überwacht, weil dies eine sinnvolle Maßnahme zur
Steigerung der allgemeinen Sicherheit wäre: Einerseits weichen
Kriminelle, besonders die Organisierten und die Terroristen, auf
[Wegwerf-Handys (engl. *burner phones*), die
nur einmal verwendet werden wie im November 2015 in
Paris](https://www.nytimes.com/2016/03/20/world/europe/a-view-of-isiss-evolution-in-new-details-of-paris-attacks.html),
oder auf Anonymisierungsdienste und gestohlene Identitäten aus, um
Kommunikationsmuster zu verbergen und der Vorratsdatenspeicherung zu
entgehen; andererseits muss die Vorbeugung von terroristischen
Anschlägen durch Analysen von Vorratsdaten aus mathematischen Gründen
fehlschlagen, der [Base Rate
Fallacy](https://www.schneier.com/crypto-gram/archives/2006/0315.html#5).
Elektronische Kommunikation wird überwacht, weil das technisch einfach
möglich ist, daher Begehrlichkeiten bei Ermittlungsbehörden weckt und
den Anschein von Tatkraft erwecken soll.

Weil es keine absolute Sicherheit gibt, muss jedes Streben nach
Sicherheit endlos und unstillbar bleiben. Entsprechend rechtfertigt die
„Befriedigung“ dieses Strebens den Einsatz jeder aktuell technisch
machbaren sicherheitssteigernden Maßnahme. Lassen Sie sich bitte nicht
von fehlgeleiteten Innenpolitikern verwirren, die inbrünstig behaupten,
flächendeckende und anlasslose Überwachung sei zum Schutz vor Terror
unverzichtbar. In der [New York Times war im Juli 2013 zu
lesen](https://www.nytimes.com/2013/07/04/opinion/kristof-how-could-we-blow-this-one.html),
dass seit 2005 jährlich 23 Amerikaner durch Terror sterben.
Dreiundzwanzig! Etwa doppelt so viele sterben an Bienen- und
Wespenstichen, 15-mal so viele durch Stürze von Leitern. Bedenken Sie
jetzt, dass [im Sommer 2013 in Deutschland 250 Menschen bei
Badeunfällen gestorben
sind](https://www.spiegel.de/panorama/sommer-2013-250-menschen-bei-badeunfaellen-ums-leben-gekommen-a-921684.html).
Jährlich gibt es bei uns mehr als [3000
Verkehrstote](https://de.wikipedia.org/wiki/Verkehrstod). An den Folgen
von Alkoholmissbrauch sollen sogar
[74.000](https://www.welt.de/welt_print/wissen/article8194870/74-000-Alkoholtote-pro-Jahr-in-Deutschland.html)
Menschen pro Jahr in Deutschland sterben. Natürlich sind Terrorakte
wesentlich medien- und wahlkampfwirksamer. Aber wenn Sie um Ihre
Sicherheit besorgt sind oder gar die Ihrer Mitmenschen, gibt es offenbar
deutlich wichtigere Themen als Terrorismus.

Dreiundzwanzig!

Zudem bin ich *sehr* beunruhigt, dass
Terroristen den Ermittlungsbehörden im Vorfeld regelmäßig bekannt sind,
bevor sie ihre Taten ausführen. Das war beim [11.
September](https://www.nytimes.com/2004/02/24/us/cia-was-given-data-on-hijacker-long-before-9-11.html)
so, und das gilt für den
[Boston-Marathon](https://www.zeit.de/gesellschaft/zeitgeschehen/2013-04/boston-fbi-kritik)
wie auch für mindestens [vier andere Fälle in den USA zwischen 2008 und
2013](https://www.politifact.com/truth-o-meter/statements/2013/apr/23/peter-king/rep-peter-king-says-alleged-boston-bombers-are-fif/),
für die Terroranschläge auf [Charlie Hebdo im Januar
2015](https://de.wikipedia.org/wiki/Anschlag_auf_Charlie_Hebdo) sowie
für diejenigen [am 13. November 2015 in
Paris](https://de.wikipedia.org/wiki/Terroranschl%C3%A4ge_am_13._November_2015_in_Paris).
Auf Spiegel Online hat Sascha Lobo im März 2016 recherchiert, dass
[alle 15 identifizierten islamistischen Attentäter, die in den letzten
zwei Jahren in der EU Anschläge verübt hatten, einschlägig im
islamistischen Kontext behördlich bekannt
waren](https://www.spiegel.de/netzwelt/netzpolitik/sascha-lobo-ueber-is-terror-ueberwachung-ist-die-falsche-antwort-a-1084629.html).
Er bezeichnet dies als „tiefgreifendes, strukturelles, multiples
Staatsversagen“. Dem gibt es nichts hinzuzufügen.

Grenzenlose Überwachung ist gerade in Mode, und warum sollten wir bei
elektronischer Kommunikation stehen bleiben? Es gibt seit längerer Zeit
Projekte, die zeigen, dass wir wesentlich mehr überwachen können –
nämlich unser gesamtes Leben (siehe z. B.
[MyLifeBits](https://www.microsoft.com/en-us/research/project/mylifebits/)).
Die Idee, sich eine Videokamera mit Mikrophon auf den Kopf zu schnallen
und die aufgenommenen Daten für die Ewigkeit zu speichern, war bei
Projektstart (ca. 2002) noch neu. Heute können Smartphones, Brillen oder
Uhren diese Aufgabe übernehmen, morgen ein Implantat in der Netzhaut
oder gleich im Gehirn.

Es würde sicherlich nur noch wenige Verbrechen geben, wenn
*jede/r* gesetzlich verpflichtet würde, so ein
Implantat zu tragen und die Daten an einen vertrauenswürdigen Server
einer vertrauenswürdigen Ermittlungsbehörde zu senden. Immer und
überall. Dann haben wir nichts mehr zu verbergen.

<a name="leseempfehlungen"></a>Gehen Sie in eine Buchhandlung Ihres
Vertrauens, kaufen und lesen Sie die Romane [*The Circle* von Dave
Eggers](https://en.wikipedia.org/wiki/The_Circle_%28Eggers_novel%29),
[*Little Brother* von Cory
Doctorow](https://de.wikipedia.org/wiki/Little_Brother_%28Roman%29) und
[*ZERO* von Marc
Elsberg](https://de.wikipedia.org/wiki/Zero_%E2%80%93_Sie_wissen,_was_du_tust).
Ersterer entwirft eine orwellsche Entwicklung unserer Welt, der zweite
eine kafkaeske, letzterer kombiniert beides – Weckrufe mit Denkanstößen
zur Verteidigung unserer Freiheit. Alle Bücher sind in deutscher Sprache
verfügbar. *Little Brother* steht unter einer
Creative-Commons-Lizenz und wird auf der [Web-Seite des Autors zum
kostenlosen Download](http://craphound.com/littlebrother/download/)
angeboten (eine [Übersetzung
woanders](https://archive.org/details/LittleBrotherByDoctorowdeutsch)).

Lesen! Verschenken! Besonders *Little Brother* an Jugendliche! Da steht
nahezu alles von dem drin, worüber ich hier schreibe; allerdings
unterhaltsam, spannend und haarsträubend.

{% include labels.md %}
