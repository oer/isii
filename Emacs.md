---
layout: page
sequence: 14
title: 'GNU Emacs: GnuPG, DefaultEncrypt, ExtendSMIME'
# Do not include in TOC # toctitle: Emacs
description: GNU Emacs makes encryption easy.
keywords: Emacs, encryption, e-mail, EasyPG
Time-stamp: "2019-06-16 11:25:07"
lang: en
---

I’m using [GNU Emacs](https://www.gnu.org/software/emacs/) for almost
everything related to text processing. In particular, for e-mail and
news I’m using
[Gnus](https://www.gnu.org/software/emacs/manual/gnus.html).

GNU Emacs comes with built-in support for
[GnuPG](https://www.gnupg.org/), e.g., via the [EasyPG
Assistant](https://www.gnu.org/software/emacs/manual/epa.html), which is
able to en- and decrypt files with the extension `.gpg` transparently if
you put the following into your `~/.emacs`:

    (require 'epa-file)
    (setq epa-file-encrypt-to "<your keyid>")

Moreover, [Gnus supports
GnuPG](https://www.gnu.org/software/emacs/manual/html_node/message/Security.html)
via the insertion of so-called MML secure tags, which contain encryption
instructions to be performed before a message is sent. However, in the
past I sent plaintext e-mails (more than once, I’m afraid) that really
should have been encrypted ones.  To prevent myself from forgetting to
encrypt e-mails again, I wrote
[DefaultEncrypt](https://gitlab.com/lechten/defaultencrypt), provided
by file `jl-encrypt.el`.

DefaultEncrypt aims for automatic insertion of MML secure tags into
messages if public keys (either OpenPGP public keys or S/MIME
certificates) for all recipients are available.  In addition, before a
message is sent, the user is asked if plaintext should really be sent
unencryptedly when public keys for all recipients are available.

In general, I [recommend OpenPGP via GnuPG over
S/MIME](https://blogs.fsfe.org/jens.lechtenboerger/2013/12/23/openpgp-and-smime/)
(which is the default for Gnus).  If you are really interested in S/MIME
then I suggest that you take a look at file ExtendSMIME (file `jl-smime.el`).

My signed versions are available here:

-   For Emacs versions up to 24.x.
    -   [DefaultEncrypt Version 4.1.1](/emacs/jl-encrypt4.1.1/jl-encrypt.el)
        -   [GnuPG signature](/emacs/jl-encrypt4.1.1/jl-encrypt.el.asc)
    -   [ExtendSMIME Version 3.1](/emacs/jl-smime3.1/jl-smime.el)
        -   [GnuPG signature](/emacs/jl-smime3.1/jl-smime.el.asc)
-   For Emacs versions starting with 25.1.
    -   [DefaultEncrypt Version 4.4](/emacs/jl-encrypt4.4/jl-encrypt.el)
        -   [GnuPG signature](/emacs/jl-encrypt4.4/jl-encrypt.el.asc)
    -   [ExtendSMIME Version 3.3](/emacs/jl-smime3.3/jl-smime.el)
        -   [GnuPG signature](/emacs/jl-smime3.3/jl-smime.el.asc)
