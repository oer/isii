---
layout: page
sequence: 5
title: So schlimm?! Was können Alice und ich jetzt machen?
toctitle: Digitale Selbstverteidigung
description: Digitale Selbstverteidigung erfordert
keywords: PC, Grundsicherung, Hintertür, Schwachstelle, Exploit, Updates, Firewall, freie Software, Firefox, Verschlüsselung, asymmetrische, Kryptographie, NoScript, Werbe-Blocker, Ad-Blocker, Malware, Malvertising, SSL, TLS, Vertrauen, Man-in-the-Middle Angriffe, MITM, Proxy, Anonymisierung, Mix
Time-stamp: "2019-07-24 17:08:26"
permalink: /Digitale_Selbstverteidigung.html
---

Die zentrale Idee für informationelle Selbstbestimmung im Internet ist,
Datenschutz aktiv durch *digitale Selbstverteidigung* zu betreiben. Dazu
muss Alice erstens *selbst* entscheiden, wann
sie anonym bleiben möchte, wann sie ihre Handlungen z. B. durch Cookies
für welche Zeiträume protokollieren lässt, und wann sie ihre Anonymität
wem gegenüber aufgibt, um Dienste in Anspruch zu nehmen. Zweitens muss
Alice geeignete Software installieren und konfigurieren, um ihre
Entscheidungen auch durchsetzen zu können. Auf Schutz durch andere
sollte sie nicht warten. In diesem Abschnitt erläutere ich grundlegende
Techniken ([Grundsicherung des PCs][grundsicherung-des-pcs],
[asymmetrische Kryptographie als Grundlage der Verschlüsselung im Internet][ssl-tls],
[Proxy-Verfahren als Grundlage von Anonymität im Internet][proxies]),
bevor nachfolgend konkrete Software-Projekte thematisiert werden.
Wie in der Einleitung bereits angedeutet, halte ich den Einsatz
eigener Hardware entsprechend der
[FreedomBox](https://freedomboxfoundation.org/)-Vision für ratsam; die
Verwendung unfreier, versklavender Cloud-Dienste in den USA mit fragwürdigen
~~Datenschutz-~~ Datenschatz- und Nutzungsbedingungen ist keine gute
Idee.

**Inhalt der folgenden Unterabschnitte:**

* TOC
{:toc}

## Grundsicherung des PCs

Jeder mit dem Internet verbundene Computer (also PCs, Router, Telefone,
Tablets, E-Books, smarte Brillen, Uhren, Glühbirnen, Stromzähler und
TVs, digitale Puppen, Autos usw.) ist Angriffsversuchen ausgesetzt,
durch die Unbekannte Zugriff auf unsere Geräte, unsere Daten
(Passwörter, Kontodaten, Kreditkarteninformationen, Lebensumstände und
Vorlieben, Forschungsergebnisse, strategische Informationen) oder
einfach auf unsere Rechenleistung und Bandbreite erhalten wollen (der
Computer wird beispielsweise zum
[Bot/Zombie](https://de.wikipedia.org/wiki/Zombie_%28Internet%29)
[[Erläuterungen vom Bundesamt für Sicherheit in der Informationstechnik
mit 6-minütigem
Animationsfilm](https://www.bsi-fuer-buerger.de/BSIFB/DE/Risiken/BotNetze/botnetze_node.html)],
der Spam versendet, an DDoS-Angriffen teilnimmt, illegale Inhalte
verteilt, Bitcoins schürft etc.).

*Jeder* Computer wird mit fehlerhafter
Software ausgeliefert, die anfällig für
[Zero-Day-Exploits](https://de.wikipedia.org/wiki/Exploit) ist, also
für Angriffe, gegen die es *keinen* Schutz
gibt. Das betrifft PCs, Router, Telefone, Tablets, E-Books, smarte
Brillen, Uhren, Glühbirnen, Stromzähler und TVs, digitale Puppen, Autos
usw.

Ich hatte bereits erwähnt, dass [„offline“ ein starkes Qualitätsmerkmal
ist][smart]. Ich möchte dies wiederholen: Offline ist
ein starkes Qualitätsmerkmal.

Darüber hinaus wissen wir spätestens seit den Snowden-Enthüllungen, dass
Geheimdienste Geräte mit Hintertüren ausstatten (lassen), wie der vom
Spiegel im Jahre 2013 dokumentierte [50-seitige
ANT-Katalog](http://www.spiegel.de/netzwelt/netzpolitik/neue-dokumente-der-geheime-werkzeugkasten-der-nsa-a-941153.html)
gezeigt hat. Lesen Sie den
[Spiegel-Artikel](http://www.spiegel.de/netzwelt/netzpolitik/neue-dokumente-der-geheime-werkzeugkasten-der-nsa-a-941153.html)
nochmal nach: Es geht nicht um Terrorabwehr, sondern Spionage mit Zielen
in Politik und Wirtschaft. Hardware aus den USA wird schon auf dem
Postwege mit Hintertüren versehen.

Natürlich gehen nicht nur die USA so vor. Jeder gute
Auslandsgeheimdienst dürfte ähnliche Programme verfolgen, nur sind die
fähigeren bisher noch nicht in die Schlagzeilen geraten. Beispielsweise
erfordert ein chinesisches Gesetz aus dem Jahr 2015, dass [Technologie
für kritische Sektoren (wie Banken und Versicherungen) „sicher und
kontrollierbar“](https://www.nytimes.com/2015/07/03/business/international/jitters-in-tech-world-over-new-chinese-security-law.html)
sein müsse, was mit dem Einbau von Hintertüren und der Herausgabe von
Verschlüsselungsschlüsseln interpretiert wird. China und USA. Wo kommen
unsere Hard- und Software nochmal her?

Hintertüren können generell (von Geheimdiensten wie anderen Kriminellen)
in der Hardware, im [BIOS](https://de.wikipedia.org/wiki/BIOS), in
[Firmware](https://de.wikipedia.org/wiki/Firmware) (z. B. von
Prozessor, Festplatten oder Netzwerkkarten), im Betriebssystem oder in
anderer Software eingebaut werden. Eine schockierende, aber
faszinierende Forschungsarbeit zeigte im Jahre 2013, wie [Trojaner in
der Hardware durch eine Änderung der Dotierung von Transistoren
verborgen](http://sharps.org/wp-content/uploads/BECKER-CHES.pdf) werden
können.  Weil diese Art von Hardware-Trojanern keine zusätzlichen
Schaltkreise benötigt, ist es nahezu unmöglich, sie zu entdecken.

Kriminelle, die uns über das Internet angreifen, verfügen nicht über die
Möglichkeit, unsere Hardware mit Hintertüren bzw.
[Trojanern](https://de.wikipedia.org/wiki/Trojanisches_Pferd_%28Computerprogramm%29)
auszustatten.  Sie müssen daher auf die nächstbeste Möglichkeit
zurückgreifen, nämlich
[Firmware](https://de.wikipedia.org/wiki/Firmware)-Trojaner, die auch
in oben zitiertem
[Spiegel-Artikel](http://www.spiegel.de/netzwelt/netzpolitik/neue-dokumente-der-geheime-werkzeugkasten-der-nsa-a-941153.html)
erwähnt werden.  Diese Trojaner überleben in der
[Firmware](https://de.wikipedia.org/wiki/Firmware) naturgemäß selbst
die Neuinstallation des Betriebssystems.  Als Reaktion auf die im Zuge
von [„Vault 7“](https://wikileaks.org/ciav7p1/)
enthüllten CIA-Angriffswerkzeuge hat
[Intel im März 2017 das Werkzeug CHIPSEC veröffentlicht, mit dem man sein BIOS auf unbefugte Modifikationen prüfen kann](https://securingtomorrow.mcafee.com/business/chipsec-support-vault-7-disclosure-scanning/).

Die IT-Sicherheitsexpertin und
[Qubes-OS](https://www.qubes-os.org/)-Entwicklerin Joanna Rutkowska
hat im Jahre 2015 detailliert aufgeschlüsselt, dass
[praktisch keine Aussicht besteht, Rechner mit modernen Intel-Prozessoren abzusichern](https://blog.invisiblethings.org/papers/2015/x86_harmful.pdf).
Besonders gravierend ist, dass Intel-Prozessoren seit 2008 eine
sogenannte Management Engine (ME) beinhalten.  Diese ME ist ein
eigener Computer *innerhalb* des Prozessors, der Zugriff auf die
gesamte Hardware besitzt (einschließlich Hauptspeicher- und
Netzwerkzugriff) und der durch außerhalb von Intel unbekannte, in der
Firmware fest verdrahtete Software gesteuert wird.  Mir als
Informatiker erscheint das schon bemerkenswert: Im Allgemeinen steuern
wir Programmierer, was der Prozessor als Gehirn eines Rechners macht.
Im Falle von Rechnern mit modernen Intel-CPUs sind uns diese Steuerung
und Kontrolle entzogen.
([Wie im August 2017 bekannt wurde, gibt es offenbar einen undokumentierten Betriebsmodus von Intel-CPUs, in dem die ME deaktiviert ist](https://www.heise.de/security/meldung/Intel-Management-Engine-ME-weitgehend-abschaltbar-3814631.html);
das ist allerdings wohl für die NSA gedacht – deutschen Behörden,
Unternehmen oder gar Bürgern steht das nicht zur Verfügung.)

Haben Sie Ihre
[Firmware im Mai 2017 aktualisiert, um eine in der ME enthaltene Sicherheitslücke zu schließen](https://www.heise.de/security/meldung/Sicherheitsluecke-in-vielen-Intel-Systemen-seit-2010-3700880.html)?
Was denken Sie über den im
[September 2017 publizierten Hack der ME](https://www.heise.de/security/meldung/Intel-Management-Engine-gehackt-3837239.html)?
Oder die [Warnung des BSI im November 2017 vor mehreren Schwachstellen](https://www.bsi.bund.de/SharedDocs/Warnmeldungen/DE/CB/2017/11/warnmeldung_cb-k17-2012.html)?
Sind Sie für größere IT-Budgets verantwortlich?  Ist Intel wirklich
eine verantwortbare Wahl?  Bei AMD ist die Lage übrigens auch nicht besser –
mit dem Platform Security Processor (PSP) gibt es dort ein Äquivalent
zur ME.

Haben Sie sich mal gefragt, warum das wohl so ist und wer von dieser
Funktionalität profitiert?  Wieso gibt es nicht mehr Widerstand gegen
den Kauf derart absurder Geräte?  (Letztere Frage kann ich nur mit
Unkenntnis der Kaufenden erklären.  Da Sie das hier gelesen haben,
gibt es für Sie keine Ausrede mehr.)

Die bisherigen Erläuterungen zu nicht vertrauenswürdigen Prozessoren
haben übrigens nichts mit /den/ Unsicherheitsbegriffen des Jahres
2018, [Meltdown und Spectre](https://meltdownattack.com/), zu tun.
Meltdown und Spectre stellen im Wesentlichen Sicherheitslücken in
zahlreichen modernen Prozessoren und Grafikkarten dar, die angesichts
der Komplexität dieser Komponenten nicht besonders überraschen.
Meltdown und Spectre sind aber eben auch „nur“ Sicherheitslücken,
keine bewusst hinzugefügten Komponenten wie die Management Engine.

<a name="hardware"></a>
Auch wenn sie ebenfalls von Meltdown und Spectre betroffen sind,
können ARM-, MIPS- oder POWER-Prozessoren freiheitsgewährende
Alternativen zu nicht vertrauenswürdigen modernen Prozessoren von
Intel und AMD sein, eventuell diese
[POWER9-Workstations mit offener Firmware (2017)](https://www.heise.de/newsticker/meldung/IBM-Power9-Workstation-mit-offener-Firmware-und-Linux-3798402.html),
über die ich allerdings nichts weiß, was über den verlinkten Artikel
hinausgeht.  Alternativ können Sie das
[PowerPC-Notebook-Projekt](https://www.powerpc-notebook.org/de/)
mit vorantreiben!

Mein aktuelles Notebook ist ein ThinkPad X200 mit deaktivierter
Management Engine, freiem Bootloader ([Libreboot](https://libreboot.org/))
und GNU/Linux als Betriebssystem ([Trisquel](https://trisquel.info/de)).
Verweise auf Unternehmen, die derartige Geräte verkaufen, finden Sie
beim Zertifizierungsprogramm
[„Respects Your Freedom“ der Free Software Foundation](https://www.fsf.org/resources/hw/endorsement/respects-your-freedom).
(Das sind natürlich keine schicken Bleeding-Edge-Geräte mit modernen
Prozessoren, sondern solide Geräte mit älteren Prozessoren, die noch
keine oder eine deaktivierte Management Engine beinhalten.
[Wie gesagt, unsere Marktmacht entscheidet.][smart])

Ich hatte bereits erwähnt, dass [freie Software][freie-software]
empfehlenswert ist, um Kontrolle über unsere Geräte zu erhalten.
Ich möchte dies wiederholen: Verwenden Sie freie Software.

Nach dieser deprimierenden Einleitung hoffe ich, dass Sie noch nicht
verzweifelt sind, und versuche nun, Sie zur Absicherung Ihres PCs zu
motivieren. Lassen Sie mich zu einer Analogie greifen. Vermutlich
verschließen Sie die Tür, wenn Sie Ihr Haus oder Ihre Wohnung verlassen.
Warum eigentlich? Schlüsseldienste bzw. Kriminelle mit deren Werkzeugen
können die Tür wahrscheinlich in kurzer Zeit öffnen. Und wenn die nicht,
dann jeder, der mit einem Vorschlaghammer oder einer Abrissbirne einen
neuen Eingang anlegt. Der entscheidende Punkt ist, dass Sie sich durch
die verschlossene Tür nicht als Opfer aufdrängen, auch wenn Ihnen klar
ist, dass Sie Einbrüche nicht verhindern können. Ähnlich sollte es auch
im Internet sein: Lassen Sie Ihre Türen nicht geöffnet stehen, drängen
Sie sich nicht als Opfer auf.

Die folgenden Maßnahmen sind für private Rechner gedacht, um deren
Angriffsfläche zu reduzieren und um den von einem Angriff verursachten
Schaden zu reduzieren.  Im Unternehmenskontext ist stattdessen ein
systematisches Sicherheitsmanagement erforderlich, wie es beispielsweise
nach den [BSI-Vorgaben zum IT-Grundschutz](https://www.bsi.bund.de/DE/Themen/ITGrundschutz/itgrundschutz_node.html)
aufgebaut werden kann.

<a name="live-cd"></a>**Live-CD.** Die
einfachste Art, den Rechner zu schützen, ist der Einsatz einer Live-CD.
Hier wird das Betriebssystem von einer CD/DVD gestartet, die nur lesbar
ist (oder von einem USB-Stick mit Schreibschutzschalter). Selbst wenn
ein Angreifer eindringen kann, kann er das Betriebssystem nicht
verändern, so dass Alice beim nächsten Start wieder ein sauberes System
nutzt. (Nach den einleitenden Erläuterungen sollte klar sein, dass es
auch hier keine garantierte Sicherheit gibt. Schadsoftware kann sich
nicht nur im Betriebssystem einnisten, sondern auch im BIOS oder in
Firmware. Sie kann auch ab Werk oder seit einer Zwischenstation bei der
Lieferung in der Hardware eingebaut sein. Dennoch verschaft ein
Live-System einen deutlichen Sicherheitsvorsprung gegenüber einem
„normalen“ PC.)

Ein bekanntes GNU/Linux-Live-System ist
[Knoppix](http://knoppix.net/). Darüber hinaus können viele
verbreitete
[GNU/Linux-Distributionen](https://de.wikipedia.org/wiki/Linux-Distribution)
wie [Ubuntu](http://ubuntuusers.de/) und
[Fedora](https://fedoraproject.org/wiki/FedoraLiveCD) als Live-CDs
gestartet werden. [Tor](https://www.torproject.org/) ist hier
allerdings nicht enthalten.

<a name="tails"></a>
Demgegenüber ist [*Tails*](https://tails.boum.org/) ein sowohl von CD als
auch USB-Stick ausführbares
Live-[Debian-GNU/Linux](https://www.debian.org/), das verspricht,
sämtliche Netzwerkdaten automatisch durch
[Tor](https://www.torproject.org/) zu leiten und keinerlei Spuren auf
der Festplatte zu hinterlassen. Weder Alice noch ich haben diese CD
bisher getestet. Die durch ihre Arbeit mit Edward Snowden bekannt
gewordene [Laura](https://de.wikipedia.org/wiki/Laura_Poitras) hält
[Tails für ein hervorragendes
Werkzeug](https://www.wired.com/2014/10/laura-poitras-crypto-tools-made-snowden-film-possible/).

Speziell für ihr Online-Banking verwendet Alice ein Live-Linux, das
von einer CD oder einem USB-Stick mit Schreibschutzschalter gestartet
wird und daher deutlich weniger anfällig für persistente Schädlinge
ist als andere Systeme.

Für Bastler kommt evtl. auch das Aufsetzen eines
[benutzerfreundlichen, schreibgeschützten Live-Systems mit SATA-DOM](https://www.vkldata.com/Benutzerfreundliche-Live-Systeme-mit-SATA-DOM)
in Frage.

Falls Alice ein „normal installiertes“ Betriebssystem nutzt, beachtet
sie alle folgenden Punkte.

**Firewall.** Alice aktiviert eine Firewall,
bevor sie den Rechner erstmalig an das Internet anschließt. Dies ist
notwendig weil nahezu jedes Betriebssystem in der Standardkonfiguration
Dienste startet, auf die über das Internet zugegriffen werden kann und
die zudem Programmierfehler enthalten, durch die ein Angreifer Kontrolle
über den Rechner erlangen kann. Der Erfolg der Würmer
[Blaster](https://de.wikipedia.org/wiki/W32.Blaster) und
[Conficker](https://de.wikipedia.org/wiki/Conficker) demonstrierte dies
sehr eindrucksvoll. Man beachte, dass derartige Würmer keinerlei
Interaktion von Alice erfordern: Sie installiert nichts aus dubiosen
Quellen, sie öffnet keine Anhänge. Sie schließt ihren Rechner lediglich
an das Internet an. Wenn Sie allerdings ihre (richtig konfigurierte)
Firewall aktiviert, werden alle Dienste vor Zu- und damit Angriffen aus
dem Internet abgeschirmt.

Alice hört übrigens nicht auf Ratschläge, auf die Firewall zu
verzichten, wenn sie stattdessen alle nicht benötigten Dienste
deaktiviert. Sie bliebe dann nämlich bei *allen* Schwachstellen in den Netzwerkprotokollen
selbst schutzlos (z. B.
[dieser hier](https://docs.microsoft.com/en-us/security-updates/SecurityBulletins/2006/ms06-032)).
Bei [manchen Schwachstellen in Netzwerkprotokollen](https://docs.microsoft.com/en-us/security-updates/SecurityBulletins/2008/ms08-001)
ist sie allerdings immer noch schutzlos, weil [die Firewall löchrig
ist](https://www.kb.cert.org/vuls/id/115083). (Zudem hofft Alice, dass
das Aktivieren der Firewall nicht allzu viele zusätzliche Schwachstellen
aufreißt.)

**Backups.** Alice sichert die ihr wichtigen
Dateien regelmäßig auf einer externen USB-Festplatte. Falls sie ihren
Rechner nach einem Angriff neu aufsetzen muss, sind keine wichtigen
Daten verloren. Selbst wenn sie nicht angegriffen wird, werden ihre
Festplatten (auch die externe für die Backups) irgendwann den Geist
aufgeben (es handelt sich um rotierende Magnetplatten mit allerhand
Feinstmechanik, die natürlichem Verschleiß unterliegt). Zudem prüft sie
gelegentlich, ob die Backups noch lesbar sind. Dies gilt ebenfalls für
Backups auf CD oder DVD.

**Virenscanner, Antivirus-Software.** Unter GNU/Linux benötigen
Alice und Bob (noch?) keinen Virenscanner – für andere Menschen mag das
anders aussehen.  Unter dem Betriebssystem, das
auf den meisten PCs vorinstalliert ist, habe ich mich (solange ich
selbst noch solche Rechner verantwortet habe) auf den
mitgelieferten Virenscanner in Kombination mit
[EMET](https://www.microsoft.com/en-us/download/details.aspx?id=50766)
verlassen,
obwohl der mitgelieferte Virenscanner in Tests regelmäßig nicht zur
Spitzengruppe gehört. Beide (Virenscanner und EMET) verfolgen das Ziel,
vor schädlichen Inhalten aus dem Internet zu schützen (z. B. Viren in
E-Mails, Trojaner in heruntergeladener Software, Angriffs-Code in
Office-Dokumenten, PDF-Dokumenten und Web-Seiten). Der Schutz von
Virenscannern wirkt natürlich nur, wenn die Angriffsmuster bereits
bekannt sind; EMET aktiviert zusätzliche Schutzmaßnahmen. Dennoch
benutzt Alice ihren gesunden Menschenverstand und ist vorsichtig mit
Inhalten aus unbekannten Quellen. Gegen gezielte Angriffe wie
beispielsweise [auf Pro-Tibet-Gruppen in
2008](https://www.heise.de/security/meldung/Trojaner-Angriffe-auf-Pro-Tibet-Gruppen-192193.html)
und
[2015](https://netzpolitik.org/2015/citizenlab-analysiert-malware-angriffe-auf-aktivistengruppen-aus-hong-kong-und-tibet/)
bleibt sie allerdings schutzlos … Weitergehende Sicherheit ist
experimentierfreudigen Menschen vorbehalten, die etwa
[SELinux](https://de.wikipedia.org/wiki/SELinux) oder
[Qubes OS](https://www.qubes-os.org/) einsetzen.

Generell empfehle ich natürlich, Windows überhaupt zu nicht verwenden,
sondern [freier Software][freie-software] den Vorzug zu geben.
Unfreie Virus-Scanner sind gefährlich, vor Sicherheitslücken in
denjenigen von
[Symantec und Norton warnte im Juli 2016 das US-CERT](https://www.us-cert.gov/ncas/alerts/TA16-187A),
im November 2016 dann auch
[das Bundesamt für Sicherheit in der Informationstechnik](https://www.bsi-fuer-buerger.de/SharedDocs/Warnmeldungen/DE/TW/2016/warnmeldung_tw-t16-0135.html).
Zum Jahreswechsel 2016/17 machte
[Kaspersky Negativschlagzeilen](https://www.heise.de/security/meldung/Kaspersky-torpediert-SSL-Zertifikatspruefung-3587871.html),
im [Dezember 2017 auch Microsoft](https://www.heise.de/security/meldung/Notfall-Patch-fuer-Windows-Co-Kritische-Sicherheitsluecke-im-Virenscanner-von-Microsoft-3913800.html).

Auch Browser-Hersteller wie Google (Chrome) und Mozilla (Firefox) halten
viele Arten von
[Antivirus-Software für schädlich, wie ein kurzer Artikel auf heise online im Januar 2017](https://www.heise.de/security/artikel/Ex-Firefox-Entwickler-raet-zur-De-Installation-von-AV-Software-3609009.html)
nachdrücklich dokumentiert.

<a name="noscript"></a>**Browser mit NoScript und Werbe-Blocker.**
Alice verwendet unterschiedliche Browser, nämlich
den [Tor Browser][tor] für das
„normale“, anonymisierte Surfen und den Firefox mit nachfolgender
manueller Konfiguration für spezielle Fälle.

Sie benutzt zunächst den Menüeintrag Bearbeiten → Einstellungen →
Datenschutz → „Firefox wird eine Chronik: nach benutzerdefinierten
Einstellungen anlegen“ mit der Auswahl „Die Chronik löschen, wenn
Firefox geschlossen wird“, um zumindest Cookies, Cache und sonstige
Spuren ihres Web-Lebens regelmäßig zu löschen. Bob favorisiert hier:
„Firefox wird eine Chronik: niemals anlegen“

Was Alice tun muss, um Kontrolle über Caching, Cookies, Web-Bugs und
aktive Inhalte zu gewinnen, hängt von ihrer Wahl des
Anonymisierungsdienstes ab:
[JAP](http://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
empfiehlt eine andere Konfiguration als
[Tor](https://www.torproject.org/). In jedem Fall vergewissert sie
sich, dass die Firefox-Erweiterung **[NoScript](http://noscript.net/)** installiert ist,
weil diese aktive Inhalte solange blockiert, bis Alice sie explizit
erlaubt. Alice installiert
[NoScript hier](https://addons.mozilla.org/de/firefox/addon/noscript/)
per [HTTPS][ssl-tls].

Wer den [Tor Browser][tor] einsetzt,
muss selbst entscheiden, ob NoScript aktiviert sein soll oder nicht, wie
im [Abschnitt zu Tor][tor] dargelegt
wird. Alice und Bob haben sich entschieden, NoScript zu aktivieren; zum
einen reduziert dies die Gefahr, Opfer eines
[Drive-By-Downloads](https://de.wikipedia.org/wiki/Drive-by-Download)
zu werden, zum anderen stellt der Browser dann weniger Informationen zur
Verfügung, die [Fingerabdrücke][fingerprint]
aussagekräftiger werden lassen.
Sie sorgen über den Sicherheitsschieberegler im Tor Browser dafür, dass die
Einstellung „Skripte allgemein erlauben (nicht empfohlen)“ *nicht* aktiviert
ist, da NoScript sonst ausgeschaltet ist.

<a name="drive-by-download"></a>NoScript ist deshalb so wichtig, weil
moderne Browser mit ihren zahlreichen Medientypen, Plugins und aktiven
Inhalten (für Java- und Flash-Spiele, interaktive Seiten, Musik, Videos,
Bilder, Zeichensätze und PDF-Dokumente) zahlreiche Sicherheitslücken
enthalten, durch die Alice’ Rechner beim Besuch einer beliebigen
Web-Seite in einen Bot/Zombie verwandelt werden kann.  Manche dieser
Sicherheitslücken lassen sich allein durch das Anschauen einer
entsprechend präparierten Web-Seite ausnutzen, wodurch automatisch
beliebige Schad-Software im Hintergrund heruntergeladen und installiert
werden kann, ohne irgendeine Nutzerinteraktion zu erfordern.  Diese
automatische Installation von Schad-Software wird als
[Drive-by-Download](https://de.wikipedia.org/wiki/Drive-by-Download)
bezeichnet und stellt eine ernstzunehmende Gefahr dar.  Da die meisten
der zugrunde liegenden Sicherheitslücken von JavaScript bereitgestellte
Funktionalität voraussetzen und da JavaScript von NoScript deaktiviert
wird, kann NoScript viele Arten von Angriffen auf den Browser
verhindern.

<a name="malvertising"></a>
**Ad-Blocker/Werbe-Blocker gegen Malvertising.**
In ihrem „normalen“ Browser (also nicht dem
Tor Browser, siehe
[FAQ-Eintrag zu Erweiterungen](https://www.torproject.org/docs/faq.html.en#TBBOtherExtensions))
installiert Alice zudem die Firefox-Erweiterung
[**uBlock Origin**](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
als Ad-Blocker (Werbe-Blocker, Tracking-Blocker), der im Zusammenhang mit
[Web-Bugs][web-bugs] erwähnt wurde.  Zunächst verhindert
dies, dass zahlreiche bekannte (und unbekanntere) Werbenetze
Informationen über Alice sammeln.  Andererseits erhöht dies die
PC-Sicherheit, da über kompromittierte Werbenetze
[seit langem (2008)](https://www.heise.de/security/meldung/Schaedliche-Werbebanner-auf-populaeren-Webseiten-175987.html)
und [immer wieder (2010)](https://www.heise.de/security/meldung/Webseiten-verteilen-Malware-ueber-gehackte-OpenX-Server-1078897.html)
Schadprogramme wie
[Viren und Trojaner per Drive-by-Download (2012)](https://www.heise.de/security/meldung/Weiterhin-Virenalarm-auf-Wetter-com-1576132.html)
in Werbebannern verteilt werden, wovor auch das
[Bundesamt für Sicherheit in der Informationstechnik warnt (2013)](https://www.bsi.bund.de/DE/Presse/Pressemitteilungen/Presse2013/Verteilung_von_Schadprogrammen_ueber_Werbebanne_05042013.html)
und [wovor noch nicht einmal Malvertising-Experten gefeit sind (2015)](https://pagefair.com/blog/2015/halloween-security-breach/).

Diese Problematik hat sich mittlerweile eher verschärft, da
Schad-Software nicht mehr nur über kompromittierte Werbenetze verteilt
wird, sondern in von kriminellen regulär bezahlten Anzeigen als
Werbebanner ausgeliefert werden, weshalb es mit
[*Malvertising*](https://en.wikipedia.org/wiki/Malvertising) inzwischen
einen eigenen Begriff für das Ausliefern von Malware über Werbung
(Advertising) gibt.  Beispiele solcher Malware wurden bei
[Zedo und der Google-Tochter DoubleClick (2014)](https://www.heise.de/security/meldung/Doubleclick-und-Zedo-lieferten-virenverseuchte-Werbung-aus-2400733.html),
bei [Yahoo! (2015)](https://blog.malwarebytes.org/malvertising-2/2015/08/large-malvertising-campaign-takes-on-yahoo/),
bei [MSN.com (2015)](https://blog.malwarebytes.org/malvertising-2/2015/08/angler-exploit-kit-strikes-on-msn-com-via-malvertising-campaign/),
bei [Google Ads (2015)](https://www.heise.de/security/meldung/Nuclear-Exploit-Kit-mit-Google-Ads-ausgeliefert-2596908.html)
wieder bei [DoubleClick sowie weiteren Werbenetzen (2015)](https://blog.malwarebytes.org/malvertising-2/2015/09/large-malvertising-campaign-goes-almost-undetected/),
bei [eBay.de und t-online.de (2015)](https://blog.malwarebytes.org/malvertising-2/2015/10/kampagnen-malvertising-campaign-goes-after-german-users/),
bei [AOL, BBC, New York Times (2016)](https://www.heise.de/security/meldung/Malvertising-Kampagne-Webseiten-von-AOL-BBC-und-MSN-verteilten-Erpressungs-Trojaner-3139150.html)
und wieder bei [DoubleClick (2018)](https://www.heise.de/security/meldung/Malvertising-Googles-Werbeplattform-DoubleClick-fuer-Krypto-Mining-missbraucht-3953253.html)
beobachtet.  Eine im
[Sommer 2016 entdeckte Malvertising-Kampagne verseuchte so diverse Web-Seiten wie New York Times, Playboy, Sky.com, Answers.com und andere, was seit 2013 unentdeckt geblieben sein könnte](https://www.enigmasoftware.com/adgholas-advertising-campaign-infects-thousands-targets-millions-each-day/).
Eine weitere Spielart von Malvertising wurde im Dezember 2016 beschrieben,
die nicht PCs direkt angreift, sondern
[Heim-Router infiziert, von denen ausgehend dann sämtliche heimischen Geräte angegriffen werden](https://www.proofpoint.com/us/threat-insight/post/home-routers-under-attack-malvertising-windows-android-devices).
Aktuelle Fälle von
[Malvertising werden regelmäßig im Blog Malwarebytes.org dokumentiert](https://blog.malwarebytes.org/?s=malvertising).

Auf Werbeeinnahmen angewiesene Medien zeigen sich gegenüber Malvertising
erstaunlich lernresistent:
[So infizierte Forbes im September 2015 seine Besucher durch Malvertising](https://www.fireeye.com/blog/threat-research/2015/09/malvertising_attack.html),
nur um diese
[im Januar 2016 aufzufordern, ihre Ad-Blocker zu deaktivieren und dann direkt wieder Malware auszulieferen](https://www.engadget.com/2016/01/08/you-say-advertising-i-say-block-that-malware/).

Als wäre dies nicht schon schlimm genug, können die
Tracking-Identifikatoren der Werbenetze (z. B. Cookies) auch ohne
Malware von anderen Kriminellen für gezielte Angriffe missbraucht
werden: Im Rahmen der Snowden-Enthüllungen wurde bekannt, dass
[NSA und GHCQ die Cookies von Google als Identifikatoren für Spionagetätigkeiten verwenden](https://www.washingtonpost.com/news/the-switch/wp/2013/12/10/nsa-uses-google-cookies-to-pinpoint-targets-for-hacking/),
was es in Kombination mit dem
[Angriffsprogramm QUANTUM ermöglicht, Rechner zielgerichtet zu übernehmen](https://www.schneier.com/blog/archives/2013/12/nsa_tracks_peop.html).

Als Schlussfolgerung bleibt nur: Ein Werbe-Blocker ist digitale
Selbstverteidigung, und ich empfehle
[uBlock Origin](https://addons.mozilla.org/en-us/firefox/addon/ublock-origin/)
für den Firefox. (Früher hatte ich hier mal Adblock Plus empfohlen. Nach
den
[Recherchen von Sascha Pallenberg zur „nicht aufdringlichen Werbung“](https://www.mobilegeeks.de/adblock-plus-undercover-einblicke-in-ein-mafioeses-werbenetzwerk/),
die auch
[bei Heise zusammengefasst](https://www.heise.de/newsticker/meldung/Adblock-Plus-Weitere-Vorwuerfe-und-Widersprueche-1909535.html)
werden, dann das verbesserte Adblock Edge, das inzwischen durch
[uBlock Origin](https://addons.mozilla.org/en-us/firefox/addon/ublock-origin/)
abgelöst wurde.)

Im September 2017
[empfiehlt auch die Stiftung Warentest sehr deutlich, nicht ohne Tracking-Blocker zu surfen; Testsieger unter verschiedenen Blocker-Alternativen wurde uBlock Origin](https://www.test.de/Tracking-Wie-unser-Surfverhalten-ueberwacht-wird-und-was-dagegen-hilft-5221609-0/).

Nebenbei sei bemerkt, dass diverse Web-Betreiber und -Werber im Herbst
2017 eine weitere Einnahmequelle für sich entdeckt haben,
das Krypto-Mining, und zwar sowohl im Sinne von
[Malvertising](https://www.heise.de/security/meldung/Malvertising-Kampagne-setzt-auf-Krypto-Mining-in-fremden-Browsern-3833536.html)
als auch auf
[„normalen“ Web-Seiten wie Pirate Bay](https://www.heise.de/newsticker/meldung/Pirate-Bay-laesst-heimlich-Krypto-Miner-auf-Nutzer-PCs-laufen-3834222.html).
Beim Krypto-Mining werden unsere Browser missbraucht, um mit unserem
Strom und unserer Rechenleistung Krypto-Währungen wie Bitcoin zu
errechnen.  Während ich diesen Ansatz witzig finde, fragen sich
Kollegen von mir, ob das die Grenze zum Diebstahl nicht überschritten
hat, und offenbar überlegen
[Browser-Hersteller, ob sie uns davor schützen können](https://www.heise.de/security/meldung/Krypto-Mining-im-Browser-Software-Hersteller-wollen-Nutzer-besser-schuetzen-3865577.html).
Mit NoScript kann Ihnen das egal sein, da NoScript das Krypto-Mining
im Browser verhindert.

**Updates.** Alice spielt regelmäßig Updates
ihrer Software ein, da diese oft Sicherheitslücken schließen. Sie macht
dies nicht nur für das Betriebssystem, sondern achtet auf zusätzlich
installierte Programme (z. B. den Browser und alle seine Plugins,
Virenscanner, Office-Produkte, PDF-Viewer, Video-Player, Java).
Notorisch [gefährliche Software wie Adobe Flash, die regelmäßig für
Angriffe ausgenutzt
wird](https://www.heise.de/security/meldung/Der-Liebling-aller-Cyber-Kriminellen-Flash-3157553.html),
verwendet sie gar nicht.

**Benutzerverwaltung.** Alice richtet eine
Kennung ohne Administratorrechte ein, die sie zur normalen Arbeit
verwendet, um einem Angreifer, der durch eine Lücke im Browser oder
PDF-Viewer eindringt, nicht sofort vollen Zugriff auf ihren PC zu
gewähren.  Vor allem bei Online-Diensten sichert sie alle Kennungen
mit *guten* Passwörtern
– [„hallo“, „ficken“, „passwort“ und „qwertz“](https://hpi.de/news/jahrgaenge/2016/hpi-wissenschaftler-ermitteln-die-zehn-meistgenutzten-deutschsprachigen-passwoerter.html)
gehören nicht dazu.  Alice hat übrigens kein Problem damit, kryptische
Passwörter auf Papier aufzuschreiben, da sie sich in erster Linie vor
Online-Angreifern schützen will.

Es gibt verschiedene Vorgehensweisen, um *gute* und trotzdem merkbare
Passwörter zu generieren, etwa basierend auf einem
[*zufälligen* Master-Passwort, das Sie für jeden Dienst nach einem nur Ihnen bekannten Schema modifizieren](https://www.heise.de/security/artikel/Passwort-Schutz-fuer-jeden-1792413.html),
oder
[Schneiers Schema basierend auf den Anfangsbuchstaben und Satzzeichen einer Phrase, die Sie nie vergessen werden](https://www.schneier.com/blog/archives/2014/03/choosing_secure_1.html).

Falls ein Dienst dies unterstützt, können Sie den Schutz von
Passwörtern natürlich durch
[Zwei-Faktor-Authentifizierung](https://de.wikipedia.org/wiki/Zwei-Faktor-Authentifizierung)
erhöhen, wozu neben einem Passwort ein zweiter Faktor genutzt wird.
(Wir alle nutzen Zwei-Faktor-Authentifizierung beim Geldabheben mit
EC-Karte – die PIN-Nummer entspricht einem Passwort, das nur in
Kombination mit dem zweiten Faktor, der EC-Karte, verwendet werden
kann.)  Verschiedene Web-Dienste unterstützen
Zwei-Faktor-Authentifizierung in Form von Passwort und SMS-Versand.
Wenn ein Dienst-Anbieter Ihre Handy-Nummer schon kennt, können Sie
durch Zwei-Faktor-Authentifizierung nur gewinnen.

Alice und Bob verwenden übrigens keine Passwort-Manager, deren
Sicherheit sie nicht verstehen.  Im
[März und April 2017 wurden beispielsweise mehrere Sicherheitslücken im Passwort-Manager LastPass gefunden](https://www.heise.de/security/meldung/Weitere-Luecke-in-LastPass-geschlossen-neue-Version-verfuegbar-3672957.html),
in [2016 und im Dezember 2017 auch im mit Windows 10 automatisch installierten Passwort-Manager Keeper](https://www.heise.de/security/meldung/Windows-10-Luecke-in-vorinstalliertem-Passwort-Manager-Keeper-gibt-Kennwoerter-preis-3921316.html).
Weiter oben erwähnte ich bereits, dass das Aufschreiben von
Passwörtern keine schlechte Idee ist.  Von
[Krypto-Guru Martin Hellman](https://de.wikipedia.org/wiki/Martin_Hellman)
gibt es dazu das überlegenswerte Zitat:
„[Write down your password. Your wallet is a lot more secure than your computer.](https://www.technologyreview.com/s/405473/calling-cryptographers/)“
(Schreib dein Passwort auf.  Deine Brieftasche ist viel sicherer als
dein Computer.)  Auch der IT-Sicherheitsexperte
[Bruce Schneier hält das für einen guten Ratschlag](https://www.schneier.com/blog/archives/2005/06/write_down_your.html).

Allerdings gibt es auch namhafte Experten, die Passwort-Manager
empfehlen.
[Bruce Schneier gehört auch hier dazu](https://www.schneier.com/blog/archives/2014/09/security_of_pas.html) :)

Biometrie ist dagegen mit Vorsicht zu genießen.  Wenn Ihre
Fingerabdrücke oder ein Foto Ihrer Iris einmal in falsche Hände
gerät, haben Sie keine Chance, diese biometrischen Merkmale zu
ändern.  Der [Iris-Scanner im Samsung-Handy Galaxy S8 taugt beispielsweise nichts](https://www.ccc.de/de/updates/2017/iriden).
Logisch, oder?

**WPA2.** Wenn Alice zu Hause WLAN einrichtet,
dann neben dem offenen [Freifunk](http://freifunk.net/) nur mit dem
Verschlüsselungsmechanismus WPA2.  Sie schaltet dabei das
[völlig unsichere WPS](https://www.heise.de/security/meldung/Massive-WLAN-Sicherheitsluecke-1401820.html)
ab.

**Festplattenverschlüsselung (optional).** Die
Festplatten, USB-Sticks, Notebooks, die Alice auf Reisen mitnimmt,
verschlüsselt sie unter Einsatz der Bordmittel ihres Betriebssystems
oder[loop-aes](http://loop-aes.sourceforge.net/loop-AES.README).

![Warnung](./public/icons/stop.png){:.logo} Ich meine das ernst: Wer den eigenen PC
nicht vernünftig absichert, braucht sich keine Gedanken über
Anonymisierungstechniken zu machen.


<a name="ssl-tls"></a>
## Verschlüsselte Verbindungen mit SSL/TLS – asymmetrische Kryptographie

Verschlüsselung ist ein uralter Ansatz, um die
[Vertraulichkeit](https://de.wikipedia.org/wiki/Vertraulichkeit) von
Nachrichten zu gewährleisten, also den Nachrichteninhalt vor
Außenstehenden zu verbergen. Heutzutage stellt die
[Kryptographie](https://de.wikipedia.org/wiki/Kryptographie)
vielfältige Techniken zur Verfügung, um die Vertraulichkeit und auch die
[Integrität](https://de.wikipedia.org/wiki/Integrit%C3%A4t_(Informationssicherheit))
(Nachweis unbefugter Modifikationen) von Nachrichten und Dateien
sicherzustellen und gehört daher zu den Grundtechniken der digitalen
Selbstverteidigung.

Gängige kryptographische Techniken werden durch offene, standardisierte
Protokolle beschrieben, und zu den wohl am häufigsten (weil auch
unbewusst) benutzten Protokollen gehören SSL und das Nachfolgeprotokoll
[TLS](https://de.wikipedia.org/wiki/Transport_Layer_Security).
[SSL wird spätestens seit Juni 2015 als unsicher angesehen](https://tools.ietf.org/html/rfc7568),
weshalb nur noch TLS
eingesetzt werden sollte; da der Name SSL aber weit verbreitet ist,
schreibe ich im Folgenden „SSL/TLS“. Beide Protokolle haben das Ziel,
z. B. beim Online-Banking und -Shopping Ihre Kontodaten zu sichern, wenn
die Adresse im Browser mit `https://` beginnt, wobei das unscheinbare
„`s`“ hinter „`http`“ für „Sicherheit“ steht. Wann immer Sie eine
Web-Seite mit `https://` (statt `http://`) abrufen, kommt das durch
SSL/TLS abgesicherte
[HTTPS](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol_Secure)
zum Einsatz (statt des ungeschützten HTTP) und verspricht (ohne
Garantie, wie ich nachfolgend erläutere) Vertraulichkeit und Integrität:
Die Kommunikation zwischen Browser und Web-Server ist verschlüsselt und
sollte für Dritte daher aussehen wie eine zufällige, unverständliche
Zeichenfolge; darüber hinaus sollten beide bemerken können, wenn Dritte
Daten modifizieren (z. B. die Kontonummer des Empfängers einer
Überweisung).

Wichtig für Alice und Bob sind neben HTTPS insbesondere POPS und IMAPS
zum gesicherten Abruf ihrer E-Mails im
[Thunderbird](https://www.thunderbird.net/de/) und SMTPS zum
gesicherten Versand.

Im Wesentlichen soll der Schutz von SSL/TLS automatisch funktionieren.
Ganz so einfach ist es aber nicht.

Wenn Alice und Bob SSL/TLS einsetzten, werden die Daten verschlüsselt.
So weit so gut. Es bestehen aber folgende Kernprobleme:

1.  Alice und Bob wissen nicht, *für wen* die
    Daten verschlüsselt werden. Sie hoffen zwar, dass die Daten für den
    beabsichtigten Empfänger verschlüsselt werden (z. B. für den
    Web-Server). Sie haben aber kaum eine Möglichkeit nachzuprüfen, ob
    sie einem
    [Man-in-the-Middle-Angriff](https://de.wikipedia.org/wiki/Man-in-the-middle-Angriff)
    zum Opfer fallen.

    Weiter unten begründe ich diese Sichtweise.
2.  Selbst wenn kein Man-in-the-Middle-Angriff vorliegt, wissen Alice
    und Bob nicht, wer außer dem beabsichtigten Empfänger die
    Kommunikation noch entschlüsseln kann:
    -   Browser und Web-Server handeln untereinander aus, welche
        kryptographischen Verfahren genau eingesetzt werden. Es könnte
        sein, dass die ausgehandelten Verfahren *wirkungslos* sind und daher
        gar keinen Schutz bieten.

        Sie können wirkungslos sein, weil sie

        -   beabsichtigt mit einer Hintertür entworfen worden sind,
        -   mit einer Hintertür implementiert worden sind (obwohl das
            Verfahren an sich sicher ist)
        -   oder weil sie von Anfang an unsicher waren, was erst später
            bemerkt wurde, aber seitdem nicht publiziert worden ist.

        Diese drei Aussagen ergeben sich im Wesentlichen aus
        [Snowden-Dokumenten, die im September 2013 publik
        wurden](https://www.propublica.org/article/the-nsas-secret-campaign-to-crack-undermine-internet-encryption).

    -   Zur Verschlüsselung werden Zufallszahlen benötigt. So wie ein
        Zahlenschloss mit wenigen (sagen wir 42) möglichen Kombinationen
        keinen Schutz bietet, bietet Verschlüsselung ohne zufällige
        „Zufalls“zahlen keinen Schutz. (Beide scheinen bei
        oberflächlicher Betrachtung zu wirken; einem ernsthaften Angriff
        können sie aber nicht standhalten.) Die Erzeugung von
        „Zufalls“zahlen ist extrem schwierig. Einerseits werden dabei
        [immer](http://www.debian.org/security/2008/dsa-1571)
        [wieder](https://www.heise.de/security/meldung/Schwache-Schluessel-bei-NetBSD-1829052.html)
        [Fehler](https://www.heise.de/security/meldung/RSA-Schluessel-zertifizierter-Smartcards-geknackt-1959704.html)
        gemacht, zum anderen werden offenbar [Verfahren mit Hintertür
        standardisiert](https://arstechnica.com/information-technology/2013/09/new-york-times-provides-new-details-about-nsa-backdoor-in-crypto-spec/).
    -   <a name="lavabit"></a>Es könnte sein, dass ein als Empfänger oder als
        Kommunikationsdienste anbietendes Unternehmen gesetzlich
        verpflichtet worden ist, Ver- und Entschlüsselungsschlüssel bei
        Behörden zu hinterlegen. Und zwar ohne die Gesprächspartner
        informieren zu dürfen. Unternehmen in demokratischen Staaten.
        Höchst schockierend, erstaunlich und bemerkenswert.

        Wenige Dienstbetreiber wie Ladar Levison, der Gründer der Firma
        Lavabit für verschlüsselte E-Mail-Kommunikation,
		[schließen dann lieber ihr Unternehmen](https://www.theguardian.com/commentisfree/2014/may/20/why-did-lavabit-shut-down-snowden-email),
        als dass sie ihre Kunden hintergehen würden. Offenbar müssen
        sämtliche US-Unternehmen ihre Kunden derart ausspionieren.
        Levison rät davon ab, private Daten US-basierten Unternehmen
        anzuvertrauen. ([„I would strongly recommend against anyone trusting their private data to a company with physical ties to the United States.“](https://www.theguardian.com/technology/2013/aug/08/lavabit-email-shut-down-edward-snowden))

Sind Sie noch da, oder sind Sie hintenübergefallen?

Um es ganz klar zu sagen: Wir wissen nicht, welche Verfahren sicher
sind. Zumindest die NSA kennt selbst entdeckte, selbst eingebaute und in
ihrem Auftrag eingebaute Hintertüren. Das ist Wahnsinn und geht über
mein Thema, informationelle Selbstbestimmung, weit hinaus. Ich möchte
mir lieber nicht vorstellen, was alles durch SSL/TLS „geschützt“ wird,
von Finanztransaktionen bis zur Steuerung von Anlagen in Industrie und
Infrastruktur. Kann irgendeine Organisation mit so viel Macht
verantwortungsbewusst umgehen? Was passiert, wenn dieses Wissen in
weitere Hände gerät?

Der zutiefst beunruhigende Roman
[Blackout](https://de.wikipedia.org/wiki/Blackout_%E2%80%93_Morgen_ist_es_zu_sp%C3%A4t)
von Marc Elsberg zeigt am Beispiel der Stromversorgung, welche
Horrorszenarien auf uns zukommen werden, wenn Sicherheitslücken und
Hintertüren in kritischen Infrastrukturen für gezielte Angriffe
ausgenutzt werden.

Ich schweife ab. Zurück zu obigen Kernproblemen. Alice und Bob können
erklären, was Man-in-the-Middle-Angriffe sind. Im Normalfall nutzen sie
[Anonymisierungstechniken wie Tor][tor] nur für normales Surfen, wo
sie keine Passwörter eingeben müssen. Wenn sie die Nutzung von Diensten
anonymisieren wollen, die die Übertragung von Passwörtern erfordern,
dann setzen sie Mechanismen ein, die sie vor Man-in-the-Middle-Angriffen
schützen, etwa
[manuelles Certificate Pinning](https://blogs.fsfe.org/jens.lechtenboerger/2014/03/10/certificate-pinning-with-gnutls-in-the-mess-of-ssltls/),
was über die nachfolgenden Erläuterungen hinausgeht.


### Asymmetrische Kryptographie

[Asymmetrische Kryptographie](https://de.wikipedia.org/wiki/Asymmetrisches_Kryptosystem)
stellt eine Grundlage vieler Sicherheitsmechanismen im Internet dar,
unter anderem von SSL/TLS und auch vom
[später besprochenen GnuPG zur E-Mail-Verschlüsselung][gnupg].
Zudem beinhaltet Ihr Personalausweis eine Smartcard, die asymmetrische
Verfahren implementiert.  Insofern gehört das Folgende zur
Allgemeinbildung.

Durch Verschlüsselung werden Nachrichten unter Verwendung eines sogenannten
Schlüssels in unverständliche, zufällig aussehende Zeichenketten
verwandelt. Bei *symmetrischen* Verfahren setzen Sender und Empfänger
denselben Schlüssel für Ver- und Entschlüsselung ein.  Demgegenüber muss die
Empfängerin (sic!) bei *asymmetrischer* Verschlüsselung ein Schlüssel*paar*
besitzen.  Ein Schlüsselpaar besteht aus einem geheimen (oder privaten) und
einem öffentlichen Schlüssel.  Wie die Namen schon andeuten, muss die
Besitzerin gut auf den geheimen Schlüssel aufpassen, so dass dieser nie in
fremde Hände gerät, während der öffentliche Schlüssel weitergegeben wird.

Wenn Bob einen öffentlichen Schlüssel von Alice erhalten hat, kann er
mit diesem Nachrichten an Alice verschlüsseln. Nur sie (also nicht
einmal Bob) kann diese Nachrichten dann entschlüsseln, und zwar benötigt
sie dazu ihren geheimen Schlüssel. Der öffentliche Schlüssel entspricht
in etwa einem *geöffneten* Tresor mit
Zahlenkombination: Jede/r kann Nachrichten hineinlegen und den Tresor
verschließen. Geöffnet werden kann der Tresor jedoch nur von Personen,
die die Zahlenkombination (also den geheimen Schlüssel) kennen.

Bei Verwendung von HTTPS im Web sendet der Server zuerst seinen
öffentlichen Schlüssel. Bobs Browser kann seine Daten dann mit diesem
öffentlichen Schlüssel verschlüsseln. (Die Details sind komplex und
beruhen auf
[*hybriden* Verfahren](https://de.wikipedia.org/wiki/Hybride_Verschl%C3%BCsselung),
spielen hier aber keine Rolle.) Eine im Web kaum zu überwindende
Herausforderung besteht nun darin sicherzustellen, dass ein öffentlicher
Schlüssel dem „richtigen“ Empfänger gehört. Andernfalls könnte Bob seine
Nachrichten in einen Tresor legen, der zwar mit „Alice“ beschriftet ist,
der aber von ihrem Ex (der Mallory heißt) aufgestellt worden ist …

<a name="mitm1"></a>Der Ex Mallory wäre dann ein *Man-in-the-Middle*: Er
entnimmt die Nachrichten von Bob an Alice aus dem von ihm aufgestellten
Tresor und hat freie Hand, sie zu lesen oder zu ändern. Dann legt er die
(möglicherweise geänderten) Nachrichten in den echten Tresor von Alice.
Weder Alice noch Bob würden dies bemerken.


### Digitale Zertifikate, Zertifizierungsstellen und Vertrauen

Um der gerade skizzierten Gefahr zu begegnen, einem Man-in-the-Middle
zum Opfer zu fallen, werden
[Public-Key-Infrastrukturen (PKI)](https://de.wikipedia.org/wiki/Public-Key-Infrastruktur)
genutzt. Hier behaupten Zertifizierungsstellen durch
[digitale Signaturen](https://de.wikipedia.org/wiki/Digitale_Signatur)
(welche wiederum mittels asymmetrischer Verfahren erzeugt werden), dass ein
öffentlicher Schlüssel wirklich zu einem bestimmten Empfänger gehört.
(Beim Tresor entspräche eine fälschungsresistente
[Plombe](https://de.wikipedia.org/wiki/Plombe_%28Siegel%29) am Namensschild
einer digitalen Signatur.)

Ein digital signierter öffentlicher Schlüssel wird als
[digitales Zertifikat](https://de.wikipedia.org/wiki/Digitales_Zertifikat)
bezeichnet, und statt eines einfachen öffentlichen Schlüssels (wie oben
vereinfachend dargestellt) sendet ein Web-Server zuerst sein digitales
Zertifikat an Bob. Bobs Browser muss dann überprüfen, ob die digitale
Signatur dieses Zertifikats gültig ist und von einer
„vertrauenswürdigen“ Zertifizierungsstelle stammt. Wie dies im Detail
funktioniert, spielt hier keine Rolle. Wichtig ist, dass der Browser
nach erfolgreicher Prüfung davon ausgeht, dass der öffentliche Schlüssel
des Zertifikats wirklich zum sendenden Web-Server gehört.

<a name="mitm2"></a>Alice’ Ex Mallory hat nun noch (mindestens) zwei
Möglichkeiten, um trotzdem erfolgreich als *Man-in-the-Middle*
aufzutreten. Zum einen kann er einfach selbst eine Plombe am Tresor
anbringen. Im Web bedeutet dies, dass er selbst ein Zertifikat erstellt,
in dem seine eigene digitale Signatur bestätigt, der enthaltene
öffentliche Schlüssel (zu dem er auch den geheimen Schlüssel besitzt)
gehöre Alice. Offenbar sollte Bob diesem Zertifikat nicht trauen (die
Plombe sieht irgendwie komisch aus), und sein Browser wird ihn darauf
hinweisen, dass der Aussteller des Zertifikats unbekannt ist. Je nach
Browser und Version sieht Bob einen Hinweis der folgenden Art:

![Firefox:SSL-Warnung](./public/images/2016-ssl-nicht-vertraut.png)

Nach Aufklappen von „Erweitert“ sieht das je nach Problem evtl. so aus:

![Firefox:SSL-Warnung mit Details](./public/images/2016-ssl-nicht-vertraut-details.png)

Wenn Bob so eine Warnung auf einer Web-Seite sieht, wo er persönliche
Daten eingeben soll oder die gar mit Geld zu tun hat (Online-Banking
oder -Shopping), bricht er die Verbindung ab. Ansonsten würden seine
Daten und sein Geld von einem Man-in-the-Middle gestohlen.

Wenn Bob aber eine Web-Seite *ansehen* möchte,
*ohne* persönliche Daten oder gar Passwörter
einzugeben, ignoriert er solche Warnungen, da er die Web-Seite auch ohne
SSL/TLS angesehen hätte. Dass hier mit einer dem Browser unbekannten
Partei verschlüsselt wird, schadet sicher nicht.

Außerdem akzeptiert Bob natürlich die Zertifikate, die er für seine
eigenen Server selbst erstellt hat, etwa den
[anderswo erwähnten ownCloud- oder Nextcloud-Server][owncloud].
Einerseits kann Bob über [Let’s Encrypt](https://letsencrypt.org/de/)
kostenlose Zertifikate für Domänen erhalten, die er kontrolliert.
Andererseits kann er auch selbst als Zertifizierungsstelle arbeiten
und sein eigenes Wurzelzertifikat verwenden. Schließlich kann er
selbst-signierte Zertifikate verwenden, die allerdings zu
Browser-Warnungen führen und nur nach Kontrolle der
Zertifikat-Fingerabdrücke verwendet werden dürfen („Ausnahmen
hinzufügen…“ / „Ansehen…“ / „SHA1-Fingerabdruck“; dann
„Sicherheits-Ausnahmeregel bestätigen“).

Zurück zu Alice’ Ex Mallory. Sein Versuch, eine eigene Plombe an seinem
eigenen Tresor anzubringen, schlägt bei Alice und Bob fehl, da beide
genau hinsehen und obige Browser-Warnungen beachten. Seine zweite
Möglichkeit besteht darin, eine offizielle Plombe an seinem mit „Alice“
beschrifteten Tresor anbringen zu lassen. Er kennt dafür drei Wege.
Erstens kann er in die Computer einer anerkannten Zertifizierungsstelle
einbrechen und sich dort selbst ein offizielles Zertifikat erstellen. So
etwas ist in großem Stil [im März 2011 bei Comodo](https://www.heise.de/security/meldung/SSL-GAU-zwingt-Browser-Hersteller-zu-Updates-1212986.html)
geschehen und auch in 2011 [für DigiNotar](https://www.heise.de/security/meldung/Der-Diginotar-SSL-Gau-und-seine-Folgen-1423893.html)
bekannt geworden.  Im
[Juli 2016 wurde ein weiterer haarsträubender Fall bei Comodo entdeckt](https://www.heise.de/newsticker/meldung/HTML-Injection-Luecke-erlaubte-Zertifikatsklau-bei-Comodo-3282183.html).
Wie oft derartiges unbemerkt vorkommt, ist natürlich unbekannt.

Zweitens kann Mallory auch ganz offiziell eine Zertifizierungsstelle um
das Ausstellen eines Zertifikats bitten, wiederum mit einigen Varianten.
(a) Vielleicht kennt er eine Zertifizierungsstelle, die nicht richtig
prüft, was sie da zertifiziert. Diese übersieht dann, dass sie einen
Tresor mit Namen „Alice“ für jemanden verplombt, der nicht Alice ist. So
etwas kommt [immer (2001, VeriSign)](https://docs.microsoft.com/en-us/security-updates/SecurityBulletins/2001/ms01-017)
[mal (2006, GeoTrust)](https://www.heise.de/newsticker/meldung/Phishing-mit-gueltigen-SSL-Zertifikaten-175563.html)
[wieder (Comodo, Symantec, 2015)](https://www.heise.de/security/meldung/SSL-Zertifizierungsstellen-stellen-hunderte-Zertifikate-fuer-Phishing-Seiten-aus-2848793.html)
und [wieder (WoSign, 2016)](https://www.heise.de/security/meldung/Kostenlos-CA-WoSign-in-der-Kritik-Mozilla-erwaegt-Schritte-3312300.html)
und [wieder (Comodo, 2016)](https://www.heise.de/security/meldung/Zertifikats-Klau-Fatale-Sehschwaeche-bei-Comodo-3354229.html)
und [wieder (Symantec, 2017)](https://arstechnica.com/information-technology/2017/01/already-on-probation-symantec-issues-more-illegit-https-certificates/)
vor.

(b) Es gibt Zertifizierungsstellen
[wie Trustwave, die Schnüffel-Zertifikate dreist verkaufen (2012)](https://www.heise.de/security/meldung/Trustwave-verkaufte-Man-in-the-Middle-Zertifikat-1429722.html).
Und es gibt Zertifizierungsstellen
[wie Thawte, die „interne Test-Zertifikate“ ausstellen, die dann doch für andere Zwecke missbraucht werden (2015)](https://security.googleblog.com/2015/09/improved-digital-certificate-security.html).
Man glaubt es nicht, ist aber so.

(c) Mallory kann, wenn er mächtig genug ist, auch eine eigene
Zertifizierungsstelle betreiben. Die Hintergründe sind zwar nicht
bekannt, aber für mich sehen die „Fälle“
[ANSSI (2013)](https://blog.mozilla.org/security/2013/12/09/revoking-trust-in-one-anssi-certificate/),
[NIC of India (2014)](https://www.heise.de/security/meldung/Indien-stellte-falsche-Google-Zertifikate-aus-2252544.html)
und [CNNIC (2015)](https://security.googleblog.com/2015/03/maintaining-digital-certificate-security.html)
nach staatlicher Spionage aus.
[Kasachstan macht das in 2019 auch ganz offiziell, nachdem vorige Bemühungen in 2012 und 2015 noch gescheitert waren.](https://netzpolitik.org/2019/kasachstan-zwingt-nutzer-zur-installation-von-ueberwachungszertifikat/)

Drittens kann Mallory eine inoffizielle Zertifizierungsstelle betreiben
und deren Wurzelzertifikat auf Rechnern installieren, in die er
eingebrochen ist. Es gibt
[Online-Banking-Trojaner (2014)](https://www.heise.de/security/meldung/Banking-Trojaner-Infektion-ohne-Trojaner-2265585.html),
die so vorgehen.

Es gibt auch PC-Hersteller, die ihre Geräte mit vorinstallierten
Spionagewurzelzertifikaten ausliefern wie
[Dell (2015)](https://www.heise.de/security/meldung/Dell-verschlimmbessert-die-Foundation-Services-Luecke-3029552.html)
und [Lenovo (2015)](https://www.heise.de/security/meldung/Auch-Lenovo-liefert-CA-Zertifikate-auf-seinen-Rechnern-aus-3044205.html).

Schauen Sie einfach mal in Ihrem Browser nach, welchen
Zertifizierungsstellen der so „vertraut“. Im Firefox wählen Sie dazu das
Menü „Bearbeiten“ / „Einstellungen“ / „Erweitert“ / „Zertifikate“ /
„Zertifikate anzeigen“ / „Zertifizierungsstellen“; nur sechs läppische
Klicks, fast direkt zu sehen. Scrollen Sie mal drüber, wen Sie da
kennen, dem Ihr Browser „vertraut“. Wem trauen Sie (ohne
Anführungszeichen, im wörtlichen Sinne)? Alternativ schauen Sie sich
doch diese [„übersichtliche“ Darstellung von ca. 650
Zertifizierungsstellen](https://www.eff.org/files/colour_map_of_cas.pdf)
und deren „Vertrauens“beziehungen an. Eine Legende finden Sie auf der
Seite des [SSL Observatory](https://www.eff.org/observatory).

Wenn Mallory nur eine einzige Zertifizierungsstelle überredet, zwingt,
bezahlt oder betreibt, erzeugt diese eine Plombe für den Tresor mit
Namen „Alice“, ohne dass der Browser eine Warnung anzeigen würde.

Eine einzige Zertifizierungsstelle genügt ihm.

Es reicht, das schwächste Glied der Kette aufzubrechen.

Um es auf den Punkt zu bringen: Das „Vertrauensmodell“ von SSL/TLS ist
kaputt. Im Jargon ist wohl „FUBAR“ der treffende Begriff.
[metageren](https://metager.de/meta/meta.ger3?eingabe=fubar&wikiboost=on),
[startpagen](https://startpage.com/do/search?q=fubar),
[searXen](https://searx.org/?q=fubar&categories=general&language=de) oder
[unbubblen](https://www.unbubble.eu/?q=fubar&focus=web&lang=de-DE)
Sie das. Alice
und Bob wissen nicht, warum eine Zertifizierungsstelle welches
Zertifikat für wen ausgestellt hat. Nach eingehender Prüfung für ihre
Bank? Oder nach Einbruch eines Kriminellen im Namen der Bank für sich
selbst? Oder für einen Kriminellen nach Bestechung? Oder für eine
Regierung im Namen der Terrorabwehr? Alice und Bob wissen auch nicht,
warum welcher Browser welcher Zertifizierungsstelle „vertraut“.

(Hoffnung auf Besserung liegt im experimentellen Internet-Protokoll
[Certificate Transparency](https://tools.ietf.org/html/rfc6962). Grob
gesprochen sollen hier öffentliche, verifizierbare Logbücher sämtlicher
Zertifikate geführt werden. Wenn eine Zertifizierungsstelle dann
plötzlich anfängt, Zertifikate für Domänen auszustellen, für die sie gar
nicht zuständig ist, könnte der Browser die Verbindung abbrechen. Im
[Oktober 2015 erhielt Certificate Transparency einen neuen Schub durch
Google](https://security.googleblog.com/2015/10/sustaining-digital-certificate-security.html).
[Seit September 2016 unterstützt Firefox diesen Ansatz](https://bugzilla.mozilla.org/show_bug.cgi?id=944175),
[Certificate Transparency wird anfänglich jedoch noch ausgeschaltet sein](https://wiki.mozilla.org/PKI:CT).)

Um einen Man-in-the-Middle-Angriff durchzuführen, fehlt noch ein Detail.
Mallory muss die Briefe von Bob zu seinem gefälschten Tresor locken. Im
Internet bedeutet dies, dass Bobs Kommunikation durch einen von Mallory
kontrollierten Rechner fließen muss, damit er Bob ein fingiertes
Zertifikat unterschieben kann, so dass Bob die Kommunikation an Mallory
verschlüsselt, ohne dies zu merken. Mallory liest und ändert die
Kommunikation dann nach Belieben, bevor er sie erneut verschlüsselt in
Bobs Namen an das eigentliche Ziel weiterleitet.

Mallory hat gute Karten, wenn er
([WLAN-](https://www.heise.de/ct/ausgabe/2013-21-Hinter-den-Kulissen-eines-Router-Botnets-2313886.html)
oder
[„echte“](https://www.heise.de/newsticker/meldung/Britischer-Geheimdienst-GCHQ-steckt-offenbar-hinter-Cyber-Angriff-auf-Belgacom-1962497.html))
[Router][grundlagen] oder
[Internet-Knoten](https://de.wikipedia.org/wiki/Internet-Knoten)
kontrolliert. Außerdem könnte er versuchen, ein für Bob präpariertes
WLAN aufzusetzen.

Wenn Bob Anonymisierungsdienste benutzt, ist klar, dass seine
Kommunikation über Rechner dieser Dienste abgewickelt wird. Dann würde
es sich lohnen, solche Rechner zu kontrollieren oder selbst zu
betreiben.

Möglichkeiten in Hülle und Fülle.

Seien Sie so vorsichtig wie Alice und Bob, was die „Sicherheit“ von
SSL/TLS betrifft, insbesondere wenn Sie
[anonymisiert über Tor surfen][tor], wo Mallory ohne
vorherige Prüfung Mitbetreiber des Anonymisierungsdienstes werden und so
auf einfache Weise Internet-Kommunikation zu sich lenken kann!


### Browser-Erweiterungen zu SSL/TLS

Angesichts des katastrophalen Sicherheitszustandes von SSL/TLS gab und
gibt es Ansätze, die mittels Firefox-Erweiterungen eine Verbesserung
anstreben.  Das früher hier beschriebene Certificate Patrol wird nicht
mehr weiterentwickelt, weshalb seit Ende 2018 hier nur noch
[HTTPS Everywhere](https://www.eff.org/https-everywhere)
genannt wird.  HTTPS Everywhere ist eine freie
Firefox-Erweiterung, die von der [Electronic Frontier Foundation](https://www.eff.org/) und dem
[Tor-Projekt](https://www.torproject.org/) entwickelt wird. Sie ist im
[Tor Browser][tor] bereits enthalten.

HTTPS Everywhere beinhaltet folgende zwei zentrale Funktionalitäten. Zum
einen können viele Web-Server ihre Inhalte sowohl verschlüsselt per
HTTPS als auch unverschlüsselt per HTTP ausliefern.  HTTPS Everywhere
beinhaltet einen Regelsatz für viele solcher Web-Server, so dass der
Browser automatisch die verschlüsselte Verbindung bevorzugt.

Zum anderen unterstützt HTTPS Everwhere das
[SSL-Observatorium](https://www.eff.org/observatory) der Electronic
Frontier Foundation. Dieses Observatorium sammelt SSL/TLS-Zertifikate,
die im Web vorkommen. HTTPS Everwhere kann so konfiguriert werden, dass
der Browser die ihm präsentierten Zertifikate an das Observatorium
schickt; dann soll er
[in Echtzeit Warnungen geben können](https://www.eff.org/deeplinks/2012/02/https-everywhere-decentralized-ssl-observatory),
wenn Sicherheitsprobleme mit Web-Seiten erkennt werden, etwa schwache
SSL/TLS-Schlüssel. (Bei obigem Man-in-the-Middle-Angriff mit der
Deutschen Bank gab es keine weitere Warnung zum
Standard-Firefox-Dialog.) Beachten Sie, dass das Observatorium jedes
Zertifikat erhält (mit Ausnahme einiger konfigurierbarer Details). Wenn
Alice über Tor surft, weiß das Observatorium nur, dass irgendjemand den
zugehörigen Web-Server besucht hat, aber nicht wer. Ohne
Anonymisierungsdienst sähe das Observatorium dagegen die IP-Adresse von
Alice. (Laut [Datenschutzerklärung](https://www.eff.org/policy) werden
die Zertifikate und, abhängig von Konfigurationseinstellungen, das
[autonome System][exkurs] von Alice gespeichert; die
IP-Adresse wird nicht erwähnt.)

<a name="proxies"></a>
## Anonymisierende Proxies: Chaum, JAP/JonDo und Tor

Der Internet-Provider von Alice ist in der Lage, ihre gesamte
Kommunikation zu überwachen, und ihre Kommunikationsdaten weiterzugeben.
Selbst wenn sie möglichst oft ihre Daten mit
[SSL/TLS][ssl-tls] absichert, sieht
er immer noch, welche Dienste sie wie oft in Anspruch nimmt. Um dies zu
verhindern, also weitgehend unbeobachtet oder gar anonym zu surfen und
zu kommunizieren, muss Alice weiterentwickelte Techniken der digitalen
Selbstverteidigung anwenden, mit deren Hilfe sie verbirgt, mit wem sie
kommuniziert.

Ein einfaches, aber auch ungenügendes Verfahren besteht in der
Konfiguration einer einzelnen anonymisierenden Zwischenstation,
[Proxy](https://de.wikipedia.org/wiki/Proxy) oder Mix genannt
(technisch evtl. auch ein VPN-Server), dem Alice ihre
Kommunikationsdaten verschlüsselt zusendet, damit dieser sie an den
eigentlichen Adressaten weiterleitet und dessen Antworten verschlüsselt
an sie zurücksendet. Der Internet-Provider sieht dann nur noch Alice’
verschlüsselte Kommunikation mit dem Proxy. Dieses Verfahren ist
ungenügend, weil nun anstelle des Internet-Providers der Proxy
vollständiges Wissen über Alice’ Kommunikation erlangt. Was er mit
diesem Wissen macht, ist nicht klar. Löschen oder Verkaufen sind zwei
Extreme, und in Zeiten des
[Überwachungskapitalismus][ueberwachungskapitalismus]
drängt sich eine Alternative auf …

Nachdem ich diese beiden Alternativen hier jahrelang als Anregung zum
Weiterdenken nur so genannt hatte,
gibt es im Sommer 2017 auch zwei prominente Vertreter von
Proxy-Betreibern, bei denen das Ausspionieren der Nutzerinnen und Nutzer
in der Öffentlichkeit diskutiert wird:
[Facebook mit dem eingekauften VPN-Dienst Onavo](https://netzpolitik.org/2017/facebook-spioniert-nutzer-seines-vpn-dienstes-aus/)
und [AnchorFree mit dem VPN-Client Hotspot Shield Free](https://www.heise.de/newsticker/meldung/VPN-Anbieter-Aktivisten-beklagen-Datenmissbrauch-3795523.html).

Insbesondere rate ich von der Anonymisierung (!) über
[VPN](https://de.wikipedia.org/wiki/Virtual_Private_Network)-Proxies
ab. VPNs sind nützlich, um aus wenig vertrauenswürdigen oder zensierten Netzen
eine gesicherte Verbindung in ein anderes Netz aufzubauen. Für Anonymisierung
sind sie aber ungeeignet. Zusätzlich zum bereits genannten Aspekt, dass mit
dem Proxy eine in der Regel nicht vertrauenswürdige Partei vollständiges
Wissen über Alice’ Kommunikation erlangt, heißt es in einer
[vom Guardian veröffentlichten XKEYSCORE-Präsentation aus dem Jahre
2008](https://www.theguardian.com/world/interactive/2013/jul/31/nsa-xkeyscore-program-full-presentation),
auf Seite 17: „Zeige mir die Daten aller VPN-Startups aus Land X und gib
mir die Daten, damit ich sie entschlüsseln und die Benutzer aufdecken
kann. Diese Ereignisse können in XKEYSCORE einfach durchgeblättert
werden.“

Letztlich geht es bei VPN-Proxies primär um die „Umleitung“ des
Netzwerkverkehrs. Weitergehende Anonymisierungsüberlegungen, etwa zum
Schutz vor [Fingerprinting][fingerprint], stehen in der
Regel nicht im Fokus.

Die Kernidee echter Anonymisierungsdienste besteht darin, Nachrichten
aufgeteilt in mehrfach verschlüsselte Pakete gleicher Größe über eine
*Kette mehrerer Proxies*, idealerweise
zeitlich verzögert und umgeordnet, zum eigentlichen
Kommunikationspartner weiterzuleiten. Die Beobachtung von Alice’
Kommunikation wird dann nur zeigen, dass ihre Kommunikation
verschlüsselt mit einem von ihr gewählten Proxy stattfindet,
vorzugsweise im nichteuropäischen Ausland: der eigentliche Adressat
bleibt verborgen. Die wegweisende Grundlagenarbeit dieser Verfahren ist
bereits 1981 von David Chaum unter dem Titel [„Untraceable electronic
mail, return addresses, and digital
pseudonyms“](https://www.freehaven.net/anonbib/cache/chaum-mix.html)
veröffentlicht worden.

Heute existieren für E-Mail, Web, Tauschbörsen und allgemeine
TCP/IP-Verbindungen frei verfügbare und einfach zu benutzende
Software-Lösungen, die einen gewissen Grad von Anonymität basierend auf
Netzen von Proxy-Servern bieten. Im Folgenden gehe ich auf
[JAP](https://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
und [Tor](https://www.torproject.org/) zum anonymisierten Web-Surfen
ein. Bei diesen Verfahren bleibt zumindest dem Internet-Provider
verborgen, mit wem Alice kommuniziert, und der Gesprächspartner (z. B.
der Suchmaschinenbetreiber im Internet) erfährt nicht, dass es Alice
ist, mit der er kommuniziert (aus seiner Sicht findet die Kommunikation
mit dem letzten Proxy statt). Darüber hinaus gibt es keine einzelne
Stelle mehr, der Alice ihre gesamte Kommunikation anvertrauen müsste.

Grob gesagt funktionieren diese Verfahren wie folgt, wenn Alice anonym
surfen möchte, ihr Browser also eine anonymisierte Internet-Verbindung
zum Server Sara aufbauen soll: Die Daten, die der Browser eigentlich an
Sara schicken soll, werden mehrfach verschlüsselt in ein
Anonymisierungsnetz geleitet und dort zunächst im Zickzack hin und her
geschickt, wobei die mehrfache Verschlüsselung Schritt für Schritt
aufgehoben wird. Bei der letzten Station im Anonymisierungsnetz, nennen
wir sie Max, liegen dann wieder die Daten vor, die Alice an Sara
schicken wollte, allerdings ohne die
[IP-Adresse][grundlagen] von Alice. Max weiß nichts von
Alice, sondern kennt nur die vorletzte Station im Anonymisierungsnetz.
Er baut nun eine Verbindung zu Sara auf und führt die Kommunikation an
Alice’ Stelle. Er sieht dabei sämtliche zwischen Alice und Sara
ausgetauschten Daten – es sei denn, Alice wählt eine [verschlüsselte
Verbindung][ssl-tls] mit Sara
(z. B. HTTPS, IMAPS); dann sieht Max nur unverständliches Wirrwarr. Die
empfangenen Daten gehen zurück durch das Anonymisierungsnetz, wobei sie
Schritt für Schritt verschlüsselt werden. Alice entschlüsselt am Ende
wieder alles.

Beachten Sie: Alice’ Kommunikation wird also stellvertretend von einem
anderen Rechner irgendwo im Internet geführt. Im Wesentlichen wird dabei
die IP-Adresse von Alice durch die von Max ersetzt, ohne dass Max
irgendetwas von Alice wissen müsste, wodurch eine Anonymisierung
erreicht wird. Wenn Alice sich in ihren Daten selbst identifiziert,
macht sie die Anonymisierung dadurch natürlich zunichte. (Z. B. wenn sie
in einem Web-Formular ihren Namen oder ihre E-Mail-Adresse eingibt. Oder
wenn ihr Browser Cookies verschickt, die ohne Anonymisierung gesetzt
wurden. Oder wenn sie [unsinnigerweise einen Bittorrent-Client
verwendet](https://blog.torproject.org/bittorrent-over-tor-isnt-good-idea),
der ihre eigene IP-Adresse als Teil der Daten durch das
Anonymisierungsnetz sendet.)

Wie im Grundlagenabschnitt zu
[Fingerabdrücken][fingerprint] dargestellt, ist die
IP-Adresse nur *ein* identifizierendes Merkmal
unter vielen. Damit Alice beim Surfen nicht trotz anonymisierter
IP-Adresse durch Fingerprinting-Techniken identifiziert wird, darf sich
ihre Browser-Konfiguration nicht zu sehr von der anderer anonymisierter
Surfer unterscheiden. Dies kann nur funktionieren, wenn sie einen
speziell vorbereiteten Browser wie [Tor
Browser][tor] benutzt und dessen
Konfiguration nur im empfohlenen Rahmen verändert.

<a name="Disclaimer"></a> ![Warnung](./public/icons/stop.png){:.logo} Jedes dieser
Anonymisierungsprojekte befindet sich in der Entwicklung. Keines dieser
Projekte behauptet von sich, Anonymität zu garantieren. Auch ich
behaupte das nicht! Ich halte es aber für besser, ein Verfahren in
Entwicklung einzusetzen, das bekannte (z. B.
[Angriffe auf JAP](https://anon.inf.tu-dresden.de/desc/desc_anon.html)
und [Angriffe auf Tor](https://www.torproject.org/docs/faq.html.en#AttacksOnOnionRouting))
und unbekannte Schwachstellen hat, als gar nichts zu tun.

Insbesondere beabsichtigen diese Projekte *nicht*, vor sogenannten globalen passiven
Angreifern (engl. *global passive
adversaries*) zu schützen. Eine Angreiferin ist passiv, wenn sie
Internet-Kommunikation lediglich beobachtet, aber nicht aktiv
manipuliert. Sie ist global, wenn sie die Internet-Kommunikation nahezu
überall beobachten kann. Zumindest Geheimdienste wie NSA und GHCQ dürfen
als globale Angreifer betrachtet werden; vermutlich beobachten sie nicht
nur passiv, sondern kontrollieren auch „anonymisierende“
Zwischenstationen. Insofern verspricht keines der hier vorgestellten
Projekte Schutz im Rahmen der [Überwachungs- und
Spionageaffäre](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re).

Globale passive Angreifer können das durch
[JAP](http://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
und [Tor](https://www.torproject.org/) „anonymisierte“ Surfen von Alice
auf dem Web-Server Sara wie folgt de-anonymisieren: Sie beobachten die
(verschlüsselte und daher unverständliche) Kommunikation zwischen Alice
und der ersten von ihr (oder ihrer Software) gewählten Zwischenstation,
ebenso wie die Kommunikation zwischen der letzten Zwischenstation und
dem Ziel-Server Sara. Da jede Kommunikation bestimmte Muster aufweist
(z. B. umfasst jede Web-Seite andere Anzahlen und Größen an Bildern,
Stylesheets und Skripten, die in einer bestimmten Reihenfolge übertragen
werden), kann die globale passive Angreiferin versuchen, die bei Alice
und bei Sara auftretenden Kommunikationsmuster miteinander zu
korrelieren (obwohl zumindest diejenigen bei Alice verschlüsselt sind).
Es ist unklar, wie schwierig diese Korrelationsaufgabe wirklich ist.
Alice und Bob gehen davon aus, dass Geheimdienste anonymisierte
Kommunikation besonders interessant finden und daher speichern und mit
dem Ziel der De-Anonymisierung analysieren (was durch aktive Angriffe
vereinfacht wird, wenn ihre Kommunikation über kompromittierte
Zwischenstationen abgewickelt wird).

Ich mache mir zudem Gedanken, dass manche meiner Daten durch
Anonymisierung erst in von ausländischen Geheimdiensten kontrollierte
Gebiete „umgeleitet“ werden. In solchen Fällen würde ich diesen
Geheimdiensten durch meine Anonymisierungsbemühungen den Zugriff auf
meine Daten ermöglichen. Sowohl im JonDonym-Forum (wo der Beitrag nicht
mehr zu existieren scheint) als auch auf der
[Tor-Talk-Mailingliste](https://lists.torproject.org/pipermail/tor-talk/2013-July/#28850)
habe ich im Sommer 2013 entsprechende Diskussionen angestoßen. Kurz
gesagt [empfehle
ich](https://blogs.fsfe.org/jens.lechtenboerger/2013/07/19/how-i-select-tor-guard-nodes-under-global-surveillance/)
(und das ist eine Minderheiten-, wenn nicht Einzelmeinung), eine
„nahegelegene“ erste Zwischenstation auszuwählen. So eine Wahl reduziert
die Gefahr, dass meine Kommunikation mit dieser Zwischenstation durch
feindliches Gebiet verläuft und dort von Geheimdiensten gespeichert und
analysiert wird. Wenn in Ihrer Firma z. B. ein Tor-Server betrieben
wird, sollten Sie diesen als Entry-Guard konfigurieren. Wenn in Ihrer
Firma kein Tor-Server betrieben wird, beantragen Sie doch einen. Das
[Aufsetzen](https://www.torproject.org/docs/tor-relay-debian.html.en)
ist denkbar einfach.

![Warnung](./public/icons/stop.png){:.logo} Bob weist dringend darauf hin, dass bei
Einsatz eines Anonymisierungsverfahrens die eigenen Daten über
zusätzliche Zwischenstationen von Betreibern mit unbekannten Motiven
geleitet werden. Insbesondere im Falle von
[Tor](https://www.torproject.org/) ist bekannt, dass es Betreiber mit
kriminellen Absichten gibt, die versuchen, Kommunikation zu belauschen,
Klartext-Passwörter abzufangen,
[Man-In-The-Middle-Angriffe][mitm1]
auszuführen usw. Zudem wird das Surfen über mehrere Zwischenstationen
durch Erhöhung von Latenzzeiten und Verminderung von Bandbreite
natürlich langsamer …

![Warnung](./public/icons/stop.png){:.logo} **Magie? Fehlanzeige …**
Ich warne nachdrücklich, dass
es gängige Internet-Protokolle gibt, die niemand verwenden sollte. Erst
recht niemand mit Sorge um informationelle Selbstbestimmung. Und
überhaupt niemand, der ein Anonymisierungsverfahren einsetzt. Dies sind
insbesondere POP und IMAP zum Lesen von E-Mail, NNTP zum Lesen von
News-Beiträgen und, falls eine Authentifizierung erforderlich ist, auch
HTTP beim Web-Surfen sowie FTP zum Dateitransfer. Bei all diesen
Protokollen werden sämtliche Daten im Klartext übertragen, insbesondere
Benutzerkennung und Passwort. Das bedeutet, dass alle Rechner, z. B.
[beteiligte Router][grundlagen], auf dem Weg vom eigenen
Rechner bis zum Zielserver sämtliche Nachrichten mitlesen (auch nach
Belieben unbemerkbar manipulieren) und dabei Passwörter ausfiltern
können. Nach einmaligem Login darf man seine Kennung als kompromittiert
ansehen. Vor dieser Gefahr schützt auch keines der im Folgenden
beschriebenen Anonymisierungsverfahren. Das Gegenteil ist der Fall: Bei
Verwendung der folgenden Verfahren werden die eigenen Daten über
zusätzliche Wege und Zwischenstationen, die von unbekannten Parteien mit
unbekannten Motiven kontrolliert werden, weitergeleitet. Es sollte klar
sein, dass die eigenen Daten dabei auch in die dunkelsten Ecken im
Internet geraten werden.

Um sich sicher durch diese dunklen Ecken bewegen zu können, deaktivieren
Alice und Bob aktive Inhalte wie Java, JavaScript und Flash, was durch
die Firefox-Erweiterung NoScript ([wie oben
beschrieben][noscript]) sichergestellt
wird (bzw. durch geeignete Konfiguration des Tor Browsers). Zudem
vertrauen sie auf starke Kryptographie und verwenden nicht obige
Klartextprotokolle, sondern deren durch das [oben erläuterte Protokoll
SSL/TLS][ssl-tls] abgesicherten
Varianten (POPS, IMAPS, NNTPS, HTTPS, SFTP). (Und sie machen
Server-Betreiber, die nur unsichere Klartextvarianten anbieten, aber
deren Dienste sie trotzdem für wertvoll halten, darauf aufmerksam, dass
Klartextprotokolle keine gute Idee sind. Obwohl Klartextprotokolle Alice
und Bob erlauben abzustreiten, etwas mit den unter ihren Kennungen
durchgeführten Aktionen zu tun zu haben …)

Lesen Sie obigen Unterabschnitt zu
[SSL/TLS und dessen Grenzen][ssl-tls]; insbesondere
müssen Sie die Varianten von Man-In-The-Middle-Angriffen
mit und ohne Zertifikat-Warnungen kennen.

![Warnung](./public/icons/stop.png){:.logo} Wenn Alice und Bob eines der folgenden
Anonymisierungsverfahren verwenden, sind sie bei Zertifikat-Warnungen
besonders vorsichtig, weil dann insbesondere die letzte zur
Anonymisierung gedachte Zwischenstation von einem Angreifer betrieben
werde könnte, der Kennungen/Passwörter/PIN/TAN trotz
SSL/TLS-Verschlüsselung abfängt.

{% include labels.md %}
