---
layout: page
sequence: 3
title: Das verworrene Web – Verstrickungen mit Collusion/Lightbeam visualisieren
toctitle: Collusion/Lightbeam
description: Collusion visualisiert Überwachung und Tracking im Web, die sich durch Werbe-Blocker reduzieren lassen.
keywords: Collusion, Firefox, Werbe-Blocker, Ad-Blocker, Adblocker, uBlock Origin, NoScript
Time-stamp: "2019-07-25 15:44:26"
permalink: /Collusion.html
---

Im Februar 2013 (also noch vor den
[Snowden-Enthüllungen](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re)
im Juni 2013 und weit vor Bekanntwerden der
[Nutzung von Facebook-Daten durch Cambridge Analytica](https://www.spiegel.de/netzwelt/web/facebook-und-cambridge-analytica-leak-whistleblower-christopher-wylie-gesperrt-a-1198763.html)
im Frühjahr 2018) ist der äußerst empfehlenswerte Artikel
[The Tangled Web We Have Woven—Seeking to protect the fundamental privacy of network interactions](http://dx.doi.org/10.1145/2408776.2408784)
([Übersetzung](https://blogs.fsfe.org/jens.lechtenboerger/2013/04/05/das-verworrene-web-das-wir-gewoben-haben/))
von [Eben Moglen](https://en.wikipedia.org/wiki/Eben_Moglen)
erschienen. Er beschreibt eindrücklich, wie unser Denken und Handeln im
Web (in das unsere Telefone und Bücher eingewoben sind) kontinuierlich
überwacht werden. Der Autor stellt zudem die Idee eines
Privatsphäre-Proxies als Abhilfe vor, etwa in Form der
[FreedomBox](https://www.freedomboxfoundation.org/); dieser Proxy wird,
um unsere Privatsphäre zu verteidigen, unter anderem Web-Bugs entfernen
und Cookies verwalten. Ich zeige im Folgenden mit Hilfe der
Firefox-Erweiterung
[Collusion/Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/), warum
so ein Proxy (oder entsprechende Anpassungen am Browser) notwendig ist,
um unser Recht auf informationelle Selbstbestimmung zumindest ein Stück
weit einzufordern.

Hinweise: Die folgenden Erläuterungen basieren auf Experimenten mit der
Erweiterung Collusion im März 2013.  Diese Erweiterung wurde
unter dem Namen Lightbeam weiterentwickelt (das
Wort „Collusion“ bedeutet „geheime“ oder „betrügerische Absprache“;
„Lightbeam“ ist der dies aufdeckende Lichtstrahl),
dieses [2-minütige englische Video](https://vimeo.com/57546172)
erklärt die Problematik sehr anschaulich.
Zudem entstanden die unten gezeigten Screenshots im Jahre 2013 unter
Verwendung von Adblock Plus, das ich mittlerweile
*nicht* mehr empfehle; zwischenzeitlich habe ich
Adblock Edge empfohlen, das mittlerweile durch
[uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
abgelöst wurde.  uBlock Origin
basiert auf denselben Schutzmechanismen wie Adblock Plus, folgt aber
keinem fragwürdigen Geschäftsmodell.

[Alice und Bob](https://de.wikipedia.org/wiki/Alice_und_Bob) verwenden
die [freie Software](https://de.wikipedia.org/wiki/Freie_Software)
[Firefox](https://www.mozilla.org/de/firefox/new/) als Web-Browser, den
sie nach ihren Vorstellungen ausführen, untersuchen und verbessern,
weitergeben und sogar nach ihren Verbesserungen in geänderter Form
weitergeben dürfen.

Alice und Bob lesen
[immer](https://www.heise.de/tr/artikel/Verfolgungswahn-als-Werbekonzept-1240290.html)
mal
[wieder](https://www.heise.de/tr/artikel/Online-Werbung-die-kleben-bleibt-1766853.html)
in verschiedenen
[Sprachen](http://allthingsd.com/20110628/amazon-starts-an-ad-network-powered-by-your-data/)
und
[unterschiedlich](https://www.zeit.de/digital/internet/2013-03/facebook-ad-atlas-werbung)
ausgerichteten [Quellen](https://taz.de/!5077078/), dass sie im Web
für
[Werbezwecke](https://www.spiegel.de/netzwelt/web/reklame-im-web-gesamtumsatz-2012-ueber-100-milliarden-dollar-a-876708.html)
auf Schritt und Klick verfolgt werden, was als Tracking, Profilbildung
oder Retargeting bezeichnet wird. Als wäre dies nicht schon schlimm
genug, enthält Werbung im Web zudem oft Schad-Software, sogenannte
Malware, die ihre Rechner als Trojaner infizieren kann, ohne dass sie
selbst irgendwelche „Dummheiten“ machen würden. Diese als
[Malvertising](https://en.wikipedia.org/wiki/Malvertising) bezeichnete
Auslieferung von Schad-Software wird später genauer
[thematisiert][malvertising] und macht
Werbe-Blocker wie
[uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
im Rahmen der digitalen Selbstverteidigung unverzichtbar.

Dank der freien Firefox-Erweiterung
[Collusion/Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/)
können Alice und Bob einen anschaulichen Einblick in das Ausmaß des
Problems erhalten. (Es geht hier nur um die Überwachung zu Werbezwecken;
[staatliche](https://www.wired.com/2012/03/ff-nsadatacenter/) und
[anderweitig kriminelle](https://www.sueddeutsche.de/digital/staatliche-daten-spionage-trojaner-fressen-grundrecht-auf-1.1158728)
Überwachungen lassen sich so nicht erkennen …)

Nach Installation der Erweiterung Collusion erscheinen im Browser ein
zusätzliches Icon sowie der Menüeintrag „Extras“ / „Collusion Graph“.
Durch Klick auf das Icon bzw. Auswahl des Menüeintrags öffnet sich ein
neuer Reiter, in dem Collusion visualisiert, welche Parteien unter einer
Decke stecken, um Handlungen im Web zu verfolgen, zu analysieren,
vorherzusagen und generell unbekannte Dinge zu tun.

Die folgenden Ausführungen beziehen sich auf Experimente
am 10. März 2013.
Nach Besuch des Online-Shops `www.zalando.de`, der hier als
besonders abstoßendes und daher schon wieder faszinierendes Beispiel
dienen soll, zeigte Collusion den folgenden
[Graphen](https://de.wikipedia.org/wiki/Graph_%28Graphentheorie%29).

![Collusion nach Besuch von Zalando](./public/images/collusion-zalando.png)

Blau umrandete Knoten zeigen in Collusion Web-Seiten, die direkt
aufgerufen wurden; hier gibt es nur einen zentralen, blau umrandeten
Knoten, der Zalando selbst repräsentiert. Weitere Knoten zeigen weitere
Parteien, mit denen der Browser im Hintergrund kommuniziert, und
Verbindungen zwischen Knoten machen deutlich, über welche
Zwischenstationen mit welchen Parteien Kommunikation stattfindet. Es
sind sowohl die üblichen Verdächtigen dabei (das „g“- und das „f“-Logo
erkennen wohl die meisten) als auch zahlreiche wenig bis gar nicht
bekannte
[DNS-Domänen](https://de.wikipedia.org/wiki/Domain_Name_System). Jede
Domäne repräsentiert Web-Server im Internet, die von zunächst
unbekannten Betreibern mit unbekannten Interessen betrieben werden.
Dabei ist es durchaus üblich, dass mehrere Domänen zu einem Unternehmen
gehören.

Durch Auswahl eines Knotens mit der Maus werden Details zur zugehörigen
Domäne angezeigt. Am linken Rand ist hier der Anfang der Details zu
Zalando zu sehen, nämlich an wen Daten übertragen werden; eine
stattliche Liste, und von den meisten Domänen haben weder Alice noch Bob
jemals gehört …

Nun ist es nicht ungewöhnlich, dass der Browser die Web-Server mehrerer
Domänen für die Anzeige einer einzigen Web-Seite kontaktiert, z. B.
werden oftmals Bilder sowie CSS- und JavaScript-Dateien von externen
Servern eingebunden. In der Tat werden viele Inhalte für die
Zalando-Seite nicht von Servern der Domäne `zalando.de` sondern von
`ztat.net` ausgeliefert. Wie im [vorangehenden Abschnitt][grundlagen]
erläutert kann jeder dieser Server
Cookies für seine Domäne setzen, und dies geschieht hier in Hülle und
Fülle: Ein frischer Browser ohne Cookies verfügte nach dem Aufruf der
Zalando-Startseite sowie einem einzigen Klick auf einen zufällig
ausgewählten Artikel über genau *100 Cookies*
aus *44 Domänen*. Von diesen 100 Cookies waren
lediglich 13 `zalando.de`-Domänen zugeordnet (3 für `www.zalando.de`, 8
für `zalando.de`, 2 für `track.zalando.de`). 86 der 100 Cookies waren
keine Sitzungs-Cookies, sondern blieben auch nach dem Browser-Neustart
erhalten, teilweise mit einem Haltbarkeitsdatum bis ins Jahr 2038.
Welche
[HTTP](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol)-Aufrufe
der Browser dabei durchgeführt hat, können Sie [dieser Log-Datei](public/zalando-httplog.txt) entnehmen, die ich mittels
[meiner Chaosreader-Version](https://www.informationelle-selbstbestimmung-im-internet.de/chaosreader.html)
aus einem [Wireshark](https://www.wireshark.org/)-Mitschnitt erstellt
habe, nachdem ich die zahlreichen Aufrufe zur oben erwähnten Domäne
`ztat.net` entfernt habe. Jede Zeile der Log-Datei zeigt eine sogenannte
[HTTP](https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol)-Get-Anfrage
des Browsers einschließlich eines eventuellen Referrer-Headers und führt
zudem auf, ob mit dieser Anfrage (mindestens) ein Cookie verschickt
wurde („Cookie sent.“) und ob in der zugehörigen Antwort des Web-Servers
(mindestens) ein Cookie gesetzt wurde („Sets cookie.“).

Mit 100 Cookies hätte Zalando in dieser [Untersuchung des Wall Street
Journal](https://blogs.wsj.com/wtk/) im Jahre 2010 eine auch
international hervorragende Platzierung erreichen können. Glückwunsch!

Spannender als der Besuch einer einzelnen Web-Seite ist natürlich das
Surf-Verhalten über einen längeren Zeitraum. Die folgende Abbildung
stellt den Collusion-Graphen nach dem Besuch der Web-Seiten von Zalando,
Amazon, Süddeutscher Zeitung und Spiegel dar (wiederum mit jeweils einem
Klick auf einen zufällig ausgewählten Artikel).

![Collusion nach Besuch von Zalando, Amazon, Süddeutscher Zeitung, Spiegel](./public/images/collusion-mehrere.png)

Interessant werden nun Knoten, die zentral angeordnet sind und über
viele Kanten verfügen, die also viel über das gesamte Surf-Verhalten
lernen. Hier sehen Alice und Bob z. B., dass ihr Browser der Domäne
`doubleclick.net` permanent mitgeteilt hat, was sie tun. Insbesondere
sehen sie links im Bild die Mitteilung von Collusion, dass
`doubleclick.net` sie vermutlich auf allen vier von ihnen aktiv
besuchten Domänen und einer weiteren, die sie gar nicht kennen, gesehen
hat.

Warum dürfen die das? Keine Ahnung.

Warum können die das? Weil der Browser mitspielt. Unsere Privatsphäre
wird den Interessen der Werbewirtschaft untergeordnet.

Das muss aber nicht so sein. Wie ich [anderswo zur PC-Grundsicherung][grundsicherung-des-pcs]
darlege, empfehle ich die freien Firefox-Erweiterungen [uBlock
Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)
und [NoScript](https://noscript.net/), sowohl zum Schutz vor Überwachung
als auch vor der Kompromittierung der eigenen Rechner durch
[Malvertising][malvertising].

Bei Wiederholung des obigen Experiments zeigt sich mit Adblock Plus
(unter Verwendung der voreingestellten „EasyList Germany+EasyList“ mit
voreingestelltem Zulassen „nicht aufdringlicher Werbung“) folgendes
Bild.

![Collusion mit Adblock Plus](./public/images/collusion-adblock.png)

Der Graph ist deutlich ausgedünnt. Zentral sind jetzt Facebook und
Google sowie `google-analytics.com` zu erkennen, wobei letzteres
lediglich den Besuch bei (dem im Graphen nun isolierten) Amazon verpasst
hat. Alle drei sind hier nicht als Werbenetze aktiv, sehen Alice und Bob
aber trotzdem zu. Demgegenüber hat `doubleclick.net` nur noch den Besuch
von Zalando registriert (der weiße Knoten über dem Zalando-Kreis).

Alice und Bob installieren spätestens jetzt (zusätzlich zum Ad-Blocker)
[NoScript](https://addons.mozilla.org/en-US/firefox/addon/noscript/),
das die Ausführung von JavaScript verhindert. Dann funktionieren viele
Web-Seiten nicht mehr; für diejenigen, die ihnen wichtig sind, können
sie die Ausführung von JavaScript allerdings von Hand erlauben. Bei
Wiederholung des obigen Experiments ohne NoScript-Änderungen zeigt sich
schließlich folgendes Bild.

![Collusion mit Adblock Plus und NoScript](./public/images/collusion-adblock-noscript.png)

Facebook liegt immer noch in aussichtsreicher Position. Erwähnenswert
ist außerdem `ivwbox.de`, das als weißer Knoten rechts zu sehen ist
(verbunden mit der Süddeutschen und dem Spiegel) und unsere Lektüre auf
zahlreichen deutschsprachigen journalistischen Angeboten begleitet.

Alice und Bob erhalten langsam eine Idee davon, wie verworren das Web
ist und wer ihnen so zusieht. Sie könnten jetzt weitere Filterlisten im
Ad-Blocker hinzufügen oder [eigene Regeln
definieren](https://adblockplus.org/de/filters). Als weitere Filterliste
empfehle ich
[EasyPrivacy](https://easylist-downloads.adblockplus.org/easyprivacy.txt),
die über das Menü „Extras“ / „Adblock Plus“ / „Filtereinstellungen …“ /
„Filterabonnement hinzufügen …“ / „Anderes Abonnement hinzufügen“ in der
dann erscheinenden Auswahlliste wählbar sein sollte. Mit ihr
verschwinden die Collusion-Knoten zu `ivwbox.de` und `doubleclick.net`,
und `google-analytics.com` wird eingeschränkt. Entlang dieses
Menü-Pfades entfernen sie unter den Filtereinstellungen ihre Zustimmung
zu „nicht aufdringlicher Werbung“, da sie weder aufdringlich noch
unaufdringlich überwacht werden wollen.

Zusammenfassend haben Ad-Blocker den Zweck, durch ihre Filterlisten die
Kommunikation mit unerwünschten Parteien zu unterbinden. Dieser Ansatz
funktioniert naturgemäß nur, wenn die unerwünschten Parteien den
Erstellern der Filterlisten bekannt sind und die Listen stetig
aktualisiert werden. Offenbar ist dieses (auch als Blacklisting
bezeichnete) Vorgehen fehleranfällig (es könnte sowohl zu viel
aussortiert werden, was dazu führen würde, dass manche Web-Seiten nicht
mehr funktionieren, als auch zu wenig, was dann Tracking nicht
unterbinden würde). In der Tat widerspricht Blacklisting dem seit
Jahrzehnten in Forschung und Lehre anerkannten [Entwurfsprinzip der
*Fail-Safe Defaults* für sichere
Systeme](http://web.mit.edu/Saltzer/www/publications/protection/),
wonach wünschenswertes Verhalten explizit
*erlaubt* werden sollte (weil bei Erstellung eines
Systems bekannt ist, was es leisten soll), während alles ohne explizite
Erlaubnis automatisch unterbunden wird. In einer offenen Umgebung wie
dem Web ist allerdings unklar, welche Erlaubnisse ein Browser mitbringen
sollte, und so stellt er Funktionalität über Privatsphäre und erlaubt
alles, vielfältige Formen der Überwachung. Insofern verkörpert das
Blacklisting von Ad-Blockern einen deutlichen Fortschritt gegenüber dem
„reinen“ Browser.

Es sei darauf hingewiesen, dass der später diskutierte [Tor
Browser][tor] keine Filterlisten
mitbringt. In der Tat schließt das Entwickler-Team in der Dokumentation
der [Entwurfsziele des Tor
Browsers](https://www.torproject.org/projects/torbrowser/design/) den
Einsatz von Filterlisten aufgrund ihrer Nachteile explizit aus.
Stattdessen werden Anforderungen an Sicherheit und Privatsphäre
formuliert und notwendige Anpassungen am Firefox abgeleitet, um die
Anforderungen zu erfüllen.

Ein Privatsphäre-Proxy, der obige Überlegungen konsequent umsetzt, ist
eine hervorragende Idee. Bis so ein Proxy verfügbar ist, müssen Alice
und Bob selbst aktiv werden, um ihr Recht auf informationelle
Selbstbestimmung und ihre Privatsphäre zu verteidigen.

{% include labels.md %}
