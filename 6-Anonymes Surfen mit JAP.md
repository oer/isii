---
layout: page
sequence: 6
title: 'Informationelle Selbstbestimmung für mehr Privatsphäre im Web: Anonymes Surfen mit JAP/JonDo'
toctitle: Anonymes Surfen mit JAP/JonDo
description: JAP/JonDo ist nicht mehr empfehlenswert.
keywords: JAP, JonDo, Mängel
Time-stamp: "2019-07-24 17:08:37"
permalink: /Anonymes_Surfen_mit_JAP.html
---

<a href="https://www.anonym-surfen.de/software.html" class="nooutlink"><img src="./public/logos/JonDo.png" alt="JonDo Logo" class="logo"></a>
[JAP](https://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
ist ein Anonymisierungsdienst, der die Privatsphäre von Alice und Bob
der [oben beschriebenen Idee von
Chaum][proxies] folgend durch
mehrfaches, verschlüsseltes Weiterleiten ihrer Kommunikation schützen
soll.

![Warnung](./public/icons/stop.png){:.logo} Persönlich habe ich im Jahre 2014
aufgehört,
[JAP](https://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
zu verwenden.
Im [Forum der Anbieter wird seit Oktober 2015 der Niedergang des Projekts diskutiert](https://anonymous-proxy-servers.net/forum/viewtopic.php?f=6&t=9341),
wobei die Entwickler erstaunlich wenig beitragen.  Zudem listet das
[Privacy-Handbuch gravierende Mängel der Software auf](https://www.privacy-handbuch.de/handbuch_23x.htm).
Ich rate an Anonymisierung Interessierten daher, auf das im folgenden
Abschnitt beschriebene [Tor][tor] zu setzen.

Mit
[JAP](https://anon.inf.tu-dresden.de/)/[JonDo](https://www.anonym-surfen.de/software.html)
wird die Browser-Kommunikation verschlüsselt über eine *vordefinierte*
Kette (man spricht hier von einer
Kaskade) von Zwischenstationen abgewickelt.  Dadurch sehen Dritte mit
punktueller Kontrolle über die Netzwerkinfrastruktur nur noch
verschlüsselte Kommunikation, ohne zu erfahren, wer mit wem
kommuniziert.  So sehen z. B. der Internet-Provider von Alice oder die
Betreiberin des von ihr genutzten WLANs nur noch ihre verschlüsselte
Kommunikation mit dem ersten JAP-Server, erfahren aber nichts über den
besuchten Web-Server.  Diese Dritten lernen also wesentlich weniger über
Alice als zuvor, was ihre Privatsphäre schützt.  Der eigentlich von Alice
besuchte Web-Server sieht demgegenüber nur eine Anfrage des letzten
JAP-Servers der Kaskade, weiß aber nicht, dass die Kommunikation von
Alice initiiert wurde, was wiederum ihrer Privatsphäre dient.

{% include labels.md %}
