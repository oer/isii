<!--- Local IspellDict: en -->
<!--- Copyright (C) 2019 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

This project hosts source files for the German
[OER](https://en.wikipedia.org/wiki/Open_educational_resources)
Web page
[informationelle-selbstbestimmung-im-internet.de](https://www.informationelle-selbstbestimmung-im-internet.de/).

HTML is generated with the [Lanyon](https://github.com/poole/lanyon)
theme for [Jekyll](https://jekyllrb.com/).

Lanyon is distributed under the
[MIT License](https://github.com/poole/lanyon/blob/master/LICENSE.md),
while the text of the Web page is distributed under
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).

Note that HTML could be generated as GitLab Pages using
[this CI/CD configuration](.gitlab-ci.yml.disabled), but that would
require a different kind of privacy policy.
