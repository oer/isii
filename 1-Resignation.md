---
layout: page
sequence: 1
title: Resignation – Das bringt doch alles nichts!
toctitle: Resignation wäre verfehlt.
description: Es besteht kein Anlass zu Resignation und faulen Ausreden. Alternativen sind alternativlos.
keywords: Resignation, faule Ausrede, Abhören, Massenüberwachung, Datenkraken, freie Software, Verschlüsselung, Freifunk, digitale Selbstverteidigung
Time-stamp: "2019-07-24 17:07:32"
permalink: /Resignation.html
---

> „Geheimdienste schneiden alles mit, und Datenkraken wie Facebook und
> Google wissen doch eh alles.“

Angesichts dieser Einschätzung von Privatsphäre im Internet könnten
Alice und Bob in Resignation verfallen.  Oder ihr Umfeld könnte diese
Einschätzung als faule Ausrede verwenden, um auf die Dienste von
Datenkraken zurückzugreifen.  Alice, Bob, Sie und ich können uns aber
auch gemeinsam anstrengen.  Mal sehen.

Im Sommer 2013 hat
[Edward Snowden die Ausmaße staatlicher Massenüberwachung und Spionage](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re)
mit Details zu den Abhörprogrammen
[PRISM](https://de.wikipedia.org/wiki/PRISM),
[Tempora](https://de.wikipedia.org/wiki/Tempora) und allgemeiner
denjenigen der
[Five Eyes](https://de.wikipedia.org/wiki/UKUSA-Vereinbarung)
enthüllt: Unsere Kommunikation wird weltweit von Geheim- und
Nachrichtendiensten abgehört, gespeichert und analysiert, und zwar im
Wesentlichen vollständig, hemmungslos und unkontrolliert. Alles:
E-Mail, Chat, Telefongespräche, Surfverhalten von Internet-Suche über
Käufe zu Aktivitäten in Foren und sozialen Netzwerken. Ohne
demokratische Legitimation oder Kontrolle.

Das ist nicht neu.
[Dieser Artikel aus dem letzten Jahrtausend](https://www.heise.de/ct/artikel/Abhoer-Dschungel-286194.html)
befasst sich mit der damals noch weitgehend unbekannten NSA, dem
ECHELON-Abhörskandal, und dem planmäßigen Abbau unserer Grundrechte
durch Abhörgesetzgebung in Deutschland (der nach dem 11. September 2001
konsequent fortgesetzt wurde).

Dabei ist Massenüberwachung robust: Daten werden von Ermittlungsbehörden
offiziell angefordert. Daten werden per Geheimgerichtsbeschluss
angefordert. Daten werden per Massenüberwachung während der Übertragung
mitgeschnitten. Daten werden bei den Providern, deren Infrastrukturen
unterwandert sind, gestohlen.

So weit zum ersten Teil der einleitenden Einschätzung: Geheimdienste
lesen in der Tat alles mit, was technisch möglich ist. Sie könnten das
Gelesene auch beliebig modifizieren, worüber zu wenig nachgedacht wird.
Eine kleine, aber feine Einschränkung gibt es beim Lesen allerdings: Die
Inhalte „richtig“ verschlüsselter Kommunikation sind vor
Massenüberwachung geschützt. (Snowden:
[*Encryption works. Properly implemented strong crypto systems are one of the few things that you can rely on. Unfortunately, endpoint security is so terrifically weak that NSA can frequently find ways around it.*](https://www.theguardian.com/world/2013/jun/17/edward-snowden-nsa-files-whistleblower)
Beachten Sie, dass es hier um Massenüberwachung geht, gegen die
Verschlüsselung sehr wohl hilft. Gegen zielgerichtete Spionage haben wir
im Internet keine Chance, weil unsere Hard- und Software zu viele
Sicherheitslücken mitbringt, was ich im Abschnitt zur
[PC-Grundsicherung][grundsicherung-des-pcs]
aufgreife. Ich hoffe, dass die wenigsten von uns zielgerichtet
ausspioniert werden.)

Die Möglichkeiten der Massenüberwachung werden immer umfassender, weil
unser (Offline-) Leben zunehmend mit dem (Online-) Web verschmilzt.
[Eben Moglen](https://en.wikipedia.org/wiki/Eben_Moglen) hat dies im
Frühjahr 2013 (noch vor den Snowden-Enthüllungen) plastisch mit dem
[„verworrenen Web“](http://moglen.law.columbia.edu/CPC/CACM_0301_V56.02_VP_PrivacySecurity.pdf)
beschrieben, in das wir unser Offline-Leben immer weiter einweben. Lesen
Sie [seinen Artikel](http://dx.doi.org/10.1145/2408776.2408784) (oder
meine
[Übersetzung](https://blogs.fsfe.org/jens.lechtenboerger/2013/04/05/das-verworrene-web-das-wir-gewoben-haben/)).

Im Web werden wir auf Schritt und Klick verfolgt, protokolliert und
vorhergesagt, beim Lesen berichten E-Book-Reader, wer was wie lange
liest, kommentiert, überspringt oder abbricht. Unternehmen sammeln und
verkaufen die zugehörigen Daten unter unverständlichen ~~Datenschutz-~~
Datenschatzbedingungen und sind gezwungen, sie diversen (Geheim-)
Diensten zur Verfügung zu stellen. Moglen schlägt technische (z. B. die
[FreedomBox](https://freedomboxfoundation.org/)) und ökologische
Gegenmaßnahmen zum Schutz unserer Privatsphäre vor, wie Sie meiner
Empfehlung folgend bei Moglen gelesen haben, oder?

<a name="oeko"></a>Ökologisch bedeutet dabei, dass wir unsere
Internet-Umwelt durch unsere Handlungen mitgestalten oder verschmutzen,
wobei Wechselwirkungen auftreten. Wenn Alice die Dienste einer
Datenkrake in Anspruch nimmt, macht sie diese attraktiver, so dass Bob
eher in deren Saugnäpfe geraten wird. Wenn Alice beispielsweise ihr
E-Mail-Konto bei einer Datenkrake unterhält, muss Bob seine E-Mails, die
für Alice persönlich gedacht sind, an die Datenkrake übergeben, die die
Kommunikation analysiert, für Geheimdienste und Ermittlungsbehörden (und
vermutlich normale Geschäftspartnerinnen) zugänglich macht und nebenbei
auch an Alice weiterleitet.

Eine zentrale Herausforderung besteht natürlich darin, dass wir nicht
länger am Rechner sitzen müssen, um mit dem Internet verbunden zu sein.
Smartphones, Brillen und Uhren, Fernseher, Autos, E-Bücher, Puppen und
Spielekonsolen und was-weiß-ich werden zu Augen, Ohren und
Positionsmeldern für uns und andere.

Es lohnt sich, kurz innezuhalten. Dank der
[Snowden-Enthüllungen](https://www.zeit.de/digital/datenschutz/2013-10/hintergrund-nsa-skandal)
ist wohl jedem klar, dass Smartphones mit ihren Kameras, Mikrophonen und
GPS-Empfängern phantastische Überwachungswerkzeuge sind, die
ihresgleichen suchen. In der Tat hatte das
[iPad 2 im Jahre 2011 die Rechenleistung des Supercomputers Cray 2 erreicht](https://bits.blogs.nytimes.com/2011/05/09/the-ipad-in-your-hand-as-fast-as-a-supercomputer-of-yore/),
der 1985 der schnellste Rechner der Welt zu einem Preis von 17 Mio. US$
war und der
[1994 noch unter der Top-500-Liste](http://www.top500.org/list/1994/06/) der schnellsten
Rechner der Welt vertreten war. Mit solchen Geräten kann man mehr
anstellen, als Selfies zu versenden. Smart TVs bleiben da kaum zurück.
Im Jahre 2015
[warnte Smart-TV-Hersteller Samsung seine Kunden, dass die Gespräche aus deren Wohnzimmern ins Internet übertragen werden](https://netzpolitik.org/2015/samsung-warnt-bitte-achten-sie-darauf-nichts-privates-vor-unseren-smarttvs-zu-erzaehlen/).
Beachten Sie dass dieses Vorgehen eine Massenüberwachung ermöglicht,
da die Gespräche zum Zweck der Spracherkennung an von Samsung
bestimmte Server übertragen werden, wo sie in der Masse abgegriffen
werden können (von Geheimdiensten und anderen Kriminellen).  Über
derartige Massenüberwachung hinaus wurde im März 2017 im Rahmen der
[WikiLeaks-Enthüllung „Vault 7“](https://wikileaks.org/ciav7p1/)
bekannt, dass die
[CIA Smart-TVs schon länger in Abhörgeräte verwandelt](https://www.heise.de/mac-and-i/meldung/Apple-zu-Vault-7-Enthuellungen-Viele-iOS-Luecken-bereits-gepatcht-3646617.html).
Dies betrifft viele von uns: Wenn die CIA über das Netz in Fernseher
einbrechen kann, um diese in Wanzen zu verwandeln, können das natürlich auch
anderweitig Interessierte und Kriminelle.

Generell ist Samsung keine vertrauenswürdige Marke, wenn es um
Sicherheit geht: Im April 2017 wurde eine Untersuchung der
[Unsicherheit von Samsungs Open-Source-Betriebssystem Tizen bekannt, die kein gutes Haar am Hersteller lässt („alles falsch gemacht, was man nur falsch machen kann“)](https://www.heise.de/security/meldung/Betriebssystem-Tizen-fuer-Samsung-Geraete-von-Sicherheitsluecken-durchsiebt-3674713.html).

Offenbar lassen sich nicht nur Fernseher ausnutzen, um uns, unsere Familien
und unsere Freunde zu überwachen – jedes vernetzte Gerät mit Mikrophon kommt
als Wanze in Frage, und selbst
[Schaltsteckdosen](https://www.heise.de/newsticker/meldung/Smart-Home-c-t-findet-versteckte-Mikrofone-und-unsichere-Web-Frontends-3673101.html)
und [Haarbürsten](https://www.spiegel.de/netzwelt/gadgets/re-publica-2017-das-haarbuersten-mikrofon-in-ihrem-badezimmer-a-1146528.html)
werden vernetzt und enthalten Mikrophone.  Schaltsteckdosen und Haarbürsten!

In 2015 wurde eine Barbie-Puppe von VTech berühmt, die die Gespräche von
Kindern katastrophal ungesichert ins Internet schickt, wo Kriminelle sich
Zugang zu fast 6,4 Millionen Kinder-Profilen mit Namen, Geschlecht und
Geburtstagen verschaffen konnten.
Auch [Fotos, Chat-Protokolle, Informationen zu Eltern und Adressen gehörten dazu – und der Hersteller sieht sich für den Schutz dieser Daten noch nicht einmal verantwortlich](https://www.eff.org/deeplinks/2016/03/vtech-we-are-not-liable-if-we-fail-protect-your-data-eff-oh-yes-you-are).
In 2017 wurde erstmals in
[Deutschland eine „smarte“ Schnüffelpuppe namens „My friend Cayla“ von der Bundesnetzagentur verboten](https://netzpolitik.org/2017/schnueffelpuppe-my-friend-cayla-in-deutschland-verboten/) – so
etwas geschieht leider viel zu selten; ebenfalls in 2017 konnten
[2,2 Mio. Sprachaufnahmen von Eltern und Kindern, die mit „smarten“ Teddybären (Cloudpets) aufgezeichnet worden waren, offen im Internet abgerufen werden](https://www.heise.de/security/meldung/Cloudpets-2-2-Millionen-Sprachdateien-von-Kinderspielzeug-offen-im-Netz-3637923.html).
Das [FBI warnt im Sommer 2017 vor derartigem „Spielzeug“](https://www.heise.de/newsticker/meldung/FBI-warnt-vor-vernetztem-Spielzeug-3777829.html),
die [Bundesnetzagentur warnt in der Weihnachtszeit 2018 ebenfalls](https://www.spiegel.de/netzwelt/gadgets/smart-toys-bundesnetzagentur-warnt-vor-vernetztem-spielzeug-a-1242525.html).

### <a name="smart"></a>Alternative: Verzichten Sie auf „smart“, fordern Sie Freiheit

So bequem „smarte“ Geräte (also vernetzte (Super-) Computer, die nicht
so aussehen wie Computer) auch erscheinen mögen, viele gehören einfach
auf den Müll.  „Offline“ ist ein starkes Qualitätsmerkmal.


### <a name="freie-software"></a>Alternative: Wählen Sie freie Software

Natürlich besitzen auch Alice, Bob und ich zahlreiche vernetzte
Computer.  Wir wollen dann aber die Kontrolle behalten, was diese
Computer warum machen.  Mit dem „Wollen“ allein ist noch nichts gewonnen.
Kontrolle erfordert
[freie Software](https://de.wikipedia.org/wiki/Freie_Software)
und die bewusste Auswahl eingesetzter Dienste.  Unsere Marktmacht entscheidet,
welche Arten von Geräten und Diensten erfolgreich sein werden.

*Unsere* Marktmacht, *unser* Verhalten.  Sie erinnern sich an die oben
erläuterte [ökologische Dimension][oeko] unseres Verhaltens?  Offenbar
existieren auch ökonomische Lenkungsmöglichkeiten.  Nutzen Sie sie.

Falls Sie nicht wissen, was *freie* Software ist, rate ich Ihnen dringend,
sich damit zu beschäftigen, beispielsweise auf den Seiten der
[Free Software Foundation Europe](https://fsfe.org/about/basics/freesoftware.de.html).
Freie Software dürfen Sie ausführen, untersuchen und verändern, weitergeben,
verbessern und verbessert weitergeben.  Demgegenüber wirft unfreie Software
soziale und ethische Probleme auf und ist nicht vertrauenswürdig.

Ob Software frei ist oder nicht (auch als unfrei oder proprietär
bezeichnet), wird durch ihre Lizenzbestimmungen definiert, und nicht
etwa durch den Preis: Sie sollten bei „frei“ an Freiheit denken, nicht
an kostenlos.  Entsprechend sind treffende Begriffe für Software, die
nicht frei ist, *kontrollierend* oder *versklavend*.  Typische
Software von Amazon, Apple, Microsoft und sowie Teile der Software von Google
sind versklavend: Nicht wir sollen die Kontrolle über unsere Geräte
und damit über unsere Gedanken, Meinungen, Vorlieben, Aufenthaltsorte
usw. erhalten, sondern die Anbieter der Geräte.

Es gibt abstoßende Beispiele, die die Konsequenzen aus
mangelnder Software-Freiheit illustrieren:

 * Digitalisierte Bücher, für die Sie bezahlen, um sie auf „Ihrem“
   E-Book-Reader zu lesen, sind Ihrer Kontrolle entzogen.  So hat Amazon im
   Jahre 2009 beispielsweise ausgerechnet
   [den Roman „1984“ von Kindle-Geräten gelöscht](https://www.spiegel.de/netzwelt/web/e-reader-kindle-amazon-loescht-digitale-exemplare-von-1984-a-637076.html);
   [Microsoft hat in 2019 sein E-Book-Angebot geschlossen, wodurch zuvor „gekaufte“ E-Books unbrauchbar werden](https://www.spiegel.de/netzwelt/web/microsoft-e-books-der-tag-an-dem-die-buecher-nicht-mehr-funktionieren-a-1274765.html);
   E-Books gehören nicht uns.
 * [Sony bewarb seine PlayStation 3 ursprünglich mit der Möglichkeit, GNU/Linux als freies Betriebssystem darauf laufen zu lassen, bis die Firma sich im Jahre 2010 entschied, diese Möglichkeit per Firmware-Update zu entfernen](https://en.wikipedia.org/wiki/OtherOS);
   Spiele-Konsolen gehören nicht uns.
 * [„Smarte“ Turnschuhe von Nike konnte man in 2019 per Firmware-Update lahmlegen.](https://www.heise.de/newsticker/meldung/Selbstschnuerende-Turnschuhe-Firmware-Update-nur-mit-iPhone-4314190.html);
   „Smarte“ Geräte unterliegen nicht nur – wie bisher üblich – mechanischem
   Verschleiß, sondern können auch durch Software-Updates zerstört werden.
 * Google-Tochter
   [Nest Labs entschied sich, den Smart-Home-Hub Revolv, ein Gerät zur Hausautomation, im Jahre 2016 zu deaktivieren, wodurch Besitzer dieses Gerätes nur noch Elektro-Schrott besitzen](https://www.computerbase.de/2016-04/hausautomation-nest-macht-smart-home-hub-revolv-unbrauchbar/);
   [Microsoft kappte in 2019 die Server-Anbindung für Fitness-Armbänder](https://www.heise.de/newsticker/meldung/Microsoft-kappt-Server-Anbindung-von-Fitness-Armbaendern-4324958.html);
   „Smarte“ Geräte, die nur online funktionieren, gehören uns nicht.
 * Ein [Firmware-Update für Logitechs Smart-Home-Hub Harmony
   deaktivierte im Dezember 2018 eine Programmierschnittstelle zum
   Ansteuern von Fremdanbieter-Geräten](https://www.heise.de/newsticker/meldung/Logitech-lenkt-ein-Nutzer-erhalten-wieder-lokalen-Zugriff-auf-Harmony-Hub-4258594.html).
   Nach Kundenbeschwerden ruderte das Unternehmen zwar zurück, der
   Fall zeigt aber deutlich, wer die Geräte kontrolliert.
 * Apple wirbt mit im Vergleich zu kostenloser Konkurrenz angeblich
   besserer Privatsphäre:
   [“A few years ago, users of Internet services began to realize that when an online service is free, you’re not the customer. You’re the product. But at Apple, we believe a great customer experience shouldn’t come at the expense of your privacy.”](https://www.apple.com/privacy/)
   Das klingt zwar plausibel, ist aber leider falsch, wie Sie leicht
   feststellen, wenn Sie sich anschauen, was der
   [Forensik-Spezialist ElcomSoft aus der iCloud wiederherstellen kann](https://www.heise.de/mac-and-i/meldung/Forensik-Tool-soll-geloeschte-Notizen-aus-iCloud-auslesen-koennen-3718361.html),
   nämlich „gelöschte“ Browserverläufe, „gelöschte“ Fotos, „gelöschte“ Notizen
   und [Anrufprotokolle](https://www.pressebox.de/pressemitteilung/elcomsoft-co-ltd/Apple-laedt-insgeheim-iPhone-Anrufprotokolle-in-die-iCloud/boxid/825628).
   Auf Apple-Geräten lässt sich „natürlich“ nur Software installieren, die
   Apples Segen besitzt; missliebige Software wird immer wieder aus dem App
   Store geworfen.  Beispielsweise hat Apple im Jahre 2017 ohne weitere Gründe
   eine [iPhone-Software zum Auffinden verlegter AirPods (Apple-Kopfhörer für 180 Euro) aus dem hauseigenen App Store entfernt](https://www.heise.de/mac-and-i/meldung/Finder-for-AirPods-Apple-entfernt-App-zur-Kopfhoerer-Suche-3591409.html).
   [Mehrere Entwickler haben den Mac App Store im März 2017 aufgrund der restriktiven Vorgaben von Apple verlassen](https://www.heise.de/mac-and-i/meldung/Restriktiver-Mac-App-Store-Entwickler-verlassen-Apples-Software-Laden-3650899.html).
   Ebenfalls in 2017 hat Apple zahlreiche VPN-Apps, mit denen Chinesen
   – unter anderem Reporter und deren Quellen –
   sich zuvor vor staatlicher Überwachung schützen sowie staatlicher
   Zensur entziehen konnten, aus dem App Store entfernt, was
   [Reporter ohne Grenzen dazu veranlasst hat, Apple fünf Tipps für obrigkeitshörige Unterwürfigkeit mit auf den Weg zu geben](https://rsf.org/en/news/five-tips-apple-how-please-chinas-rulers).
   In 2019 hat [Apple den Fernzugang „Zurück zu meinem Mac“ deaktiviert](https://www.heise.de/mac-and-i/meldung/Apple-dreht-Zugang-zu-meinem-Mac-fuer-alle-Betriebssysteme-ab-4436836.html).
   i-Geräte gehören Ihnen nicht.
 * Ganz ähnlich entfernt auch Google missliebige Software aus den eigenen
   Stores: So wurde im Jahre 2017 die [weiter unten][adnauseam] besprochene
   [Browser-Erweiterung AdNauseam aus dem Chrome Web Store verbannt, angeblich weil ihr Zweck unklar sei](https://adnauseam.io/free-adnauseam.html);
   versklavende Software gehorcht nicht unseren Wünschen.

Kommen wir auf den Begriff „freie Software“ zurück, der viel zu wenig
bekannt ist, was ich hier zu ändern versuche.  Freie Software bezeichnet
fast dasselbe wie der bekanntere Begriff „Open Source“.
[Open Source](https://de.wikipedia.org/wiki/Open_Source) steht für einen
Software-Entwicklungsansatz, bei dem der von Programmierern geschriebene
Quelltext (engl. *source code*) für die Weiterentwicklung durch andere
veröffentlicht wird (also offen, engl. *open*, verfügbar ist).  Die meisten
Open-Source-Lizenzen erfüllen die Kriterien für freie Software.  Während der
Begriff „freie Software“ jedoch unsere Freiheit und unsere
Kontrollmöglichkeiten in den Vordergrund stellt, ist dies bei „Open Source“
nicht der Fall.  Bekannte Lizenzen für freie Software, die auch den
Bedingungen von Open Source gehorchen, was zum Begriff
[FLOSS für Free/Libre Open Source Software](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software)
geführt hat, sind die
[GNU General Public License (GNU GPL)](https://de.wikipedia.org/wiki/GNU_GPL)
oder [BSD-Lizenzen](https://de.wikipedia.org/wiki/BSD-Lizenz) wie die
[Apache-Lizenz](https://de.wikipedia.org/wiki/Apache-Lizenz) und die
[MIT-Lizenz](https://de.wikipedia.org/wiki/MIT-Lizenz).

Für Software im Dienste von Privatsphäre und informationeller
Selbstbestimmung erscheinen mir diese Freiheit und Kontrollmöglichkeiten
unverzichtbar.  Wann immer Sie auf Software treffen, deren Hersteller
Ihnen die Freiheiten freier Software verweigert, sollten Sie sehr
vorsichtig sein.  Es gibt dann ~~vermutlich~~ ganz sicher etwas zu
verbergen, das nicht Ihnen nützt, sondern anderen Parteien …

Auf
[Hardware mit freiheitsgewährenden Eigenschaften gehe ich später am Rande][hardware]
ein.


### <a name="firefox"></a>Alternative Browser: Firefox

<a href="https://www.mozilla.org/de/firefox/new/" class="nooutlink"><img src="./public/logos/firefox.png" alt="Firefox Logo" class="logo"></a>
Alice und Bob setzen zum Surfen auf die freie Software
[Firefox](https://www.mozilla.org/de/firefox/new/) (insbesondere die
später besprochene, von Datenschutzexpertinnen und Datenschutzexperten
entwickelte Firefox-Variante [Tor Browser][tor]). Ihnen ist kein anderer
empfehlenswerter Browser bekannt, wenn der Schutz ihrer Privatsphäre im Fokus
steht.

Browser von Suchmaschinenbetreibern kommen nicht in Frage – sie haben in
erster Linie den Zweck, Daten über Surfende zu generieren, die als kostenlose
Ressourcen im [Überwachungskapitalismus][ueberwachungskapitalismus]
ausgebeutet werden können.

Wenn Ihnen das Surfen mit dem [Tor Browser][tor] zu langsam ist,
sollten Sie
[wie im Abschnitt zur Grundsicherung beschrieben den Firefox in Kombination mit einem Ad-Blocker][malvertising]
einsetzen, was Sie nicht nur vor dem „normalen“ Tracking im Web
schützt, sondern auch vor
[durch Werbung verteilter Schad-Software][malvertising].

<a name="adnauseam"></a>Wenn Ihre bevorzugten Web-Seiten Ihnen bei
aktiviertem Werbe-Blocker keine Inhalte mehr anzeigen, können Sie die
freie Firefox-Erweiterung [*AdNauseam*](https://adnauseam.io/)
verwenden, um Ihren Datenschatten „interessanter“ zu gestalten.  Diese
Erweiterung kann so konfiguriert werden, dass sie im Hintergrund
automatisch sämtliche gezeigte Werbung „anklickt“, was es schwieriger
macht, Ihnen ein „echtes“ Profil zuzuordnen.  Falls Sie derartige
Ideen gut finden, sei Ihnen auch noch die freie Firefox-Erweiterung
[TrackMeNot](https://cs.nyu.edu/trackmenot/) ans Herz gelegt, die im
Hintergrund zufällig generierte Suchanfragen an ausgewählte
Suchmaschinen schickt.  Sollen sie doch Daten sammeln!

Persönlich bevorzuge ich allerdings den [Tor Browser][tor].


### Alternative: Verschlüsselte E-Mail ohne Datenkraken

Es wird niemand gezwungen, über das E-Mail-Konto des Herstellers eines
Smartphones private E-Mails zu verschicken. Auf einem Android-Gerät ist
es beispielsweise bequem, Google als Mail-Anbieter zu verwenden.
Allerdings sind hervorragende deutsche Alternativen nur wenige Klicks
entfernt. Im
[Februar 2015](https://www.test.de/E-Mail-Provider-Mail-Dienste-sehen-alles-4806144-0/)
und [September 2016](https://www.test.de/Test-E-Mail-Provider-5074644-0/)
schnitten bei Stiftung Warentest die Mail-Provider
[Mailbox.org](https://mailbox.org/de/) und [Posteo](https://posteo.de/de)
besonders gut ab.

Datenkraken (und damit auch globale Massenüberwachung) profitieren
wesentlich von der *freiwilligen* Preisgabe
persönlicher Daten. In der Tat ist es einfach und bequem, sich
Datenkraken anzuvertrauen. Für asynchrone Kommunikation, auch in
Gruppen, setze ich dann doch lieber auf E-Mail mit seinen
dezentralisierten Anbietern, idealerweise mit
[GnuPG](https://www.gnupg.org/) verschlüsselt, oder auf einen der
nachfolgend genannten alternativen Messenger.

Ganz allgemein helfen
[kryptographische Sicherheitsmaßnahmen](https://www.theguardian.com/world/2013/sep/05/nsa-how-to-remain-secure-surveillance)
auch und gerade in Zeiten globaler Massenüberwachung. Ich kann nicht
verhindern, dass meine Internet-Kommunikation abgefischt wird. Ich
möchte es den Spitzeln aber wenigstens so schwer wie möglich – wenn
nicht sogar unmöglich – machen zu verstehen, was sie da in ihrem Netz
haben. Ich greife zu digitaler Selbstverteidigung und anonymisiere mein
Surf-Verhalten mit [Tor](https://www.torproject.org/) und verschlüssele
(interessierte Empfänger vorausgesetzt) E-Mails mit
[GnuPG](https://www.gnupg.org/), wie ich auf diesen Seiten genauer
beleuchte.

### Alternative Messenger

Messenger von im [Überwachungskapitalismus][ueberwachungskapitalismus]
verwurzelten US-Firmen verdienen nicht, dass wir ihnen unsere
Gedanken und [Aufenthaltsorte][geolocation] anvertrauen.
Beispielsweise wurde bei WhatsApp ursprünglich behauptet, dass keine
[Nutzerdaten zu Facebook weitergegeben](https://www.zeit.de/digital/datenschutz/2016-09/whatsapp-facebook-datenschutz-weitergabe-daten-abmahnung-verbraucherschuetzer)
würden, was dann geändert werden sollte, massive Proteste sowie
[eine Klage des Verbraucherzentrale Bundesverbands (VZBV) im Januar 2017](https://www.vzbv.de/pressemitteilung/vzbv-verklagt-whatsapp-verbraucher-muessen-hoheit-ueber-daten-behalten)
nach sich zog.
Obendrein wurde die Ende-zu-Ende-Verschlüsselung von Signal auch in
WhatsApp umgesetzt, aber Facebook hat wohl vergessen zu erwähnen, dass
zusätzlich ein „Feature“ eingebaut wurde, mit dem Nachrichten
entschlüsselt werden können.  Der
[Guardian bezeichnete das im Januar 2017 als Hintertür (engl. *backdoor*) zum Schnüffeln](https://www.theguardian.com/technology/2017/jan/13/whatsapp-design-feature-encrypted-messages),
was von Moxie Marlinspike, dem Entwickler der zugrunde liegenden
Ende-zu-Ende-Verschlüsselung von Signal, entschieden zurückgewiesen
wurde; er [argumentierte, es handele sich um eine bewusste Entwurfsentscheidung](https://signal.org/blog/there-is-no-whatsapp-backdoor/).
Welcher Sichtweise Sie sich anschließen wollen, müssen Sie selbst
entscheiden.

Angesichts dieses „Features“ in WhatsApp möchte ich auf den später
beschriebenen „Fall“ [Lavabit][lavabit] hinweisen: US-Unternehmen
werden von US-Behörden dazu gezwungen, ihre Kunden auszuspionieren, wo
immer dies möglich oder machbar ist.  Alice und Bob setzen daher auf
Dienste-Anbieter, deren Lösungen technisch so aufgebaut sind,
dass die Anbieter gar nicht gezwungen werden *können*, sie auszuspionieren.
Derartige Lösungen existieren auch für Messenger.

<a name="signal"></a>Wer an einem
[Messenger](https://de.wikipedia.org/wiki/Instant_Messaging)
bzw. einer [Chat-Anwendung](https://de.wikipedia.org/wiki/Chat) zum
Austausch von Textnachrichten in Echtzeit auf dem Smartphone
interessiert ist, wobei Privatsphäre als Entwurfsziel im Vordergrund
steht, sollte sich die auch von
[Edward Snowden empfohlene](https://www.vox.com/2015/5/21/8638251/snowden-reddit)
freie Software [Signal](https://signal.org/blog/just-signal/)
ansehen, die die früheren Apps TextSecure und RedPhone vereint und die
beispielsweise in
[Tests der Electronic Frontier Foundation hervorragend abgeschnitten hat](https://www.eff.org/deeplinks/2018/03/secure-messaging-more-secure-mess).
Die [Funktionsweise von Signal wird in zugehöriger Dokumentation detailliert erklärt](https://signal.org/docs/).

Signal setzte unter Android lange Zeit die proprietären
[Google Play Services](https://en.wikipedia.org/wiki/Google_Play_Services)
voraus, die einen mit Google kommunizierenden Hintergrundprozess
einrichten, was für mich nicht in Frage kommt.  Im Februar 2017 hat
sich das mit Erscheinen der Signal-Version 3.30 geändert, die ohne Play
Services auskommt.  Seit März 2017 lässt sich
[Signal auch ohne Google Play über offizielle APK-Dateien](https://signal.org/android/apk/)
direkt vom Hersteller der Software auf Google-freien Android-Geräten
installieren. (Die Free Software Foundation Europe erklärt, wie Sie Ihr
[Android-Telefon befreien](https://fsfe.org/campaigns/android/android.de.html)
können.)

Prinzipiell hat Signal den Nachteil, dass es nur den alleinigen
Server-Betreiber Open Whisper Systems gibt, über dessen Infrastruktur
die (verschlüsselte) Kommunikation abgewickelt wird.  Eine derartige
Zentralisierung ist immer gefährlich, weil sie Abhängigkeiten schafft
und Begehrlichkeiten weckt.


### <a name="dezentrale-messenger"></a>Alternative: Verwenden Sie dezentrale Messenger

Dezentralisierte Chat- und Messenger-Alternativen sind aufbauend auf
[XMPP (Jabber)](https://de.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol)
entstanden. Die Netzarchitektur von XMPP ist ähnlich der von E-Mail
aufgebaut, wobei XMPP-Kennungen wie E-Mail-Adressen aussehen und die
Nachrichten zwischen verschiedenen XMPP-Server-Betreibern ausgetauscht
werden. Beispielsweise verfügen Kunden des oben erwähnten
E-Mail-Anbieters
[Mailbox.org automatisch über eine XMPP-Kennung](https://kb.mailbox.org/display/MBOKB/Uebersicht%3A+Was+ist+Jabber+XMPP).

##### <a name="off-the-record"></a>Bestehen Sie auf Off-the-Record Messaging

Verschiedene XMPP-Clients oder -Apps
unterscheiden sich deutlich hinsichtlich ihres Sicherheitsniveaus. Das
mittlerweile als klassisch zu bezeichnende Sicherheitsprotokoll für
Chat-Anwendungen ist das 2004 vorgestellte
[*Off-the-Record Messaging* (OTR)](https://de.wikipedia.org/wiki/Off-the-Record_Messaging), das
folgende Sicherheitseigenschaften „normaler“ Gespräche unter vier Augen
anstrebt: Die Gesprächspartner – und nur sie – wissen, wer was gesagt
hat; es gibt aber keine Möglichkeit, nachzuweisen wer was gesagt hat,
und es bleiben keine Spuren des Gesprächs zurück. OTR garantiert diese
Sicherheitseigenschaften durch Ende-zu-Ende-Verschlüsselung in
Kombination mit
[Diffie-Hellman-Schlüsselaustausch](https://de.wikipedia.org/wiki/Diffie-Hellman-Schl%C3%BCsselaustausch).

Ich weise ausdrücklich darauf hin, dass die Sicherheitseigenschaften von
OTR eigentlich selbstverständlich sind: Über Jahrtausende fand
menschliche Kommunikation derart geschützt statt. Erst in jüngerer
Vergangenheit reden manche Strafverfolger, Sicherheits- und
Innenpolitiker uns ein, dass es keine vertraulichen Gespräche geben
dürfe, insbesondere wenn die Gespräche per Telefon oder über das
Internet geführt werden. Alice, Bob und ich glauben das nicht. Wir
glauben an Brief-, Post- und Fernmeldegeheimnis, an Privatsphäre und
informationelle Selbstbestimmung, auch wenn wir elektronisch
kommunizieren. Daher lehnen wir Messenger ab, die OTR nicht
unterstützen.


##### OMEMO bietet OTR für Gruppen

OTR hat allerdings den Nachteil, dass es weder für asynchrone
Kommunikation (bei der Nachrichten auch dann gesendet werden können,
wenn der Empfänger offline ist) noch für Gruppenkommunikation geeignet ist.
Dieser Mangel wurde durch
[*OMEMO Multi-End Message and Object Encryption*](https://en.wikipedia.org/wiki/OMEMO) behoben, das als
Weiterentwicklung von OTR verstanden werden kann und
Ende-zu-Ende-Verschlüsselung für Gruppen-Chats umfasst. OMEMO integriert
das
[Signal-Protokoll](https://de.wikipedia.org/wiki/Signal-Protokoll),
das unter dem Namen Axolotl-Protokoll ursprünglich für Signal
(bzw. den Vorläufer TextSecure) entwickelt worden ist und auch in
anderen [vielversprechenden Anwendungen](https://de.wikipedia.org/wiki/Signal-Protokoll#Nutzung)
namhafter Sicherheitsexperten zum Einsatz kommt.

Der erste Messenger, der OMEMO unterstützt, ist die freie Android-App
[Conversations](https://conversations.im/). (Conversations ist der
erste Messenger, weil OMEMO in dem zugehörigen Projekt entwickelt
wurde.  Weitere Messenger folgten und folgen,
[eine Übersicht findet sich hier](https://omemo.top/).)

Um einen XMPP-Messenger mit OMEMO zu nutzen, benötigen Sie zunächst
eine XMPP-Kennung.  Wie gesagt, evtl. funktioniert Ihre E-Mail-Adresse
bereits als XMPP-Kennung.  Ansonsten finden Sie Anbieter im Web,
entweder in diesem
[allgemeinen XMPP-Verzeichnis](https://xmpp.net/directory.php)
oder in einer vom
[Conversations-Autor gepflegten Liste zur Kompatibilität verschiedener XMPP-Anbieter mit relevanten Standards](https://compliance.conversations.im/).

Schließlich sei betont, dass vielversprechende, Ende-zu-Ende
verschlüsselnde Messenger entstehen, deren Kommunikation vollständig
über [Tor-Anonymisierungsserver][tor]
abgewickelt wird, so dass weder sichtbar wird, wer sich mit wem
unterhält, noch worüber.  Während für Desktop-PCs der
[Tor Messenger nicht mehr weiterentwickelt wird (März 2018)](https://blog.torproject.org/sunsetting-tor-messenger)
und die Entwicklung von
[Ricochet seit 2016 kaum noch vorankommt](https://github.com/ricochet-im/ricochet/issues/555), entsteht
[Briar](https://briarproject.org/) für Android.

### <a name="voip"></a>Alternative Telefoniedienste: Mumble und Spreed.ME

Als freie, dezentralisierte, verschlüsselte Alternative zu Skype
empfehle ich [Mumble](https://wiki.mumble.info/wiki/Main_Page), dessen Server jemand mit
technischem Interesse aufsetzen muss. Alternativ könnten Sie
[Spreed.ME in Nextcloud](https://nextcloud.com/integrating-secure-private-video-and-audio-chat-in-nextcloud/)
ausprobieren, das ich selbst (noch) nicht nutze, aber das auf dem
[WebRTC-Standard](https://de.wikipedia.org/wiki/WebRTC) aufsetzt und
mit Ende-zu-Ende-Verschlüsselung direkt aus dem Browser heraus
funktioniert.

### Alternative Suchmaschinen

Auch in anderen Fällen werden wir nicht gezwungen, uns in Tentakeln zu
verheddern. Persönlich halte ich die Suchergebnisse von Google für
hervorragend, jedoch möchte ich Google nicht mit meinem Suchverhalten
füttern und verwende daher Alternativen wie
[Startpage](https://startpage.com/), wo Google-Ergebnisse auf
datenschutzfreundliche Art ausgeliefert werden, oder
[Metasuchmaschinen](https://de.wikipedia.org/wiki/Metasuchmaschine) wie
das von einem deutschen Verein mit freier Software betriebene
[MetaGer](https://metager.de/), das von einer deutschen Firma
betriebene [UNBUBBLE](https://www.unbubble.eu/) oder das
internationale [searX](https://searx.org/), das als freie Software
auch auf dem eigenen Rechner betrieben werden kann.

### Alternativen zu Cloud-Diensten, insbesondere Owncloud/Nextcloud

Ich [meide die Cloud](https://blogs.fsfe.org/jens.lechtenboerger/2013/06/08/warum-ich-die-cloud-meide/),
um private Daten nicht unkontrolliert Dritten zu überlassen. Die
Web-Seite [PRISM-Break](https://prism-break.org/de/) listet zahlreiche,
auf [freier](https://de.wikipedia.org/wiki/Freie_Software) Software
basierende Alternativen zu beliebten Datenkraken auf.

<a name="owncloud"></a>Um dies ganz deutlich zu sagen: Bei Verwendung
vieler (möglicherweise liebgewonnener) Dienste und Funktionen sind im
Internet aktuell weder Privatsphäre noch informationelle
Selbstbestimmung möglich. Sie haben aber eine Chance, dass Nachrichten,
Dokumente, Fotos, Termine und Kontakte Ihre Privatangelegenheit bleiben,
wenn Sie sie nicht unkontrolliert weitergeben. Sie könnten z. B. einen
[Netzwerkspeicher](https://de.wikipedia.org/wiki/Network_Attached_Storage)
(Network Attached Storage, NAS) für derartige Daten anschaffen; wenn Sie
technisch interessiert sind, können Sie auf diesem Gerät zudem die freie
Software [ownCloud](https://owncloud.org/) oder
[Nextcloud](https://nextcloud.com/) als vollwertige
Synchronisationslösung installieren. Dann können Sie entscheiden, wem
Sie wann Zugriff worauf gewähren wollen (sich selbst,
Familienangehörigen, Freunden; nur lokal bei Ihnen zu Hause oder auch
aus dem Internet). Anleitungen gibt es viele, auch immer wieder in
Zeitschriften. Aktuelle Hinweise finden Sie beispielsweise unter dem
[Thema „NAS“ bei heise online](https://www.heise.de/thema/NAS).

### Alternative Wege ins Internet: Freifunk

Schließlich halte ich alternative Kommunikationsnetze in Form drahtloser
[Mesh-Netzwerke](https://de.wikipedia.org/wiki/Mesh-Netzwerk) für eine
sehr gute Idee. Solche Netze bauen eine eigene
Kommunikationsinfrastruktur auf, was zusätzliche Wege ins Internet
eröffnet und Überwachung erschwert. In Deutschland halte ich das
[Freifunk-Projekt](https://freifunk.net/)
für *sehr* empfehlenswert. Seit September 2014 betreibe
ich selbst einen Freifunk-Router im Rahmen von
[Freifunk Münsterland](https://freifunk-muensterland.de/). Ich freue mich über
freies WLAN, und Freifunk ermöglicht es mir als Privatperson, mit
einfachen Mitteln Zugang zu einem offenen WLAN anzubieten, ohne Angst vor
absurder Störerhaftung haben zu müssen. (Im Oktober 2017 sollte die
Störerhaftung für WLANs durch eine Änderung des Telemediengesetzes
eigentlich abgeschafft worden sein; eine
[Analyse zu einem entsprechenden BGH-Urteil im Juli 2018](https://www.heise.de/newsticker/meldung/Analyse-Stoererhaftung-durch-neue-Rechtsunsicherheiten-ersetzt-4121377.html)
macht allerdings deutlich, dass weiterhin Rechtsunsicherheit für
Betreiber offener WLANs besteht.)

### Digitale Selbstverteidigung

Gegen die Jagd von Datenkraken im Untergrund (etwa in Form von
Werbe-Netzen, Like- oder anderen Social-Media-Buttons) ist digitale
Selbstverteidigung erforderlich. Ich halte Werbe-Blocker und
anonymisierende Browser für unverzichtbar und erläutere deren
Funktionsweise auf diesen Seiten. (Web-Server-Betreiber, die unsere
Privatsphäre respektieren, auf Social-Media-Auftritte aber trotz
ethischer und
[rechtlicher](https://www.heise.de/newsticker/meldung/EuGH-Generalanwalt-Mithaftung-fuer-den-Datenschutz-bei-Like-Buttons-4257483.html)
Bedenken nicht verzichten wollen, verwenden Projekte wie
[c’t Shariff](https://www.heise.de/ct/artikel/Shariff-Social-Media-Buttons-mit-Datenschutz-2467514.html)
oder [embetty](https://www.heise.de/newsticker/meldung/Embetty-Social-Media-Inhalte-datenschutzgerecht-einbinden-4060362.html).)

Bei kostenlosen Diensten von US-Werbeunternehmen gibt es keine
private Kommunikation. So einfach ist das.

Kommen wir damit zum zweiten Teil der einleitenden Einschätzung:
Datenkraken wie Facebook und Google wissen nicht alles. Wenn Sie Ihnen
Ihre Daten aber auf dem Silbertablett servieren, wird natürlich gierig
zugegriffen, wobei „Ihre“ Daten zu „deren“ Daten werden, die auch der
Massenüberwachung zugänglich gemacht werden.

Ich versuche im Folgenden, Hinweise zu geben, wie Alice und Bob ein
Stück Kontrolle über die eigenen Daten und damit ihre Privatsphäre im
Internet gewinnen können. Die dabei vorgestellten Techniken zur
digitalen Selbstverteidigung wirken als Schutz vor Dritten, einerseits
um deren unkontrollierten Zugriff auf den eigenen Rechner und
andererseits deren Sammlung von Kommunikationsdaten zu erschweren. Dabei
spielt es keine Rolle, welche Interessen diese Dritten verfolgen –
sowohl der Identitätsdiebstahl durch Kriminelle als auch die
Profilbildung durch Werbenetze und Grundrechtsverletzungen durch
staatliche Stellen werden erschwert.

{% include labels.md %}
