---
layout: page
sequence: 11
title: Impressum und Kontakt
toctitle: Impressum und Kontakt
description: Impressum und Kontakt
keywords: Impressum, E-Mail
Time-stamp: "2019-07-24 17:09:08"
permalink: /Impressum_und_Kontakt.html
---

<address>
Dr. Jens Lechtenbörger, Dieckmannstr. 64, 48161 Münster
</address>
Internet: [https://www.informationelle-selbstbestimmung-im-internet.de/](https://www.informationelle-selbstbestimmung-im-internet.de/)

Inhaltlich Verantwortlicher gemäß § 55 RStV: Jens Lechtenbörger
(Anschrift wie oben)

Haftungshinweis: Trotz sorgfältiger inhaltlicher Kontrolle übernehme ich
keine Haftung für die Inhalte externer Links. Für den Inhalt der
verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.
Externe Links sind durch das Symbol „↑“ gekennzeichnet.

Ich pflege diese Web-Seiten *privat* in meiner
Freizeit. Ich hoffe, dass sie für Alice und Bob für ihr Training in
digitaler Selbstverteidigung nützlich sind, weise aber
[nochmals][disclaimer]
ausdrücklich darauf hin, dass die vorgestellten Projekte sich in
Entwicklung befinden und keines der Projekte Anonymität garantiert.
Ferner weise ich darauf hin, dass ich die Rechtslage zum Betrieb eines
Tor-Exit-Servers nicht beurteilen kann.

<a href="https://www.gnupg.org/" class="nooutlink"><img src="./public/logos/gnupg.png" alt="GnuPG Logo" class="logo"></a>
Wer möchte, kann mir [verschlüsselte E-Mails][gnupg] schicken. Die
E-Mail-Adresse ist in
[meinem öffentlichen OpenPGP-Schlüssel](./public/A142FD84.asc)
enthalten. Unverschlüsselte E-Mails an diese E-Mail-Adresse sind nicht
willkommen.

Mein Schlüssel wird durch folgende Informationen identifiziert:

    pub   2048R/A142FD84 2013-04-09
    7482 02DB 2697 708B 9C0C  4FA4 7D5C 06FE A142 FD84

Den öffentlichen Schlüssel können Sie auch von Key-Servern bekommen,
z. B.:

    gpg --keyserver hkp://subkeys.pgp.net --search-key 0x7D5C06FEA142FD84

Grundlagen zur E-Mail-Verschlüsselung finden Sie im Abschnitt zu
[GnuPG][gnupg]. Wenn Sie Interesse an einer Schulung zum Thema Verschlüsselung
haben, seien Sie auf das
[CryptoParty-Projekt](https://www.cryptoparty.in/location#germany)
verwiesen. Falls Sie die Verbreitung von OpenPGP unterstützen möchten,
präsentiert [Hauke Laging zahlreiche gute Ideen](http://www.openpgp-schulungen.de/fuer/unterstuetzer-personen/),
und die [Free Software Foundation Europe](https://fsfe.org/) bietet
[Flugblätter zur E-Mail-Selbstverteidigung an](https://fsfe.org/contribute/spreadtheword.de.html#gnupg-leaflet).

Eine E-Mail-Adresse, unter der auch
unverschlüsselte E-Mails willkommen sind, befindet sich eingebettet mit
Hilfe des Steganographie-Werkzeugs
[Digital Invisible Ink Toolkit](http://diit.sourceforge.net/)
(mit dem Algorithmus BattleSteg ohne Passwort) in dem Copyright-Bild, das
unten auf jeder Seite zu sehen ist. (Ich rate Ihnen, GnuPG zu lernen, statt
das Digital Invisible Ink Toolkit zu installieren.)

{% include labels.md %}
