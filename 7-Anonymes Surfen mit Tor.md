---
layout: page
sequence: 7
title: 'Informationelle Selbstbestimmung für mehr Privatsphäre im Web: Anonymes Surfen mit Tor'
toctitle: Anonymes Surfen mit Tor
description: Tor stärkt Privatsphäre und schützt vor Zensur.
keywords: Tor, Tor Browser, Anonymisierung, anonym, Web, surfen, Hidden Services, onion, Zwiebel, Dark Net
Time-stamp: "2019-07-24 17:08:55"
permalink: /Anonymes_Surfen_mit_Tor.html
---

<a href="https://www.torproject.org/" class="nooutlink"><img src="./public/logos/tor-logo.jpg" alt="Tor Logo" class="logo"></a>
[Tor](https://www.torproject.org/) ist ein Anonymisierungsdienst
basierend auf [freier Software](https://de.wikipedia.org/wiki/Freie_Software),
der die Privatsphäre von Alice und Bob der
[oben beschriebenen Idee von Chaum][proxies]
folgend durch mehrfaches, verschlüsseltes Weiterleiten ihrer Kommunikation
schützt, was als Anonymisierung bezeichnet wird.

![Warnung](./public/icons/stop.png){:.logo} Lesen Sie zuerst den
[allgemeinen Abschnitt zu Proxies][proxies], um
zu verstehen, was mit Anonymität im Internet gemeint ist!

Mit [Tor](https://www.torproject.org/) wird die Browser-Kommunikation
verschlüsselt über eine *zufällig gewählte*
Kette von Zwischenstationen (Relays) abgewickelt, die weltweit verteilt
sind. Dadurch sehen Dritte mit punktueller Kontrolle über die
Netzwerkinfrastruktur nur noch verschlüsselte Kommunikation, ohne zu
erfahren, wer mit wem kommuniziert. So sehen z. B. der Internet-Provider
von Alice oder die Betreiberin des von ihr genutzten WLANs nur noch ihre
verschlüsselte Kommunikation mit dem ersten Tor-Server, erfahren aber
nichts über den besuchten Web-Server. Diese Dritten lernen also
wesentlich weniger über Alice als zuvor, was ihre Privatsphäre schützt.
Der eigentlich von Alice besuchte Web-Server sieht demgegenüber nur eine
Anfrage des letzten Tor-Servers der Kette, des sogenannten *Tor-Exits*, weiß aber nicht, dass die Kommunikation
von Alice initiiert wurde, was wiederum ihrer Privatsphäre dient. Auf
diese Weise ermöglicht Tor das anonyme Surfen sowie die Anonymisierung
anderer Internet-Protokolle.

Dieses [zweiminütige Erklärvideo](https://media.torproject.org/video/2015-03-animation/HQ/Tor_Animation_de.webm)
beschreibt in einfachen Worten, warum Tor eine gute Idee ist und wie
es funktioniert.

Tor zeichnen folgende Besonderheiten aus. Erstens zielt Tor nicht nur
auf die Anonymisierung beliebiger Internet-Kommunikation (Web, Instant
Messaging, IRC, SSH …, siehe
[Torify Howto](https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO)),
sondern auch auf die Umgehung von Zensurmaßnahmen, um freie
Meinungsbildung und -äußerung in totalitären (und anderen) Staaten zu
ermöglichen. Zweitens sind die Tor-Server weltweit verteilt. Drittens
kann jede/r Einzelne einen
[eigenen Tor-Server betreiben](https://www.torproject.org/docs/tor-doc-relay.html),
von dem man dann schon mal weiß, dass er vertrauenswürdig ist. Die
Rechtslage zum Betrieb eines Exit-Tor-Servers in Deutschland kann ich
nicht beurteilen – es wurden schon Server
[beschlagnahmt](https://de.wikipedia.org/wiki/Tor_(Netzwerk)),
aber man kann sich auch mit
[Nicht-Exit-Servern (Relays)](https://www.torproject.org/docs/tor-doc-relay.html)
beteiligen; in Deutschland hilft der Verein [Zwiebelfreunde](https://www.zwiebelfreunde.de/).
Viertens ist der für das anonyme Surfen im Internet vorgesehene
[Tor Browser](https://www.torproject.org/projects/torbrowser.html)
nicht einfach ein speziell konfiguriertes Firefox-Profil, sondern ein
auf dem Firefox basierender Browser, dessen Ziele, Anforderungen und
Modifikationen in einem
[Design-Dokument](https://www.torproject.org/projects/torbrowser/design/)
veröffentlicht sind. Fünftens kooperiert Mozilla im Rahmen der
[Privatsphäre-Initiative Polaris](https://blog.mozilla.org/netpolicy/2014/11/10/introducing-polaris-privacy-initiative-to-accelerate-user-focused-privacy-online/)
mit dem Tor-Projekt, um die Privatsphäre aller Firefox-Nutzerinnen zu
stärken, wobei das
[Tor-Uplift-Projekt](https://wiki.mozilla.org/Security/Tor_Uplift/Tracking)
für die Integration der Modifikationen des Tor Browsers in den Firefox
sorgt. Sechstens gibt es von der NSA mit dem berühmten Zitat
[„Tor stinks“](https://netzpolitik.org/2013/nsa-sauer-tor-stinkt-weil-in-der-masse-funktioniert-anonymisierung/)
eine Art Qualitätsprädikat. Zu guter Letzt umfasst der Ansatz von Tor
auch sogenannte [Hidden Services](https://www.torproject.org/docs/hidden-services.html.en).

<a name="tor-hidden-service"></a>
Hidden Services (versteckte Dienste) sind Internet-Dienste mit
speziellen, auf `.onion` endenden Namen, bei denen sowohl Betreiberin
als auch Nutzer anonym bleiben können und die nur innerhalb des
Tor-Netzes mit Tor-Software erreichbar sind (was auch schon mal mit dem
Kampfbegriff „Dark Net“ belegt wird). In Zeiten von
grundrechtsverletzender Massenüberwachung sind solche Dienste als
digitale Selbstverteidigung für Alice und Bob besonders attraktiv, da
sämtliche Daten ausschließlich verschlüsselt im Tor-Netz übertragen
werden. Insbesondere gibt es keinen Tor-Exit, der die eigentlichen
Netzwerkdaten beobachten oder manipulieren könnte. Zudem wird
Ende-zu-Ende-Verschlüsselung sichergestellt, ohne dass wir irgendwelchen
Zertifizierungsstellen vertrauen müssten.

Populäre Betreiberinnen von Hidden Services sind zum Beispiel das unter
der Adresse
[http://ic6au7wa3f6naxjq.onion/](http://ic6au7wa3f6naxjq.onion/)
erreichbare [GnuPG-Projekt](http://ic6au7wa3f6naxjq.onion/) und – man
höre und staune – die unter
[http://facebookcorewwwi.onion/](http://facebookcorewwwi.onion/)
erreichbare Datenkrake [Facebook](http://facebookcorewwwi.onion/). Die
deutsche Metasuchmaschine
[MetaGer betreibt ebenfalls einen Hidden Service](https://metager.de/de/tor),
der unter
[http://b7cxf4dkdsko6ah2.onion/](http://b7cxf4dkdsko6ah2.onion/) zu
soliden, datenschutzfreundlich und anonym ausgelieferten Suchergebnissen
führt. (Alice hat den Hidden Service dieser Suchmaschine wie im Firefox
üblich durch einen Klick auf das grüne Pluszeichen neben dem Suchfeld zu
ihren Suchmaschinen hinzugefügt und durch „Sucheinstellungen ändern“ als
automatisch benutzte Suchmaschine definiert. Ihre anonymen Suchanfragen
verlassen das Tor-Netzwerk dadurch nicht mehr, auch nicht in Zeiten
illegaler Massenüberwachung.)
[Hidden Services des Tor-Projektes werden hier aufgelistet](https://onion.torproject.org/), die Tor-Web-Seite ist
unter [http://expyuzz4wqqyqhjn.onion/](http://expyuzz4wqqyqhjn.onion/)
erreichbar. Das
[Debian-Projekt bietet seit August 2016](https://blog.torproject.org/debian-and-tor-services-available-onion-services)
zahlreiche seiner
[Dienste über Hidden Services an](https://onion.debian.org/), insbesondere können Software und Updates
von Tor geschützt installiert werden, wozu das Paket
[apt-transport-tor](https://github.com/diocles/apt-transport-tor)
dient. Nach Installation dieses Pakets können in `/etc/apt/sources.list`
die normalen Software-Quellen durch Hidden Services ersetzt werden.
(Unabhängig vom Web sei darauf hingewiesen, dass Bob beispielsweise
seine eigenen
[SSH-Server als Hidden Services betreibt, um Verkehrsdatenanalysen zu erschweren](https://stribika.github.io/2015/01/04/secure-secure-shell.html).)

Besonders hervorheben möchte ich, dass die renommierte
[New York Times](https://www.nytimes.com/)
seit Ende Oktober 2017 als Onion-Dienst unter der Adresse
[https://www.nytimes3xbfgragh.onion/](https://www.nytimes3xbfgragh.onion/)
zu erreichen ist.  Laut
[eigener Aussage](https://open.nytimes.com/https-open-nytimes-com-the-new-york-times-as-a-tor-onion-service-e0d0b67b7482)
geschieht dies als Experiment in sicherer Kommunikation, wodurch die
Zeitung auch in Ländern gelesen werden kann, wo sie zensiert wird.
Die Privatsphäre der Leser wird geschützt, sie müssen keine Angst vor
Überwachung haben.  Dies ist umso bemerkenswerter, als
überregionale deutsche Tageszeitungen wie Süddeutsche und FAZ im Jahre
2017 noch nicht einmal per [HTTPS][ssl-tls] erreichbar waren, aber die
Aktivierung von JavaScript voraussetzten, was jede/r an IT-Sicherheit
Interessierte als verantwortungsloses Handeln erkennen kann.  Mit dem
von mir empfohlenen [NoScript][noscript] waren derart unsichere
Web-Seiten daher unbenutzbar.  Die New York Times hat hier
nachahmenswerte Maßstäbe gesetzt und ist deutschen Organen weit voraus
(die in 2018 immerhin HTTPS eingerichtet haben).

Für das anonymisierte Surfen wird Tor am einfachsten mit dem
[Tor-Browser-Paket](https://www.torproject.org/projects/torbrowser.html)
installiert, das sowohl Tor als auch den Tor Browser beinhaltet. Die
Software kann bei Bedarf direkt vom USB-Stick gestartet werden. Der Tor
Browser enthält Schutzmaßnahmen vor
[Fingerprinting][fingerprint], so können Sie
beispielsweise Zugriffe auf Canvas-Elemente unterbinden. Das oben
beschriebene [NoScript][noscript] ist im Tor Browser
enthalten und kann in seinen Wirkungen (neben weiteren die Sicherheit
erhöhenden Einschränkungen der Browser-Funktionalität) seit der
[im April 2015 erschienenen Version 4.5](https://blog.torproject.org/tor-browser-45-released)
durch einen Sicherheitsschieberegler (engl. *security slider*)
beeinflusst werden (erreichbar durch einen Klick auf die
Zwiebel links neben der Adressleiste, dann „Security Settings“; die
Zahl und Benennung der verfügbaren Sicherheitsebenen variiert in
verschiedenen Versionen des Tor Browsers).

![Sicherheitsregler im Tor Browser](./public/images/tor-browser-security-2018.png)

Generell verkörpert dieser Schieberegler einen von Alice und Bob zu
wählenden Kompromiss zwischen Sicherheit und Funktionalität.  Je mehr
Sicherheit, also Schutz vor (cross-device oder „normalem“)
[Tracking und Fingerprinting][fingerprint] sowie Schutz vor
Angreifern, die in ihre Rechner eindringen wollen, sie sich wünschen,
auf desto mehr potenziell gefährliche Funktionalität müssen sie
verzichten, indem sie den Regler von der niedrigsten Voreinstellung
(Standard) nach oben verschieben.  Dieser Verzicht hat zur Folge, dass manche
Web-Seiten nicht oder nur eingeschränkt funktionieren.  Persönlich
verwende ich eine der höheren Einstellungen.

Für den Tor Browser wird von der Installation weiterer Erweiterungen wie
Werbe-Blockern in den
[FAQ](https://www.torproject.org/docs/faq.html.en#TBBOtherExtensions)
abgeraten. (Wie im
[Design-Dokument](https://www.torproject.org/projects/torbrowser/design/)
genauer ausgeführt wird, setzen Modifikationen direkt am Browser an, um
Tracking und Fingerprinting entgegenzuwirken. Durch Erweiterungen würde
Ihr Browser-Verhalten von dem anderer abweichen, was die Möglichkeiten
für Ihre Identifikation erhöhen würde.) Spätestens der
[Skandal um die Browser-Erweiterung Web-of-Trust (WoT)](https://www.ndr.de/nachrichten/netzwelt/Nackt-im-Netz-Millionen-Nutzer-ausgespaeht,nacktimnetz100.html)
dürfte geklärt haben, dass Sie keine Browser-Erweiterungen installieren
sollten, die nicht ausgiebig von Sicherheitsexperten geprüft worden
sind. Lesen Sie die
[Sicherheitshinweise zu Tor](https://www.torproject.org/download/download#warning)
im Download-Bereich!

Ihre durch Tor anonymisierte IP-Adresse kann Alice auf dieser
[Tor-Testseite](https://check.torproject.org/) sehen.  Seit der
[im September 2018 erschienen Version 8.0 des Tor Browsers](https://blog.torproject.org/new-release-tor-browser-80)
wird der durch das Tor-Netz verwendete Pfad durch einen Klick auf das
Informationssymbol am linken Rand der Adressleiste angezeigt:
Hier ist ein Beispiel zu sehen, wo mein Zugriff
aus den USA zu kommen schien (mit Zwischenstationen in Frankreich und
der Tschechischen Republik).

![Tor-Knoten im Tor Browser](./public/images/tor-browser-circuit-2018.png)

Es ist übrigens normal und beabsichtigt, dass der erste Tor-Knoten (in
obiger Abbildung derjenige in den Niederlanden) als sogenannter
[Entry-Guard](https://www.torproject.org/docs/faq#EntryGuards) über
längere Zeit unverändert bleibt: Wenn diese Knoten regelmäßig gewechselt
würden, wäre es nahezu sicher, dass Mallory als Angreifer mit Kontrolle
über wenige Guards und Exits irgendwann jeden Tor-Nutzer überwachen
könnte. Details und offene Forschungsfragen zu diesem Thema finden sich
im
[Tor-Blog](https://blog.torproject.org/improving-tors-anonymity-changing-guard-parameters).

Darüber hinaus besteht ein bekanntes Problem bei der Nutzung von Tor
darin, dass manche Web-Seiten-Betreiber (oder ihre Dienstleister, in
der Vergangenheit insbesondere CloudFlare) Tor als „böse“ ansehen und
Zugriffe blockieren.  Dies lässt sich oft durch die Nutzung von
Suchmaschinen-Proxies umgehen, etwa von
[Startpage](https://startpage.com/) oder
[MetaGer (als Onion-Dienst)](http://b7cxf4dkdsko6ah2.onion/) für die
Web-Suche.  Wenn der direkte Zugriff auf ein Suchergebnis verweigert
wird, helfen eventuell „Anonyme Ansicht“ bzw. „anonym öffnen“ dieser
Suchdienste, was in folgenden Beispiel-Suchen hervorgehoben ist.  Dann
wird die Web-Seite nicht direkt von einem „bösen“ Tor-Exit geladen,
sondern vom Tor-Exit über einen Proxy des Suchmaschinenbetreibers, was
seltener sabotiert wird.

![Proxy-Funktion von Startpage](./public/images/startpage-proxy-2018.png)

![Proxy-Funktion von MetaGer](./public/images/metager-proxy.png)

Am Rande sei erwähnt, dass mit
[Orbot](https://www.torproject.org/docs/android) eine
Tor-Implementierung für Android existiert.  Unter Android ist der
[Tor Browser seit September 2018 in experimentellen Versionen verfügbar](https://blog.torproject.org/new-alpha-release-tor-browser-android)
(und löst damit [Orfox](https://guardianproject.info/apps/orfox/) ab).

Wer an Betriebssystemen interessiert ist, die sämtliche Daten automatisch
anonymisiert über Tor versenden, sei auf das im Rahmen der PC-Grundsicherung
erwähnte [Live-Linux Tails][tails]
verwiesen. Darüber hinaus integriert das dort ebenfalls genannte
[Qubes OS](https://www.qubes-os.org/) virtuelle Maschinen auf Basis von
[Whonix](https://www.whonix.org/),
bei denen sämtliche Netzwerkdaten automatisch durch Tor anonymisiert
werden. Während Tails ein [Live-System][live-cd] darstellt, das vom USB-Stick
oder von CD gestartet werden kann, ohne Spuren am eingesetzten Rechner zu
hinterlassen, ist Qubes OS ein vollwertiges Betriebssystem.

Nebenbei sei mit
[OnionShare](https://github.com/micahflee/onionshare)
auf eine Anwendung hingewiesen, mit der Dateien über Tor Hidden Services
anonym geteilt werden können. Dies funktioniert auf beliebigen
Rechnern ohne Installation von Server-Software und auch dann, wenn einer oder
beide der Rechner hinter NAT-Routern vernetzt sind und also eigentlich nicht
aus dem Internet erreichbar sind. Technisch startet die Anwendung einen
integrierten Web-Server, der über Tor als Hidden Service unter einer kaum
erratbaren `.onion`-Adresse verfügbar gemacht wird. Nur Personen, denen eine
derartige Adresse mitgeteilt wird, können über ihren Tor Browser auf die
zugehörige Datei zugreifen.

Abschließend sei die selbstverständliche Tatsache betont, dass
Tor wie jedes andere gute Werkzeug sowohl von „den Guten“ ge- als auch
von „den Bösen“ missbraucht werden kann. Wer bei Tor an den Kampfbegriff
„Dark Net“ denkt, sollte sich das
[SecureDrop-Verzeichnis](https://securedrop.org/directory/) ansehen, das
Hidden Services von Medien-Organisationen (z. B. Guardian und Washington
Post) auflistet, über die Sie und ich bestmöglich geschützt brisante
Informationen weitergeben können. Diese Dienste dürften zu den hellsten
Angeboten gehören, die es im Web gibt.

{% include labels.md %}
