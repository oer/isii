---
layout: page
sequence: 15
title: chaosreader
# Do not include in TOC # toctitle: chaosreader
description: This page on chaosreader is only of historical interest.
keywords: chaosreader
lang: en
Time-stamp: "2017-01-07 11:45:29"
---

I’m experimenting with
[chaosreader](http://chaosreader.sourceforge.net/) to analyze my
smartphone’s network traffic. (Smartphones are, let’s say, interesting
when privacy is concerned. I’ve got
[freed](https://fsfe.org/campaigns/android/android.en.html) ones, where
I tend to capture most network traffic via
[tcpdump](http://www.tcpdump.org/). Sometimes I feel like analyzing
what my phone does behind the scenes; then, in addition to chaosreader,
[Wireshark](https://www.wireshark.org/) is a great tool …)

As long as chaosreader was not actively maintained I published my
patched versions here. In June 2014, the original author, Brendan Gregg,
merged my patches in his [Git
repository](https://github.com/brendangregg/Chaosreader). Hence, the
following is only of historical interest now.

-   Switch to GPLv3.
-   Integrate [diff to reassemble chunked HTTP
    transfers](http://refrequelate.blogspot.com/2008/07/more-de-chunking-chaosreader-patch.html).
-   Parse linux cooked captures, which result from listening on “any”
    interface. (Chaosreader0.94 does not produce any output for such
    pcaps.)
-   Use HTTP content-type to identify file types such as HTML, XML,
    JavaScript, CSS; use those types for better file extensions than
    “data”.
-   More systematic Content-Type handling based on MIME types. (More
    image types included in Image Report based on MIME types.)
-   Uncompress gzip’ed data.
-   Add new command line switch (“-n”) to show host names in HTTPlog and
    to create href-links from HTTPlog rows to the corresponding rows in
    the table on index.html.
-   Add new command line switch (“-d”) to parse captured DNS replies and
    show DNS names instead of IP addresses on index page; save DNS
    replies as text files.
-   Prefer host names obtained from HTTP Host headers over IP addresses
    in HTTP logs.
-   Create new text HTTPlog file indicating referrers and cookies.
-   Extend GET/POST report to include all GET requests instead of only
    those containing parameters.
-   Create new “External Image Report” (linked from index.html), where
    images are embedded from their origin servers. In contrast, the
    “Image Report” points to images on the local hard disk. The new
    report may be more suitable for publication on Web pages as it does
    not require to publish (potentially copyright protected) images.
-   Show also empty parts on index.html that result from cache hits.
-   Create directory passed after switch “-D.”
-   Optimized hexadecimal dumps to use less memory.
-   Modified “IP Count” to “IP and MAC Count.”
-   Fixed a few bugs concerning output.
-   Treat port 8118 (polipo) and 9050 (Tor) as HTTP, also from localhost
    to localhost.

Downloads:

-   [GPLv3](https://www.gnu.org/licenses/gpl.html)
-   My versions of chaosreader
    -   0.95i, 2014-04-14
        -   [chaosreader0.95i](/chaosreader/chaosreader0.95i)
            ([chaosreader0.95i.asc](/chaosreader/chaosreader0.95i.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95i.diff](/chaosreader/chaosreader0.95i.diff)
            ([chaosreader0.95i.diff.asc](/chaosreader/chaosreader0.95i.diff.asc))
    -   0.95h, 2014-04-12, new version by Pavel Hančar
        -   [chaosreader0.95h](/chaosreader/chaosreader0.95h)
            ([chaosreader0.95h.asc](/chaosreader/chaosreader0.95h.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95h.diff](/chaosreader/chaosreader0.95h.diff)
            ([chaosreader0.95h.diff.asc](/chaosreader/chaosreader0.95h.diff.asc))
    -   0.95g, 2013-04-18
        -   [chaosreader0.95g](/chaosreader/chaosreader0.95g)
            ([chaosreader0.95g.asc](/chaosreader/chaosreader0.95g.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95g.diff](/chaosreader/chaosreader0.95g.diff)
            ([chaosreader0.95g.diff.asc](/chaosreader/chaosreader0.95g.diff.asc))
    -   0.95f, 2013-04-15
        -   [chaosreader0.95f](/chaosreader/chaosreader0.95f)
            ([chaosreader0.95f.asc](/chaosreader/chaosreader0.95f.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95f.diff](/chaosreader/chaosreader0.95f.diff)
            ([chaosreader0.95f.diff.asc](/chaosreader/chaosreader0.95f.diff.asc))
    -   0.95e, 2013-03-15
        -   [chaosreader0.95e](/chaosreader/chaosreader0.95e)
            ([chaosreader0.95e.asc](/chaosreader/chaosreader0.95e.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95e.diff](/chaosreader/chaosreader0.95e.diff)
            ([chaosreader0.95e.diff.asc](/chaosreader/chaosreader0.95e.diff.asc))
    -   0.95d, 2012-02-10
        -   [chaosreader0.95d](/chaosreader/chaosreader0.95d)
            ([chaosreader0.95d.asc](/chaosreader/chaosreader0.95d.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95d.diff](/chaosreader/chaosreader0.95d.diff)
            ([chaosreader0.95d.diff.asc](/chaosreader/chaosreader0.95d.diff.asc))
    -   0.95c, 2012-01-04
        -   [chaosreader0.95c](/chaosreader/chaosreader0.95c)
            ([chaosreader0.95c.asc](/chaosreader/chaosreader0.95c.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95c.diff](/chaosreader/chaosreader0.95c.diff)
            ([chaosreader0.95c.diff.asc](/chaosreader/chaosreader0.95c.diff.asc))
    -   0.95b, 2011-09-24
        -   [chaosreader0.95b](/chaosreader/chaosreader0.95b)
            ([chaosreader0.95b.asc](/chaosreader/chaosreader0.95b.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95b.diff](/chaosreader/chaosreader0.95b.diff)
            ([chaosreader0.95b.diff.asc](/chaosreader/chaosreader0.95b.diff.asc))
    -   0.95, 2011-09-11
        -   [chaosreader0.95](/chaosreader/chaosreader0.95)
            ([chaosreader0.95.asc](/chaosreader/chaosreader0.95.asc))
        -   Patch (diff file) for [chaosreader 0.94-3 shipped on
            Debian](http://packages.debian.org/squeeze/chaosreader):
            [chaosreader0.95.diff](/chaosreader/chaosreader0.95.diff)
            ([chaosreader0.95.diff.asc](/chaosreader/chaosreader0.95.diff.asc))
