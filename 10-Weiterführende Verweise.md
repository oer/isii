---
layout: page
sequence: 10
title: Weiterführende Verweise
toctitle: Weiterführende Verweise
description: Weiterführende Verweise zu Anonymität, Kryptographie und informationeller Selbstbestimmung
keywords: Kryptographie, Cryptoparty, Anonymität, Vorratsdatenspeicherung, Zensur
Time-stamp: "2017-02-25 14:49:22"
permalink: /Weiterführende_Verweise.html
---

-   Das
    [CryptoParty-Projekt](https://www.cryptoparty.in/location#germany)
    mit Schulungen zur Verschlüsselung
-   Die Kampagne
	[„Befreien Sie Ihr Android!“](https://fsfe.org/campaigns/android/android.de.html)
	der Free Software Foundation Europe und
	[Hardware-Empfehlungen](https://www.fsf.org/resources/hw/endorsement/respects-your-freedom)
	der Free Software Foundation
-   Der Roman [Little Brother](http://craphound.com/littlebrother/download/)
	von Cory Doctorow zum Wahn der Terrorabwehr. Klare Leseempfehlung,
    unterhaltsam, spannend und haarsträubend.
-   [Artikel zum verworrenen Web von Eben Moglen](https://blogs.fsfe.org/jens.lechtenboerger/2013/04/05/das-verworrene-web-das-wir-gewoben-haben/)
-   Informationen und Kampagnen der Arbeitskreise
    [Vorratsdatenspeicherung](http://www.vorratsdatenspeicherung.de/)
    und [Zensur](http://ak-zensur.de/)
-   Web-Seiten empfohlener Projekte
	-   [Firefox](https://www.mozilla.org/de/firefox/new/)
	-   [Tor und Tor Browser](https://www.torproject.org/)
    -   [GnuPG](https://www.gnupg.org/)
	-   [Freifunk in Deutschland](http://freifunk.net/) sowie
		[Freifunk im Münsterland](https://freifunk-muensterland.de/)
-   [Erläuterungen zur Anonymität im Internet](https://wiki.kairaven.de/open/anon/netzwerk/anet) mit
    umfangreicheren technischen Darstellungen als auf diesen Seiten
-   [Stand der Forschung zur Anonymität im Internet](https://www.freehaven.net/anonbib/topic.html)
	(englisch)
-   [Crypto-Gram Newsletter von Bruce Schneier](https://www.schneier.com/crypto-gram/)
	(englisch) mit
    Nachrichten und Kommentaren zu Datenschutz und IT-Sicherheit

{% include labels.md %}
