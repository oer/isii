---
layout: page
sequence: 8
title: 'Informationelle Selbstbestimmung für mehr Privatsphäre bei E-Mail: Verschlüsselung mit GnuPG/PGP'
toctitle: E-Mail-Verschlüsselung mit GnuPG
description: GnuPG ist freie Software zur E-Mail-Verschlüsselung
keywords: GnuPG, OpenPGP, Verschlüsselung, E-Mail, vertraulich, privat, Privatsphäre, Krypto-Party
Time-stamp: "2019-07-24 17:09:02"
permalink: /E-Mail-Verschlüsselung_mit_GnuPG.html
---

<a href="https://www.gnupg.org/" class="nooutlink"><img src="./public/logos/gnupg.png" alt="GnuPG Logo" class="logo"></a>
Wenn Alice und Bob Wert auf Privatsphäre legen oder einfach nicht
möchten, dass jede/r an der Übertragung Beteiligte (Internet- und
E-Mail-Provider, Geheimdienste, andere Kriminelle) ihre Nachrichten
lesen (und auch beliebig verändern) kann, müssen sie diese im Rahmen
digitaler Selbstverteidigung verschlüsseln.  Aus ihren Nachrichten
werden dann unverständliche Zeichenfolgen, die nur noch von Alice und
Bob selbst entschlüsselt werden können.  Haben Sie keine Angst:
Verschlüsseln ist nicht so kompliziert, wie oft behauptet wird.  Die
im Zusammenhang mit Verschlüsselung verwendeten Begriffe mögen
ungewohnt sein, kompliziert sind die grundlegenden Mechanismen aber
nicht.

Im Folgenden stelle ich die Grundlagen der sogenannten
[asymmetrischen](https://de.wikipedia.org/wiki/Asymmetrisches_Kryptosystem)
Kryptographie basierend auf Schlüsselpaaren vor, deren Kenntnis für
die E-Mail-Verschlüsselung notwendig sind.  Wenn Sie in der Wahl Ihrer
Kommunikationsmittel frei sind und vertraulich Kommunizieren möchten,
sollten Sie allerdings nicht auf E-Mail setzen, sondern auf den
bereits
[zuvor erwähnten und von Krypto-Experten empfohlenen Messenger Signal][signal]
oder einen [dezentralen Messenger][dezentrale-messenger].
Signal verkörpert auch im Jahre 2019 den Stand der Technik für sichere
Kommunikation und erfordert überhaupt keine kryptographischen
Kenntnisse auf Seiten von Alice und Bob, während E-Mail-Kommunikation
einerseits prinzipbedingt die zuvor erläuterten, Jahrtausende alten
Sicherheitseigenschaften des [Off-the-Record-Messaging][otr] nicht
garantieren kann und andererseits mit zahlreichen Altlasten und
Herausforderungen bei korrekter Umsetzung und Nutzung zu kämpfen hat.
Diese [Analyse (englisch, 2019) verdeutlicht die Probleme von GnuPG zur E-Mail-Verschlüsselung](https://latacora.singles/2019/07/16/the-pgp-problem.html).

Wenn Alice und Bob allerdings per E-Mail kommunizieren, gibt es keinen
Grund, dies ungeschützt im Klartext zu tun, der während der
Übertragung und auch im Postfach mitgelesen und modifiziert werden
kann.  Wenn Alice vertrauliche und unverfälschte Nachrichten empfangen
möchte, muss sie einen geöffneten Tresor bereitstellen.  Das ist
alles.

Bob kann seine Nachricht dann in den Tresor von Alice legen. Wenn er den
Tresor schließt, schnappt das Schloss zu, und niemand außer Alice –
weder Eve noch Mallory noch Bob selbst – kann auf die Nachricht
zugreifen. Nur Alice, die die öffnende Zahlenkombination (oder den
öffnenden Schlüssel) zum Tresor besitzt, ist jetzt noch in der Lage, an
Bobs Nachricht zu gelangen und diese zu lesen.

So einfach ist das. Alice benötigt nur einen Tresor, zu dem niemand
außer ihr die öffnende Kombination besitzt.

Im Rahmen der E-Mail-Verschlüsselung werden der Tresor und die
öffnende Zahlenkombination durch ein sogenanntes *Schlüsselpaar*
verkörpert.  So ein Schlüsselpaar besteht aus zwei Schlüsseln, einem
*öffentlichen* und einem *geheimen* (oder *privaten*) Schlüssel. Der
Tresor entspricht dem öffentlichen Schlüssel. Entsprechend
verschlüsselt Bob seine Nachrichten an Alice mit ihrem öffentlichen
Schlüssel. Die den Tresor öffnende Zahlenkombination entspricht dem
geheimen Schlüssel, mit dem Alice die an sie verschlüsselten
Nachrichten entschlüsselt. Wie die Begriffe „geheim“ und „öffentlich“
schon andeuten, muss Alice gut auf ihren geheimen Schlüssel aufpassen,
so dass dieser nie in fremde Hände gerät, während sie ihren
öffentlichen Schlüssel weitergibt.

Am Computer werden Tresor und öffnende Zahlenkombination durch Zahlen
dargestellt, und die Nachricht im geschlossenen Tresor ist
unverständlicher Kauderwelsch. Wie diese Zahlen genau aussehen, ist
Thema moderner Magie, der sogenannten
[asymmetrischen](https://de.wikipedia.org/wiki/Asymmetrisches_Kryptosystem)
(genauer der
[hybriden](https://de.wikipedia.org/wiki/Hybride_Verschl%C3%BCsselung))
Verschlüsselung, deren Details in zahlreichen Lehrbüchern dargestellt
werden. Alice und Bob erstellen und verwenden die notwendigen Zahlen mit
geeigneter Software; Bob macht das, ohne diese Magie zu verstehen.
(Alice und Bob können auch nicht erklären, wie die Magie eines
Fernsehers funktioniert, sie verwenden ihn trotzdem.)

[GnuPG](https://www.gnupg.org/) (*GNU Privacy Guard, gpg*,
deutsch etwa „Privatsphäre-Wächter“) ist bewährte
[freie Software](https://de.wikipedia.org/wiki/Freie_Software),
die diese Magie als Umsetzung des offenen
[Standards OpenPGP zur E-Mail-Verschlüsselung](https://de.wikipedia.org/wiki/OpenPGP)
nutzbar macht. Mit GnuPG stellt Alice ihren persönlichen, an
ihre E-Mail-Adresse gebundenen Tresor mit öffnender Zahlenkombination
her. Dann erzählt sie Familie, Freunden und Bekannten, dass diese ihre
Nachrichten von nun an in ihren Tresor legen sollen. Dafür müssen auch
diese GnuPG verwenden.

Das ist die zentrale Herausforderung.

Alice muss einen Tresor aufstellen, und ihre Gesprächspartner müssen den
Willen aufbringen, den Tresor zu benutzen (und eigene Tresore für die an
sie adressierten E-Mails aufstellen).

Eine gute Anleitung zur Nutzung von GnuPG mit dem E-Mail-Programm
[Thunderbird](https://www.thunderbird.net/de/) und der
GnuPG-Erweiterung [Enigmail](https://www.enigmail.net/index.php/en/)
ist die Seite zur
[E-Mail-Selbstverteidigung](https://emailselfdefense.fsf.org/de/).
Sie werden schrittweise von der Installation über die
Erzeugung eines Schlüsselpaares und den Austausch öffentlicher Schlüssel
bis zu Ver- und Entschlüsselung geleitet. Probieren Sie das unbedingt
aus. Verschlüsseln ist nicht so kompliziert, wie oft behauptet wird. Für
erste Tests beinhaltet die Anleitung zur
[E-Mail-Selbstverteidigung](https://emailselfdefense.fsf.org/de/) den
E-Mail-Roboter Edward (eine E-Mail-Adresse mit zugehörigem Programm, das
automatisiert auf Ihre Test-E-Mails reagiert).

Der vorangehende Test ist ein Auszug aus meinem
[Plädoyer für E-Mail-Verschlüsselung mit GnuPG](https://blogs.fsfe.org/jens.lechtenboerger/2013/12/11/alice-bob-und-der-geoeffnete-tresor-ein-plaedoyer-fuer-e-mail-verschluesselung-mit-gnupg/).
Lesen Sie bei Interesse doch
[dort](https://blogs.fsfe.org/jens.lechtenboerger/2013/12/11/alice-bob-und-der-geoeffnete-tresor-ein-plaedoyer-fuer-e-mail-verschluesselung-mit-gnupg/)
weiter. Zudem finden im Rahmen des
[CryptoParty-Projekts](https://www.cryptoparty.in/location#germany)
Schulungen mit „echten“ Menschen statt. Umfassende deutschsprachige
Darstellungen zu OpenPGP und GnuPG mit vielfältigen technischen Details
finden Sie bei
[Hauke Laging](http://www.hauke-laging.de/sicherheit/openpgp.html) und im
[Raven Wiki](https://wiki.kairaven.de/open/krypto/gpg/gpganleitung).

Hier weise ich noch nachdrücklich darauf hin, dass ich bewusst GnuPG
(und damit implizit den zugrunde liegenden Internet-Standard
[OpenPGP](https://de.wikipedia.org/wiki/OpenPGP)) empfohlen habe, aber
nicht S/MIME, was oftmals in Unternehmen zum Einsatz kommt. Für
Privatanwender ist S/MIME nicht zu empfehlen, weil (a) die Einrichtung
deutlich komplizierter ist, (b) sie gezwungen sind, in der Regel
nicht vertrauenswürdigen Zertifizierungsstellen zu
[„vertrauen“](https://blogs.fsfe.org/jens.lechtenboerger/2013/12/23/openpgp-and-smime/)
und (c) bei
[S/MIME jegliche Form von Integritätssicherung fehlt, was etwa im Rahmen von Efail-Angriffen ausgenutzt wird, um an den Klartext verschlüsselter Nachrichten zu gelangen](https://www.heise.de/select/ct/2018/12/1528508053834389).

Alice und Bob halten die nachfolgend skizzierte
[De-Mail][demail] für Quatsch, und auch das
[Sommermärchen 2013](http://www.ccc.de/de/updates/2013/sommermaerchen)
von mehr Sicherheit durch Umstellung auf verschlüsselten
E-Mail-Versand bei deutschen Providern ersetzt keine eigene
E-Mail-Verschlüsselung mit GnuPG.

Bei Verwendung von GnuPG kann z. B. Alice’ Internet-Provider nur noch
sehen, dass Alice vertrauliche E-Mails an Bob schickt, einschließlich
der nicht verschlüsselten Betreff-Zeile, kann aber den Text nicht mehr
lesen. Evtl. möchte Alice aber gar nicht, dass überhaupt jemand von
ihrer Kommunikation mit Bob weiß. Dann muss Alice weitergehende
Maßnahmen ergreifen und könnte z. B.
[Remailer](https://de.wikipedia.org/wiki/Remailer) verwenden, die nach
dem [oben beschriebenen Prinzip von Chaum][proxies] die E-Mail
mehrfach verschlüsselt über Zwischenstationen zu Bob weiterleiten und
dabei identifizierende Daten entfernen.

In der Vergangenheit habe ich hier [Mixminion](https://www.mixminion.net/)
empfohlen, ein Remailer-Projekt mit freier Software. Seit August 2013
warnt der Autor, dass die Software nicht mehr weiterentwickelt wird und
vermutlich nicht funktioniert. Eine Alternative sind
[Mixmaster-Remailer](https://de.wikipedia.org/wiki/Mixmaster-Remailer).

Abschließend sei erwähnt, dass mit
[OpenKeychain](https://www.openkeychain.org/) eine Android-Implementierung
von OpenPGP existiert. Eine damit einsetzbare E-Mail-App ist
[K-9 Mail, die seit Ende 2016 auch PGP/MIME unterstützt](https://k9mail.github.io/2016/12/26/K-9-Mail-5.200-released.html).
Wenn Sie mit einer älteren Version von K-9 Mail Probleme mit verschlüsselten
E-Mails hatten, sollten Sie das erneut ausprobieren.

Persönlich ist mir mein Smartphone zu unsicher, um darauf geheime Schlüssel zu
speichern: Ich entschlüssele nur am PC. Wenn Sie E-Mails nur am Smartphone
lesen und schreiben, gewinnen Sie allerdings immer noch an Sicherheit, wenn
Sie Ihre Schlüssel dem Gerät anvertrauen und auf verschlüsselte E-Mails
umsteigen.

{% include labels.md %}
