---
layout: page
sequence: 2
title: Internet-Grundlagen – IP-Adressen, Cookies, Web-Bugs, Fingerprinting und weitere technische Identifikationsmerkmale
toctitle: Internet-Grundlagen
description: Ein Crash-Kurs mit Grundlagen von Internet und Web
keywords: IP-Adressen, Cookies, Web-Bugs, Fingerprinting, DNS
Time-stamp: "2019-07-24 14:05:39"
permalink: /Grundlagen.html
---

Kommunikation im Internet ist in Analogie zum traditionellen Versand von
Briefen und Postkarten einfach zu verstehen (weitere Informationen
liefern einschlägige Lehrbücher über Rechnernetze): Zur Verbindung mit
dem Internet wird jedem Rechner vom Provider eine
[IP-Adresse](https://de.wikipedia.org/wiki/IP-Adresse) zugewiesen, so
dass der Rechner im Internet eindeutig adressierbar und damit
identifizierbar wird. Während Adressen auf Postkarten durch Namen,
Straßen sowie Postleitzahlen und Orte angegeben werden, sind IP-Adressen
(in Version 4 des Internet-Protokolls) 32 Bit lange Zahlen, die
typischerweise in der Form `85.176.2.1` geschrieben werden. (Jeweils 8
Bit werden zu einer Dezimalzahl zwischen 0 und 255 zusammenfasst, die
Dezimalzahlen werden durch Punkte getrennt.) Jedes im Internet
versendete Datenpaket besitzt immer Sender- und Empfänger-IP-Adresse
(anders als bei der Postkarte und ähnlich dem Brief, wobei die
Senderadresse eines Briefs im Prinzip keine Rolle spielt und auch fehlen
oder falsch sein kann). Ihre eigene IP-Adresse können Sie beispielsweise
auf [dieser Tor-Testseite](https://check.torproject.org/) sehen.

<a name="exkurs"></a>
**Exkurs**: „Was ist meine IP-Adresse“ scheint eine beliebte Frage
zu sein, insbesondere haben sich
in der Vergangenheit des Öfteren Kunden des Internet-Providers Alice auf
diese Seite verirrt. Daher sei genauer erklärt, wie Sie die eigene
Heimat im Internet mit IP-Adresse und autonomem System bestimmen können:
Das Internet setzt sich aus Teilnetzen zusammen, die
[autonome Systeme](https://de.wikipedia.org/wiki/Autonomes_System)
genannt und durch Nummern identifiziert werden und denen IP-Adressbereiche
zugewiesen sind. Alice-Kunden erhielten eine IP-Adresse aus dem
autonomen System mit der Nummer 13184, das von „HANSENET Telefonica
Germany GmbH & Co.OHG“ betrieben wird. Wenn Ihnen auf der
[Tor-Testseite](https://check.torproject.org/) z. B. die IP-Adresse
`85.176.2.1` angezeigt wird, können Sie diese im
[Web-Formular von Team Cymru](https://asn.cymru.com/cgi-bin/whois.cgi)
eingeben (Haken an „verbose“ setzen) und erhalten dann Informationen
zum autonomen System, dem Sie angehören. Im Beispiel erfahren Sie,
dass die IP-Adresse `85.176.2.1` dem autonomen System AS13184 von
HANSENET angehört und die IP-Adresse in das sogenannte IP-Präfix
`85.176.0.0/13` fällt. Dabei bedeutet „`/13`“, dass dies der
IP-Adressbereich mit genau den IP-Adressen ist, deren erste 13 Bit (in
Dualdarstellung) mit denjenigen von `85.176.0.0` übereinstimmen. (Die
Leserin mache sich zur Klausurvorbereitung klar, dass dies genau
diejenigen IP-Adressen einschließt, die mit 85.176, 85. …, 85.183
beginnen ;-)

Zurück zur Postanalogie: Für jede Datenübertragung im Internet (Abruf
einer Web-Seite, Versand einer E-Mail, Übertragung von Buchstaben im
Chat, …) werden – durch das
[Internet Protocol](https://de.wikipedia.org/wiki/Internet_Protocol) (IP)
vorgegeben – vom Rechner des Senders alle Daten in IP-Pakete aufgeteilt,
die mit IP-Adressen von Sender und Empfänger versehen sind. Diese Pakete
werden anhand der Empfänger-IP-Adresse (bzw. dem der Postleitzahl
entsprechenden IP-Präfix) schrittweise über spezielle Rechner (sog.
*Router* – diese entsprechen Postkästen und Briefverteilzentren) zum
Empfänger weitergeleitet. Mittels der Absenderadresse kann der Empfänger
dann Antworten verschicken.

Diese Darstellung verdeutlicht, dass jeder Router entlang des Weges von
der Absenderin Alice zum Empfänger Bob sehen kann,
*dass* Alice und Bob kommunizieren (so wie für
jede/n am Transport eines Briefs Beteiligte/n die Anschriften von
Absender und Empfänger sichtbar sind). Wenn die Kommunikation
unverschlüsselt abläuft, kann jeder Router darüber hinaus sehen,
*worüber* Alice und Bob sich austauschen und die
Nachrichtentexte beliebig ändern (so wie eine mit Bleistift geschriebene
Postkarte überall während des Transportes gelesen und geändert werden
kann). Spätestens seit der [Überwachungs- und Spionageaffäre 2013](https://de.wikipedia.org/wiki/%C3%9Cberwachungs-_und_Spionageaff%C3%A4re_2013)
sollte klar sein, dass zumindest Geheimdienste alles mitlesen. Darüber
hinaus sind Router auch nur Computer mit Betriebssystem und allem, was
dazugehört: „Normale“ Schwachstellen, die von Geheimdiensten und
anderen Kriminellen für Spionage- und Sabotagezwecke ausgenutzt
werden, nachgewiesen etwa [für die NSA in 2016](https://www.heise.de/security/meldung/NSA-Exploits-Cisco-schliesst-weitere-Einfallstore-in-Firewalls-3301380.html),
für [die CIA in 2017](https://www.heise.de/security/meldung/Sicherheitsupdate-in-Sicht-Gravierende-Telnet-Luecke-bedroht-zahlreiche-Cisco-Switches-3658915.html),
für jeden (mindestens
[sieben Hintertüren in 2018 bei Cisco](https://www.zdnet.com/article/cisco-removed-its-seventh-backdoor-account-this-year-and-thats-a-good-thing/),
weitere bei der Konkurrenz von Juniper
[in 2018](https://www.heise.de/security/meldung/Sicherheitsupdates-Junipers-Junos-OS-offen-fuer-Fernzugriff-ohne-Passwort-4188397.html)
und [in 2019](https://www.heise.de/security/meldung/Sicherheitsluecken-mit-Hoechstwertung-in-Juniper-ATP-4271009.html));
[absichtlich von Geheimdiensten (2014)](https://www.theguardian.com/books/2014/may/12/glenn-greenwald-nsa-tampers-us-internet-routers-snowden)
oder [absichtlich von Geheimdiensten und unbekannten Parteien (2015)](https://www.heise.de/security/meldung/Schnueffelcode-in-Juniper-Netzgeraeten-Weitere-Erkenntnisse-und-Spekulationen-3051260.html)
eingebauten Hintertüren,
[Bot-Netzen (2013)](https://www.heise.de/ct/ausgabe/2013-21-Hinter-den-Kulissen-eines-Router-Botnets-2313886.html)
und [immer (2016)](https://www.heise.de/security/meldung/Auch-Standard-Passwoerter-von-Unitymedia-Routern-leicht-knackbar-3071738.html)
[wieder (2018)](https://www.heise.de/meldung/MikroTik-Hunderttausende-Router-schuerfen-heimlich-Kryptogeld-4243857.html)
Schwachstellen in heimischen WLAN-Routern
(WLAN-Router sind keine „echten“ Backbone-Router; für
Betroffene macht das aber keinen Unterschied).

Angesichts derartiger Schwachstellen in der Internet-Infrastruktur
sollte klar sein, dass digitale Selbstverteidigung unerlässlich ist.
Für Alice und Bob spielt es keine große Rolle, ob sie von
westlichen oder östlichen Geheimdiensten, wirtschaftlichen
Konkurrenten oder gewöhnlichen Kriminellen angegriffen werden.  Auch
angesichts der Vorwürfe um Spionage durch 5G-Ausrüstung von Huawei
in den Jahren [2018](https://www.spiegel.de/netzwelt/gadgets/huawei-und-zte-viele-spionage-vorwuerfe-null-beweise-a-1242276.html)
[und 2019](https://www.spiegel.de/netzwelt/web/spionageverdacht-vodafone-fand-sicherheitsluecken-in-huawei-produkten-a-1265138.html)
kann ich keine Strategie für Unabhängigkeit auf europäischer Ebene
erkennen.

Im Gegensatz zum traditionellen Postversand besteht eine Besonderheit
im Internet darin, dass Alice sich bei der Einwahl ins Internet, also
insbesondere vor *jeder* Kommunikation bei ihrem Internet-Provider
ausweisen muss, und dass *sämtliche* ihrer Kommunikationsdaten über
mindestens einen Router ihres Providers vermittelt werden. (In
Analogie zur Post hieße das, dass Alice jede Postsendung unter
Ausweiskontrolle absenden müsste.) Dementsprechend ist Alice’ Provider
(bzw. jeder Dritte mit Kontrolle über dessen Rechner, z. B. Kriminelle
oder staatliche Stellen) in der Lage, jeden ihrer Schritte im Internet
zu protokollieren. Verständlicherweise möchte Alice das nicht, und
daher setzt sie [Tor als Anonymisierungstechnik][tor] ein, um dies zu
verhindern.  (Der Provider kann auch eine längere Internet-Abstinenz
des Internet-Junkies Alice wahrnehmen und interpretieren.)

<a name="geolocation"></a>
Schließlich sollte klar sein, dass mit jeder IP-Adresse auch
*physische Ortsinformationen* verknüpft sind.
Jede Internet-Kommunikation erlaubt Rückschlüsse über die
Aufenthaltsorte von Sender und Empfänger.  Die Zuordnung eines Ortes
(entweder durch Längen- und Breitengrad als Geokoordinaten oder als
Anschrift mit Straße und Ort) wird als *Geolocation* oder
*Geotargeting* bezeichnet.  Die Genauigkeit der Ortung hängt von der
Art der IP-Adressvergabe ab.  Bei statischen IP-Adressen, die einem
Gerät über einen längeren Zeitraum fest zugeordnet werden, kann die
Ortung äußerst genau erfolgen (etwa bei meinem Arbeitsplatzrechner,
der seit Jahren mit derselben IP-Adresse im selben Büro im selben
Gebäude steht), bei dynamischen IP-Adressen, die Geräten an
wechselnden Standorten zugewiesen werden, kommen sämtliche der
wechselnden Standorte in Frage.  Je nach Internet-Anschluss fallen die
zugehörigen Regionen unterschiedlich groß aus.  Die Web-Seite
[WhatsMyIP](https://www.whatsmyip.org/ip-geo-location/) kann meine
dynamische IP-Adresse am heimischen PC immerhin korrekt meiner
Heimatstadt zuordnen.  Verständlicherweise möchte Alice nicht bei
jeder Messenger-Nachricht und jedem Web-Seitenabruf Spuren ihres
Aufenthaltsortes hinterlassen, die von Dritten für unbekannte Zwecke
genutzt werden können.  Daher setzt sie
[Tor als Anonymisierungstechnik][tor] ein, um dies zu verhindern.

Im Internet tummeln sich viele Parteien, die daran interessiert sind,
Profile über Alice zu erstellen, beispielsweise für personalisierte
Werbung („… dieser Schuh könnte Ihnen gefallen …“), für
individualisierte Versicherungstarife („… wenn Sie zusätzlich zu Ihrer
Autoversicherung auch die Krankenversicherung bei uns abschließen,
weniger Zeit am Rechner verbringen und zudem aufhören, online so viel
Pizza zu bestellen, haben wir das passende Angebot …“) oder zur
Terrorabwehr („… die da hat ähnliche Interessen wie Osama – die nehmen
wir mit, um sie ~~unter Folter zu verhören~~ mit
[erweiterten Verhörmethoden](https://www.zeit.de/politik/deutschland/2014-12/central-intelligence-agency-folter-bericht-veroeffentlicht)
(engl. *[enhanced interrogation techniques](https://en.wikipedia.org/wiki/Enhanced_interrogation_techniques)*)
zu befragen …“). Da Alice bei jeder Internet-Nutzung von ihrem Provider
eine andere IP-Adresse zugewiesen bekommen könnte, müssen diese Parteien
auf andere Techniken zurückgreifen, um Alice zuverlässig zu
identifizieren bzw. ihre Handlungen unter verschiedenen IP-Adressen
miteinander in Beziehung zu setzen.

Nun ist es leider so, dass eine derartige Fülle von Identifikations- und
Überwachungsmechanismen existiert, dass eine manuelle Konfiguration des
Browsers mit dem Ziel informationeller Selbstbestimmung aussichtslos
erscheint. Alice und Bob setzen daher auf die von Datenschutzexperten
entwickelte Firefox-Variante [Tor Browser][tor]. Der Rest dieses
Abschnitts geht auf Identifikationsmechanismen ein, denen diese
Firefox-Variante entgegenzuwirken versucht.

Das traditionell auf Web-Servern eingesetzte Mittel zur
Nutzeridentifikation und -überwachung, auch bei wechselnden IP-Adressen,
sind [Cookies](https://de.wikipedia.org/wiki/HTTP-Cookie).
Vereinfachend kann man sich einen Cookie als einen Ausweis (mit einer
vorgegebenen, evtl. sogar unbegrenzten Gültigkeit) vorstellen, den ein
Web-Server, nennen wir ihn Sara, Alice’ Web-Browser übergibt. Der
Browser zeigt diesen Ausweis dem Server Sara bei jedem weiteren Besuch
von Alice, ohne dass Alice etwas machen müsste. Alice muss von der
Existenz dieses Ausweises nicht einmal wissen. Dadurch kann Sara genau
protokollieren, wann Alice welche Web-Seiten bei ihr angesehen hat. Wenn
Sara z. B. eine Online-Händlerin mit umfangreichem Sortiment ist oder
auch eine Suchmaschine, kann sie viel über Alice lernen.
Verständlicherweise möchte Alice das nicht, und daher konfiguriert sie
ihren Browser so, dass sie Kontrolle über Cookies erhält.

Cookies haben für neugierige Parteien den Nachteil, dass sie vom
Web-Browser nur Servern der ausstellenden
[DNS-Domäne](https://de.wikipedia.org/wiki/Domain_Name_System)
präsentiert werden. Wenn also z. B.
[https://www.informationelle-selbstbestimmung-im-internet.de/](https://www.informationelle-selbstbestimmung-im-internet.de/)
Cookies an Web-Browser verteilt, dann werden diese Cookies nur Servern
präsentiert, deren Name auf
`informationelle-selbstbestimmung-im-internet.de` endet.

<a name="web-bugs"></a>
Hier kommen
[Web-Bugs](https://de.wikipedia.org/wiki/Web-Bug)
sowie Like- und andere Social-Media-Buttons als weit verbreitete
Techniken zur Erstellung von Profilen über Grenzen von DNS-Domänen ins
Spiel. Web-Bugs sind typischerweise unsichtbare
*Verweise* (z. B. winzige Bilder, die nur einen
Bildpunkt belegen) auf einen anderen Server Eve, während
Social-Media-Buttons diese Verweise mit sichtbaren Icons kombinieren. In
jedem Fall wird so ein Verweis auf einer Web-Seite von Sara so
eingebettet, dass Alice’ Web-Browser dem Verweis zu Eve
*automatisch* folgt, wenn Alice Sara besucht. (Wenn
Sara ihren Social-Media-Auftritt bewerben möchte, aber Alice’
Privatsphäre respektiert, setzt sie [c’t
Shariff](https://www.heise.de/ct/artikel/Shariff-Social-Media-Buttons-mit-Datenschutz-2467514.html)
zur Anzeige der Social-Media-Buttons ein. Dann funktioniert der Verweis
ohne automatische Weiterleitung.)

Eve kann zusammen mit dem Bild auch Cookies an Alice’ Web-Browser
senden. Bei dem im Hintergrund stattfindenden Aufruf des Inhalts von Eve
sendet Alice’ Browser zudem einen sogenannten
*Referrer*-Header an Eve (der im HTTP-Standard als
`HTTP_REFERER` bezeichnet wird), der Alice als Besucherin der Web-Seite
Sara identifiziert. Zusätzlich kann der Aufruf an Eve beliebige, von
Sara festgelegte Informationen in HTTP-Parametern weiterreichen. Auf
diese Weise erfährt Eve, welche Seiten Alice sich bei Sara anschaut,
ohne dass Alice wissen müsste, dass Eve überhaupt existiert.
Typischerweise bezahlt Eve viele Server-Betreiber dafür, Verweise auf
ihre Server anzulegen, um die Besuche auf verschiedenen Web-Servern
miteinander korrelieren zu können und so möglichst viel über Alice zu
lernen – ein Bespitzeln und Ausspionieren im Hintergrund, das
euphemistisch auch als
[Retargeting](https://www.heise.de/tr/artikel/Online-Werbung-die-kleben-bleibt-1766853.html)
bezeichnet wird. Im [folgenden Abschnitt][collusion] wird
dieser Sachverhalt mit Hilfe der Firefox-Erweiterung
[Collusion/Lightbeam](https://addons.mozilla.org/en-US/firefox/addon/lightbeam/)
visualisiert. Verständlicherweise möchte Alice nicht derart gläsern
werden, greift zu digitaler Selbstverteidigung und konfiguriert ihren
Browser so, dass sie ein wenig Kontrolle über Bilder fremder Server und
damit auch Web-Bugs erhält.  Wie später zur
[Grundsicherung][grundsicherung-des-pcs]
ausgeführt installiert Alice zum „normalen“ Surfen insbesondere einen
Werbe-Blocker (auch Ad-Blocker oder Tracking-Blocker genannt) wie die
Firefox-Erweiterung
[uBlock Origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/),
die sie nicht nur vor dem Bespitzeln im Hintergrund, sondern auch vor
dem später genauer erläuterten [Malvertising][malvertising] schützt.
In einem Test im September 2017
[empfiehlt auch die Stiftung Warentest sehr deutlich, nicht ohne Werbe-Blocker zu surfen; Testsieger wurde uBlock Origin](https://www.test.de/Tracking-Wie-unser-Surfverhalten-ueberwacht-wird-und-was-dagegen-hilft-5221609-0/).

Weiterhin können Web-Bugs auch in E-Mails eingebettet werden, wodurch
Dritte lernen,
[ob, wann, von wo und wie lange](https://www.golem.de/0407/32178.html)
Alice speziell präparierte
E-Mails liest. Verständlicherweise möchte Alice das nicht, und daher
benutzt sie ein E-Mail-Programm, das E-Mails (auch HTML-Inhalte) als
ASCII-Texte anzeigt. (Die Internet-Götter gebieten in der
[Netiquette](https://www.netplanet.org/netiquette/email.shtml), dass man
mittels E-Mail Textnachrichten schreibt, dass man größere Dateien mit
Dateitransferprogrammen überträgt und dass man sich bunte Seiten im
Web-Browser ansieht.  Alice ist zwar nicht gläubig, aber einsichtig.
Bob ist überrascht, dass im Jahre 2018 noch
[Forschungsartikel in einschlägigen Sicherheitskonferenzen erscheinen müssen, um vor HTML-E-Mails und schlechten E-Mail-Clients zu warnen](https://freedom-to-tinker.com/2017/09/28/i-never-signed-up-for-this-privacy-implications-of-email-tracking/).
Etwas später wurde dann in 2018 die
[Sicherheitskatastrophe Efail für verschlüsselte E-Mails](https://www.heise.de/select/ct/2018/12/1528508053834389)
bekannt, die im Wesentlichen auf die Verwendung von HTML für E-Mails
zurückgeht, aber auch zeigt, dass S/MIME als kaputt angesehen werden
muss.  Aufklärung ist notwendig.)

Zudem können selbst auf den ersten Blick harmlose
Mechanismen wie der Browser-Cache und die Chronik missbraucht werden, um
Alice’ Surfverhalten auszuspionieren. Durch
[Caching](https://de.wikipedia.org/wiki/Cache) kann ein Browser
Web-Inhalte auf der lokalen Festplatte speichern, so dass beim Besuch
einer Web-Seite nicht jedes Mal alle Inhalte aus dem Internet geladen
werden müssen, sondern im Idealfall nur noch die, die sich seit dem
letzten Besuch geändert haben. Obwohl Caching im Internet zunächst eine
nützliche Technik ist, um Latenzzeiten zu reduzieren und Bandbreite zu
sparen, kann sie auch missbraucht werden, um Benutzer unbemerkt zu
identifizieren.

Um Caching zu ermöglichen, sendet ein Web-Server neben der eigentlichen
Web-Seite auch Verwaltungsinformationen in HTTP-Headern, damit Browser
und Server sich einigen können, welche Inhalte bereits im Cache
vorliegen und welche noch geladen werden müssen. Beim Besuch von
[https://www.informationelle-selbstbestimmung-im-internet.de/](https://www.informationelle-selbstbestimmung-im-internet.de/)
sendet der Web-Server unter anderem einen ETag-Header wie den folgenden:

    Etag: "1e9805a-1ef4-443906cc"

Der Browser legt die Seite dann in seinem Cache ab und sendet bei
folgenden Aufrufen der Seite
[https://www.informationelle-selbstbestimmung-im-internet.de/](https://www.informationelle-selbstbestimmung-im-internet.de/)
unter anderem den folgenden HTTP-Header:

    If-None-Match: "1e9805a-1ef4-443906cc"

Dadurch ist der Web-Server zunächst in der Lage zu erkennen, ob sich die
Seite zwischenzeitlich geändert hat oder nicht. Entsprechend kann er dem
Browser entweder mit einer aktualisierten Seite antworten oder ihm
mitteilen, dass dessen Version im Cache noch aktuell ist. So weit, so
gut.

Für Alice ist allerdings nicht zu erkennen, welche Informationen sich
hinter dem Wert `1e9805a-1ef4-443906cc` verbergen. Es ist insbesondere
nicht klar, ob alle Besucher denselben Wert erhalten (und an den
Web-Server zurücksenden) oder ob der Web-Server für jeden Besucher einen
identifizierenden Wert erzeugt. Falls letzteres der Fall sein sollte,
kann der Web-Server jeden Besucher mit Hilfe des Caching-Mechanismus
identifizieren.

Allgemein empfehle ich, den Browser so zu konfigurieren, dass Cookies
und Cache beim Beenden gelöscht werden (über das Menü „Bearbeiten“ /
„Einstellungen“ / „Datenschutz“ / „Chronik“ / „nach benutzerdefinierten
Einstellungen anlegen“). Die Firefox-Variante [Tor Browser][tor]
ist bereits richtig konfiguriert.

Eine andere Partei, die viel über Alice’ Internet-Aktivitäten lernt, ist ihr
[DNS-Server](https://de.wikipedia.org/wiki/Domain_Name_System). DNS hat
im Internet die zentrale Aufgabe, den für Menschen lesbaren Namen von
Web-Seiten wie `www.informationelle-selbstbestimmung-im-internet.de` die
für die maschinelle Kommunikation notwendigen IP-Adressen zuzuordnen.
Wenn Alice also `www.informationelle-selbstbestimmung-im-internet.de` in
ihrem Browser eintippt, befragt der Browser zunächst einen DNS-Server
nach einer IP-Adresse zu diesem Namen und führt die Kommunikation dann
unter Verwendung der erfragten IP-Adresse durch. Aus der DNS-Anfrage des
Browsers lernt der Betreiber des DNS-Servers, dass Alice sich für
informationelle Selbstbestimmung im Internet interessiert.
Verständlicherweise möchte Alice das nicht, und daher setzt sie
[Tor als Anonymisierungstechnik][tor] ein, um
dies zu verhindern. Außerdem ist sie sorgfältig in der Wahl alternativer
DNS-Server; solche bei großen Suchmaschinenbetreibern in den USA, die
Geschwindigkeitsvorteile beim Surfen versprechen, verwendet sie nicht.

Schließlich gibt es zahlreiche technische Merkmale, die in der
Internet-Kommunikation übertragen werden und dazu beitragen, dass Alice
identifiziert werden könnte. So werden bei jedem Abruf einer Web-Seite
diverse technische Informationen in HTTP-Headern übertragen, unter
anderem Name und Version des verwendeten Browsers (`HTTP_USER_AGENT`)
sowie verweisende Web-Seite (`HTTP_REFERER`) und für Caching relevante
Daten (z. B. `ETag`). Was Ihr Browser berichtet, können Sie auszugsweise
[dieser HTTP-Demonstration](http://ip-check.info?lang=de) entnehmen
oder mit Hilfe der
[Netzwerkanalyse der Web-Entwickler-Werkzeuge im Firefox](https://developer.mozilla.org/de/docs/Tools/netzwerkanalyse)
ansehen.

<a name="fingerprint"></a>
Verschiedene technische Merkmale lassen sich
zu sogenannten Fingerabdrücken (engl.
*fingerprints*) zusammenfassen, die sich wie „echte“
Fingerabdrücke für Identifikationszwecke nutzen lassen. Für
unterschiedliche Browser, Betriebssysteme und Netzwerkprotokolle,
Rechner, Grafikkarten und Treiberversionen, installierte
Browser-Plugins, installierte Schriftarten, Bildschirmauflösungen und
vieles mehr werden technische Unterschiede in derartigen Fingerabdrücken
zusammengefasst und bieten mächtige Wiedererkennungsmöglichkeiten der
Surfer durch Web-Seitenbetreiber, die *ohne*
Cookies funktionieren. Im Jahre 2010 erfuhren Fingerabdrücke mit der
Vorstellung des Projekts [Panopticlick](https://panopticlick.eff.org/)
größere Bekanntheit. Inzwischen sind zahlreiche Fingerprinting-Techniken
dazugekommen, insbesondere
[Canvas-Fingerprinting](https://de.wikipedia.org/wiki/Canvas_Fingerprinting)
im Jahre 2014, und werden webweit eingesetzt, um uns und unsere
Vorlieben hinterrücks kennenzulernen. Auf der (Ende 2015 überarbeiteten)
[Projektseite von Panopticlick](https://panopticlick.eff.org/) können
Sie das Identifikationspotenzial eines Fingerabdrucks Ihres Browsers
berechnen lassen, alternativ auf
[Browserprint.info](https://browserprint.info/)
oder bei [Am I Unique](https://amiunique.org/)
mit Techniken vom Sommer 2016.  Letztere kommen auch bei
[Uniquemachine (2016)](http://uniquemachine.org/)
zum Einsatz, einem Forschungsprojekt, das angibt, Browser mit einer
Genauigkeit von über 99% identifizieren zu können.
Ein [Artikel zu JavaScript-Template-Angriffen (2019, englisch) zeigt die Vielfalt von Browser-Eigenschaften](https://www.ndss-symposium.org/wp-content/uploads/2019/02/ndss2019_01B-4_Schwarz_paper.pdf),
die für Fingerprinting genutzt werden können, auch im Tor Browser.

Die bisherigen Techniken dienen dazu, Profile über unser Surfverhalten
auf einzelnen Geräten zu erstellen. Ende 2015 rückte darüber hinaus eine
besonders hinterhältige Form von Cross-Device-Tracking in die
Diskussion. Beim Cross-Device-Tracking geht es darum, unser Verhalten
und unsere Interaktionen mit verschiedensten Geräten (engl.
*devices*) zu überwachen, etwa am heimischen PC oder
SmartTV, im Urlaub mit E-Book-Reader und Tablet, unterwegs mit dem
Smartphone. Besonders einfach ist dieses Tracking natürlich für
Datenkraken wie Google und Facebook, bei denen sich Benutzer mit
verschiedenen Geräten freiwillig mit derselben Kennung anmelden.
Empörung haben jedoch hinterhältigere
[Tracking-Techniken erregt, die mit Ultraschall arbeiten](https://www.golem.de/news/tracking-ultraschallschnipsel-verfolgen-nutzer-ueber-mehrere-geraete-1511-117483.html):
Ein Gerät sendet dabei für Menschen unhörbare Ultraschalltöne aus, die
von den Mikrophonen anderer Geräte aufgenommen werden; alle beteiligten
Geräte tauschen durch diese Töne untereinander Daten aus und senden
diese an Datenkraken im Internet, wo dann wieder umfangreiche
Beziehungen zwischen Geräten und Menschen erstellt und an unbekannte
Parteien zu unbekannten Zwecken verkauft werden.

Alice und Bob glauben nicht, dass sie Fingerprinting – weder auf
einzelnen Geräten noch cross-device – durch manuelle
Browser-Einstellungen verhindern können.

Um die Wirksamkeit von Fingerprinting zu reduzieren, müssen möglichst
viele Browser ununterscheidbare Fingerabdrücke aufweisen. Dies setzt
voraus, (a) dass der Browser nicht jedes Detail über sich und seine
Ausführungsumgebung ausplaudert, was größere Eingriffe in das
Browser-Verhalten erfordert, und (b) dass Alice und Bob das Verhalten
ihres Browsers nicht durch manuelle Anpassungen aus der Masse
hervorstechen lassen. Aktuell scheint mir der [Tor Browser][tor]
mit dem integrierten
[TorButton](https://www.torproject.org/docs/torbutton/) und
deaktiviertem JavaScript den besten Schutz zu bieten.

Ich betone, dass es mir in den obigen Beschreibungen um Situationen
geht, wo Alice’ Verhalten protokolliert wird, ohne dass es einen Zweck
gäbe, dem Alice zugestimmt hätte. Anders sieht die Situation aus, wenn
Alice sich durch Angabe von Kennung und Passwort identifiziert, um eine
Leistung in Anspruch zu nehmen, etwa beim E-Mail-Abruf über ein
Web-Interface oder beim Online-Kauf. Auch in solchen Situationen kommen
Cookies zum Einsatz, um Alice’ Aktionen zu protokollieren. Dies ist hier
auch erforderlich, damit Alice beispielsweise genau die Dinge in ihrem
Einkaufskorb und auf ihrer Rechnung wiederfindet, die sie haben wollte.
Alice gibt ihre Identität zweckgebunden preis und weiß (hoffentlich),
worauf sie sich einlässt.

In der Praxis stellt sich die Frage der Zustimmung zugegebenermaßen oft
kompliziert dar. Unverständliche Nutzungs- und Datenschatzbedingungen
erfordern die Einwilligung in die Weitergabe von Daten, manchmal
angeblich anonymisiert oder pseudonymisiert, manchmal offen in
detaillierter Form. Persönlich halte ich es mit folgender, vereinfachter
Faustformel: Daten sind entweder anonymisiert oder nützlich. Da keine
Datenkrake Interesse an unnützen Daten hat, wird es mit Anonymisierung
nicht weit her sein. (Ich bin mir bewusst, dass es Ansätze wie
[„Differential Privacy“](https://en.wikipedia.org/wiki/Differential_privacy)
gibt, unter denen anonymisierte Daten nützlich sein könnten. Ihr
vertrauenswürdiger Einsatz basierend auf freier Software, auch im
Server-Umfeld, erscheint mir aber unrealistisch.) In der Tat gibt es
zahlreiche Beispiele „anonymisierter“ Datensammlungen, in denen
Einzelpersonen identifiziert wurden, mustergültig veranschaulicht durch
den im November 2016 aufgedeckten
[Skandal zur Browser-Erweiterung Web of Trust (WOT)](https://www.ndr.de/nachrichten/netzwelt/Nackt-im-Netz-Millionen-Nutzer-ausgespaeht,nacktimnetz100.html):
Gehandelt wurden angeblich anonymisierte Daten, die in Wirklichkeit
vollständige Web-Verläufe umfassten. [“Informationen zu laufenden
Polizei-Ermittlungen, die Sadomaso-Vorlieben eines Richters, interne
Umsatzzahlen eines Medien-Unternehmens und Web-Recherchen zu
Krankheiten, Prostituierten und Drogen.“](https://www.ndr.de/nachrichten/netzwelt/Nackt-im-Netz-Millionen-Nutzer-ausgespaeht,nacktimnetz100.html)

Digitale Selbstverteidigung gepaart mit Verzicht ist eine gute
Option. Meine Software-Empfehlungen sind der
[Firefox mit NoScript und Werbe-Blocker][noscript], alternativ der
[Firefox als Tor Browser mit NoScript][tor] und [GnuPG][gnupg].

{% include labels.md %}
