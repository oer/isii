# Source: https://github.com/patrickwelker/jekyll-strip/blob/333794ca30381e1d8b88111176d2a113703902e0/strip.rb
# The MIT License (MIT), Copyright (c) 2012 Aucor Oy

# Replaces multiple newlines and whitespace
# between them with one newline

module Jekyll
  class StripTag < Liquid::Block

    def render(context)
      super.gsub /\n\s*\n/, "\n"
    end

  end
end

Liquid::Template.register_tag('strip', Jekyll::StripTag)
