---
layout: page
sequence: 0
title: Informationelle Selbstbestimmung für mehr Privatsphäre im Internet mit Firefox, NoScript, Werbe-Blockern, Tor und GnuPG/PGP
toctitle: Einführung
description: Aufklärung zur informationellen Selbstbestimmung im Internet mit freier Software.
keywords: Informationelle Selbstbestimmung, Privatsphäre, Web, Surfen, Browser, Internet, Selbstdatenschutz, Datenschutz, digitale Selbstverteidigung, anonym, Anonymität, Firefox, Werbe-Blocker, Ad-Blocker, Adblock, uBlock, Tor, TorBrowser, OpenPGP, PGP, GPG, GnuPG, Cache, Cookie, Web-Bug, IP-Adresse, Vorratsspeicherung, Vorratsdatenspeicherung, Profil, Fingerprinting, DNS
Time-stamp: "2019-07-24 16:44:40"
permalink: /index.html
---

![Themen der Web-Seite im November 2015 als Wortwolke. Erzeugt mit
Fotowall.](./public/images/wortwolke-2015.png){:width="600" height="603"}

Das Internet beherbergt wunderbare Dienste und Orte.  Beispielsweise
können wir in Echtzeit mit Menschen an allen Enden der Welt
kommunizieren.  Wenn wir das richtig machen, bleiben die Inhalte unserer
Kommunikation *privat* (Details zur
[E-Mail-Verschlüsselung mit GnuPG][gnupg] in einem
späteren Abschnitt).  Wenn wir das noch besser machen, entsprechen die
Sicherheitseigenschaften der Kommunikation sogar denjenigen des
*Vier-Augen-Gesprächs* (Details zum
[Off-The-Record-Messaging][otr] in einem
späteren Abschnitt).  Wir können auch überall auf das Wissen der Welt
zugreifen, etwa auf zufällig angesteuerten Web-Seiten von unbekannter
Qualität (wie dieser hier) oder in Form des durch Spenden finanzierten
Enzyklopädie-Projekts
[Wikipedia](https://de.wikipedia.org/wiki/Wikipedia:Hauptseite) (dank
der freien Software [Kiwix](https://wiki.kiwix.org/wiki/Main_Page/de)
auch offline), dessen Artikel von Freiwilligen geschrieben und
verbessert werden.  Wenn wir das richtig machen, bleiben wir dabei
*anonym* und sind *vor Zensur geschützt*
(Details zum [anonymen Surfen mit Tor im Internet][tor] in einem späteren
Abschnitt).  Schließlich können wir zahlreiche Geräte über das Internet
vernetzen, was unseren Spieltrieb befriedigt und Komfort erhöht (etwa
die per Smartphone gesteuerte und zum Anlass passende Beleuchtung, die aus der
Ferne abrufbaren Bilder der Überwachungskamera, der als „Home Guard“
für uns arbeitende Staubsaugerroboter, die aus der Ferne gewartete
Heizungsanlage, das per WLAN zu öffnende Türschloss, sprachgesteuerte
Assistenten in allen Lebenslagen oder das autonome, Staus und Stress
vermeidende Auto). Ich sagte es bereits: Das Internet beherbergt
wunderbare Dienste und Orte.

Das Internet ist aber auch feindliches Gelände.  Weite Teile sind
rechtsfreie Räume, andere sind Kriegsgebiet, Software wird schlampig
programmiert, Geräte werden unsicher konfiguriert und von Dritten
kontrolliert, nur in seltenen Fällen werden unsere Rechte respektiert.
Die bereits erwähnte
[Beleuchtung kann von Spaßvögeln per Drohne im Vorbeiflug gesteuert und von Kriminellen zerstört](https://www.heise.de/security/meldung/Licht-an-Licht-aus-ZigBee-Wurm-befaellt-smarte-Gluehbirnen-3459004.html),
auf die [Kamera kann dank einer NSA-Hintertür mit Wissen des BND](https://www.zeit.de/digital/datenschutz/2016-09/videoueberwachung-nsa-bnd-frankfurt-flughafen)
oder dank schlampiger Programmierung
(z. B. bei [Samsung 2017](https://www.heise.de/newsticker/meldung/Samsung-SmartCam-Kameras-sind-Freiwild-fuer-Botnetz-Betreiber-3603201.html),
[NeoCoolCam 2017](https://www.heise.de/security/meldung/NeoCoolCam-Chinesische-IP-Kameras-mit-massiven-Sicherheitsluecken-3788061.html)
oder [Xiongmai 2018](https://www.heise.de/security/meldung/Offen-wie-ein-Scheunentor-Millionen-Ueberwachungskameras-im-Netz-angreifbar-4184521.html))
auch von Dritten zugegriffen werden, ebenso wie auf die
[Kamera des LG-Home-Guard-Staubsaugerroboters 2017](https://www.heise.de/security/meldung/HomeHack-Angriff-macht-aus-smarten-Staubsaugern-Spionage-Tools-3873782.html);
die [Heizung kann bei Minusgraden von Dritten ausgeschaltet werden](https://www.heise.de/security/meldung/Finnland-DDoS-Attacke-auf-Heizungssteuerung-3459730.html),
das Türschloss [bleibt nach einem Firmware-Update dauerhaft blockiert (2017)](https://www.heise.de/newsticker/meldung/Fehlerhaftes-Firmware-Update-legt-smarte-Tuerschloesser-lahm-3798706.html)
oder [öffnet sich auch für Hacker (2019)](https://www.heise.de/security/meldung/Smart-Home-Hubs-von-Zipato-laden-Hacker-foermlich-ein-4462032.html)
und das [selbstfahrende Auto kann aus der Ferne in den Graben gelenkt](https://www.heise.de/security/meldung/Hacker-steuern-Jeep-Cherokee-fern-2756331.html)
werden.  Die in allen Lebenslagen helfenden Assistenten erweisen sich
auch als Spione in allen Lebenslagen, von
[Bettgesprächen (Google, 2019)](https://netzpolitik.org/2019/googles-assistenzwanze-auch-bettgespraeche-werden-von-menschen-ausgewertet/)
bis zu
[Bankverbindungen und Äußerungen von Kindern (Alexa, 2019)](https://www.heise.de/newsticker/meldung/Amazon-Mitarbeiter-tippen-zum-Teil-Alexa-Sprachbefehle-ab-4374871.html),
was der [wissenschaftliche Dienst des Bundestages in 2019 für riskant hält](https://www.heise.de/newsticker/meldung/Gutachter-des-Bundestags-sehen-bei-Alexa-Risiken-fuer-Besucher-und-Kinder-4465748.html),
während [unsere Bundesregierung weitere Überwachungsmöglichkeiten zu schätzen weiß](https://netzpolitik.org/2019/bundesregierung-polizei-darf-auf-daten-von-alexa-und-co-zugreifen/).

Zahlreiche Web-Seiten liefern, ohne dies selbst zu wissen,
zusammen mit ihrer Werbung unbemerkt Schad-Software aus, die Schwachstellen im
Browser ausnutzt, um die
Kontrolle über unsere Geräte zu übernehmen (Details zu diesen als
[Drive-By-Download][drive-by-download]
und [Malvertising][malvertising]
bezeichneten Techniken in einem späteren Abschnitt).  Dass ich hier
nicht übertreibe, belegen beispielsweise die State of the Web Reports
von Menlo Security:
[2016 stellte etwa die Hälfte der Top-1-Mio-Web-Seiten ein Sicherheitsrisiko dar](https://www.heise.de/ix/meldung/State-of-the-Web-2016-Jede-zweite-Website-ist-ein-Sicherheitsrisiko-3569114.html);
in [2017 galt dies für 42% der 100.000 meistbesuchten Web-Seiten](https://info.menlosecurity.com/state-of-the-web-2017.html).
Als sei dies alles noch nicht schlimm genug, werden wir ohne
weitreichende Vorsichtsmaßnahmen natürlich bei jeder Aktion im Internet
und jeder Interaktion mit „smarten“ Geräten überwacht – sei es Klick,
Touch, Schritt oder Zuruf.

Wenn Sie sich in diesem feindlichen Gelände bewegen wollen, rate ich
Ihnen dringend, Techniken der *digitalen Selbstverteidigung* zu erlernen
und anzuwenden.  Sie sollten nicht darauf hoffen, dass andere Ihnen
helfen werden, jedenfalls ganz bestimmt weder staatliche Stellen noch
die im Überwachungskapitalismus verwurzelten Betreiber „kostenloser“
Dienste (Details zum [Überwachungskapitalismus][ueberwachungskapitalismus]
in einem späteren Abschnitt).  Hilfe von staatlichen Stellen ist so lange
nicht zu erwarten, wie Schwachstellen in unseren Geräten von diesen
missbraucht werden, um uns anzugreifen (schauen Sie nochmal auf das
Ausmaß
[staatlicher Massenüberwachung in globalem Stil](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re)
und die Details der
[Online-Durchsuchung mit Trojanern in Deutschland](https://de.wikipedia.org/wiki/Online-Durchsuchung_(Deutschland))).
Statt Schwachstellen zu missbrauchen, wäre staatliche Hilfe
angebracht, damit derartige Schwachstellen nicht länger von gewöhnlichen
Kriminellen ausgenutzt werden, die es auf unser Geld, unsere Daten und
unsere Rechenleistung abgesehen haben.  Wenn Sie Sicherheit, Privatsphäre
oder Ihr Recht auf informationelle Selbstbestimmung im Internet für
erstrebenswert halten, ist digitale Selbstverteidigung notwendig und
möglich, sie ist alternativlos.

Auf dem Papier besitzen wir alle sogenannte
[*Grund-*](https://de.wikipedia.org/wiki/Grundrecht)
und
[*Menschenrechte*](https://de.wikipedia.org/wiki/Menschenrecht),
die uns
[*Privatsphäre*](https://de.wikipedia.org/wiki/Privatsph%C3%A4re)
zusichern, etwa [Brief-](https://de.wikipedia.org/wiki/Briefgeheimnis),
[Post-](https://de.wikipedia.org/wiki/Postgeheimnis) und
[Fernmeldegeheimnis](https://de.wikipedia.org/wiki/Fernmeldegeheimnis)
oder Grundrechte auf
[*informationelle Selbstbestimmung*](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung)
und [Gewährleistung der Vertraulichkeit und Integrität informationstechnischer Systeme](https://de.wikipedia.org/wiki/Grundrecht_auf_Gew%C3%A4hrleistung_der_Vertraulichkeit_und_Integrit%C3%A4t_informationstechnischer_Systeme).
Wir sollten unbeobachtet recherchieren, uns unbeobachtet Meinungen
bilden sowie uns unbeobachtet austauschen können, jedenfalls solange es
keinen begründeten Anfangsverdacht gibt, der Überwachung im Einzelfall
rechtfertigen könnte.

In der Praxis im Internet und in Bezug auf elektronische Kommunikation
sind diese Rechte allerdings das Papier nicht wert, auf dem sie stehen.
Wir werden kontinuierlich bei allem überwacht, was wir machen, und zwar
in der Regel mehrfach: Von in- und ausländischen Geheimdiensten, von
Werbenetzen und anderen Datenkraken sowie von den meisten Betreibern
„kostenloser“ Internet- und Kommunikationsdienste (E-Mail, Chat,
Messenger, soziale Netzwerke, SMS, Telefonate, Videotelefonie).  Zudem
unterwandern in- und ausländische staatliche Stellen und andere
Kriminelle digitale Infrastrukturen von den Schaltstellen des Netzes bis
zu angeschlossenen („smarten“) Geräten.  Wer heute glaubt, dass die in
Grund- und Menschenrechten verankerte Privatsphäre oder die Kontrolle
über die am Körper getragenen oder in den eigenen vier Wänden
eingesetzten Geräte ein schützenswertes Gut ist, muss selbst aktiv
werden und Techniken der digitalen Selbstverteidigung erlernen und
anwenden.

Das Grundrecht auf
[informationelle Selbstbestimmung](https://de.wikipedia.org/wiki/Informationelle_Selbstbestimmung)
erlaubt [Alice und Bob](https://de.wikipedia.org/wiki/Alice_und_Bob)
(dies sind beliebte Namen für Sie und mich, wenn es um
Internet-Kommunikation geht, insbesondere unter Verwendung
kryptographischer Verfahren), *selbst* zu
bestimmen, welche personenbezogenen Daten sie zu welchen
*Zwecken* weitergeben.  Es steht Alice und Bob
natürlich frei, auf dieses Recht zu verzichten, etwa wenn sie im Tausch
für fragwürdige „kostenlose“ Dienste unverständlichen ~~Datenschutz-~~
Datenschatz- und Nutzungsbedingungen zustimmen, ohne diese gelesen oder
verstanden zu haben.  Alice und Bob haben sich allerdings bewusst
entschieden, auf diesem Recht und der daraus resultierenden Privatsphäre
in Relation zum Netz zu bestehen.
Im [Volkszählungsurteil des Bundesverfassungsgerichts aus dem Jahre 1983](https://de.wikipedia.org/wiki/Volksz%C3%A4hlungsurteil) haben
beide nachgelesen, dass und warum informationelle Selbstbestimmung
wichtig ist:

> „Mit dem Recht auf informationelle Selbstbestimmung wären eine
> Gesellschaftsordnung und eine diese ermöglichende Rechtsordnung nicht
> vereinbar, in der Bürger nicht mehr wissen können, wer was wann und
> bei welcher Gelegenheit über sie weiß.  Wer unsicher ist, ob
> abweichende Verhaltensweisen jederzeit notiert und als Information
> dauerhaft gespeichert, verwendet oder weitergegeben werden, wird
> versuchen, nicht durch solche Verhaltensweisen aufzufallen.  \[…\] Dies
> würde nicht nur die individuellen Entfaltungschancen des Einzelnen
> beeinträchtigen, sondern auch das Gemeinwohl, weil Selbstbestimmung
> eine elementare Funktionsbedingung eines auf Handlungsfähigkeit und
> Mitwirkungsfähigkeit seiner Bürger begründeten freiheitlichen
> demokratischen Gemeinwesens ist.“

Die Aufgabe des
[Datenschutzes](https://de.wikipedia.org/wiki/Datenschutz) ist der
Schutz des Rechts auf informationelle Selbstbestimmung; bei
Datenschutz geht es demnach nicht um so etwas vermeintlich langweiliges
wie den Schutz von Daten (das ist Aufgabe der faszinierenden
[Informationssicherheit](https://de.wikipedia.org/wiki/Informationssicherheit)),
sondern um den *Schutz von Menschen* vor dem Missbrauch sie betreffender
Daten.  Zum
[Selbstdatenschutz](https://de.wikipedia.org/wiki/Selbstdatenschutz) im
Internet zählen Maßnahmen, die Alice und Bob einsetzen, um ihre
Privatsphäre im Internet durch digitale Selbstverteidigung und bewussten
*Verzicht* zu schützen, wozu ich auf dieser
Web-Seite zahlreiche Erläuterungen und Empfehlungen gebe.

Als Vorgriff auf folgende Abschnitte sei kurz angerissen, dass es darum
geht, sich angesichts der Datenschatzbedingungen diverser Geräte
(insbesondere „smarter“ – von Smartphones über Smart TVs zu Smart
Homes, von Smart Cities bis zum Smart Planet)
und Softwareanbieter und -lösungen bewusst für akzeptable Alternativen
zu entscheiden, auch wenn sie (noch?) wenig populär sein mögen und einen
höheren Aufwand mit sich bringen.  Das Projekt
[Terms of Service; Didn't Read](https://tosdr.org/)
veranschaulicht exemplarische Nutzungsbedingungen.  Das Projekt
[PRISM-Break](https://prism-break.org/de/) listet auf freier Software
basierende Alternativen zu beliebten Datenkraken auf.  Wählen Sie
bewusst!  Machen Sie sich klar, dass wir Kontrolle über unsere Geräte nur
durch den Einsatz
[freier Software](https://de.wikipedia.org/wiki/Freie_Software)
erzielen können (Details zu [freier Software][freie-software] in einem
späteren Abschnitt).

Mit dem Erfolg von Smartphones und anderen „smarten“ Geräten hinterlässt
unser Leben mehr und mehr Spuren im Internet, und unser Leben wird durch
Impulse aus dem Internet immer weitgehender gesteuert, worauf ich in
separaten Abschnitten ausführlicher eingehe.  Diese Durchdringung von
Cyberspace und physischer Welt hat zu einer dramatischen Erosion unserer
Privatsphäre geführt; die Wirtschaftswissenschaftlerin Shoshana Zuboff
sieht uns im Zeitalter des
[Überwachungskapitalismus](https://www.faz.net/aktuell/feuilleton/debatten/die-digital-debatte/shoshana-zuboff-googles-ueberwachungskapitalismus-14101816.html),
auf den ich [später zurückkommen werde][ueberwachungskapitalismus].

Seit dem Sommer 2013 haben wir dank
[Edward Snowden zahlreiche Details über das Ausmaß staatlicher Massenüberwachung und Spionage](https://de.wikipedia.org/wiki/Globale_%C3%9Cberwachungs-_und_Spionageaff%C3%A4re)
erfahren, und wir haben gelernt, dass diese Massenüberwachung von der
freiwilligen Preisgabe von Daten und Lebensumständen an
werbewirtschaftlich orientierte, typischerweise in den USA beheimatete
Firmen profitiert.
Mit [„encryption works“](https://www.theguardian.com/world/2013/jun/17/edward-snowden-nsa-files-whistleblower)
(„Verschlüsselung funktioniert“, mehr dazu
[im folgenden Abschnitt][resignation]) gibt es allerdings eine gute
Nachricht von Edward Snowden: Wer möchte, kann sich mit technischen
Mitteln wehren.  Digitale Selbstverteidigung ist notwendig und möglich,
sie ist alternativlos.

Alice und Bob lassen sich nicht wehrlos überwachen, sondern verteidigen
ihr Grundrecht auf informationelle Selbstbestimmung im Internet unter
anderem durch Verzicht auf verlockende Dienste von Datenkraken, durch
technische Maßnahmen zur digitalen Selbstverteidigung wie spezielle
Browser-Konfigurationen, insbesondere Ad-Blocker zum Schutz vor
Malvertising und Tracking, Verschlüsselungstechniken und die Verwendung
von Anonymisierungsdiensten zum anonymen Surfen, wie ich auf diesen
Seiten darstelle.

Im Folgenden beginne ich mit einer
[Diskussion des faulen Arguments][resignation], Privatsphäre im Internet sei
angesichts von Datenkraken und Massenüberwachung nicht möglich.  Dieses
Argument fördert Bequemlichkeit, Resignation und Unwissenheit, aber
nichts davon scheint mir angebracht.  Stattdessen versuche ich mich als
Aufklärer im Sinne Kants und plädiere für Mündigkeit im Internet:
„[Aufklärung ist der Ausgang des Menschen aus seiner selbst verschuldeten Unmündigkeit.](https://de.wikisource.org/wiki/Beantwortung_der_Frage:_Was_ist_Aufkl%C3%A4rung%3F)“

In diesem Sinne gebe ich Anregungen für das mündige Handeln im Internet,
bevor es in den folgenden Abschnitten technischer wird.  Ausgehend von
[Überwachungsmöglichkeiten und Identifikationsmerkmalen im Internet][grundlagen]
und der [Visualisierung der Web-Verstrickungen][collusion] mit einer
Firefox-Erweiterung sowie [resultierenden Risiken][jaund] erläutere ich
[wirksame Techniken der digitalen Selbstverteidigung][selbstverteidigung].
Es geht um generelle Techniken zur Grundsicherung des eigenen PCs
sowie Grundlagen asymmetrischer Verschlüsselung im Web (`https`), bevor
[Tor][tor] als Projekt zur Anonymität im Internet und [GnuPG][gnupg]
für die E-Mail-Verschlüsselung im Fokus stehen.

Ich wünsche neue Erkenntnisse und freue mich auf
[Feedback][impressum]!

P.S.  Sie können diese Seiten auch offline lesen:
[„Informationelle Selbstbestimmung im Internet“ vom {{ site.datum }} als ca. 1,6MB große zim-Datei.](/selbstbestimmung.zim)
(Sie können zim-Dateien mit dem
[freien Offline-Reader Kiwix](https://wiki.kiwix.org/wiki/Main_Page/de)
lesen.  Auf meinem Handy befinden sich die deutsche und die englische
Wikipedia sowie sämtliche Wikibooks:
[Offline-Wikipedia in allen Sprachen](https://wiki.kiwix.org/wiki/Content_in_all_languages))

P.P.S.  Die erste Version dieser Web-Seite habe ich im Frühjahr 2006
erstellt, nachdem die
[EU-Bestrebungen](https://de.wikipedia.org/wiki/Richtlinie_2006/24/EG_%C3%BCber_die_Vorratsspeicherung_von_Daten)
zur
[Vorratsdatenspeicherung](http://www.vorratsdatenspeicherung.de/index.php?lang=de)
mein Interesse an Anonymisierungstechniken, insbesondere zum anonymen
Surfen geweckt hatten.  Grob zusammengefasst geht es bei der
Vorratsdatenspeicherung darum, die Kommunikation aller EU-Bürger ohne
jeglichen Verdacht zu überwachen: Wer telefoniert wann wo mit wem wie
lange, wer schickt wem wann wo E-Mails, wer ist wann wo mit welcher
IP-Adresse im Internet unterwegs? Alice, Bob und ich waren im Jahre 2005
sicher, dass das Europäische Parlament die Richtlinie zur
Vorratsdatenspeicherung nie verabschieden würde, da sie so gar nicht zu
unserem Verständnis einer demokratischen europäischen Gesellschaft
passte.  Oder der Unschuldsvermutung.  Seitdem ist viel geschehen, was
kontinuierliche Anpassungen dieser Web-Seite zur Folge hatte und hat.

Ende 2005 wurde diese unsägliche Richtlinie vom Europäischen Parlament
verabschiedet, Ende 2007 wurde sie in Deutschland per „Gesetz zur
Neuregelung der Telekommunikationsüberwachung und anderer verdeckter
Ermittlungsmaßnahmen sowie zur Umsetzung der Richtlinie 2006/24/EG“
umgesetzt, 2010 wurde dieses Gesetz vom Bundesverfassungsgericht für
verfassungswidrig erklärt.  Zudem hat sich mehrfach gezeigt, dass die
Vorratsdatenspeicherung für die Strafverfolgung nicht hilfreich ist,
etwa in einer
[Bundestagsanalyse im Jahre 2011](https://www.heise.de/newsticker/meldung/Bundestagsanalyse-Vorratsdatenspeicherung-hilft-Ermittlern-nicht-wirklich-1223876.html)
und einer [wissenschaftlichen Studie im Jahre 2012](https://www.heise.de/newsticker/meldung/Studie-Vorratsdatenspeicherung-verbessert-die-Aufklaerungsquote-nicht-1423035.html).
Zwischen 2012 und 2014 beschäftigte sich
[der Europäische Gerichtshof mit der Frage, ob die EU-Richtlinie zur Vorratsspeicherung gegen die EU-Grundrechtecharta oder gegen die Europäische Menschenrechtskonvention verstößt](https://www.heise.de/newsticker/meldung/Schlagabtausch-zur-Vorratsdatenspeicherung-vor-dem-EuGH-1914223.html).
Am 8. April 2014 hat der Gerichtshof diese Frage
[beantwortet](https://www.heise.de/newsticker/meldung/EuGH-Regeln-zur-Vorratsdatenspeicherung-verstossen-gegen-EU-Recht-2165604.html):
Die EU-Richtlinie ist ungültig, weil sie unsere Grundrechte auf Achtung
des Privatlebens und auf den Schutz personenbezogener Daten verletzt.

<a href="https://aktion.digitalcourage.de/weg-mit-vds" class="nooutlink"><img src="./public/logos/Digitalcourage_Weg_mit_VDS.jpg" alt="Verfassungsbeschwerde von Digitalcourage e.V. (CC BY-SA 3.0 Digitalcourage e.V.)" class="logo" width="278" height="204"></a>
Nach dem
[Terroranschlag auf Charlie Hebdo im Januar 2015](https://de.wikipedia.org/wiki/Anschlag_auf_Charlie_Hebdo) wurde
die Vorratsdatenspeicherung vor allem von konservativen und
„Sicherheits-“ Politikern immer wieder ins Gespräch gebracht, und im
Oktober 2015 wurde sie in Deutschland per Gesetz vom
[Bundestag wieder eingeführt](https://www.heise.de/newsticker/meldung/Bundestag-fuehrt-Vorratsdatenspeicherung-wieder-ein-2849174.html),
was durch verschiedene Gruppen mit Klagen vor dem Verfassungsgericht
angefochten wird.  Unterstützen Sie bitte die
[Verfassungsbeschwerde des Vereins Digitalcourage e.V.](https://aktion.digitalcourage.de/weg-mit-vds),
die am 28. November 2016 eingereicht wurde!

Nachdem der Europäische Gerichtshof (EuGH) wie oben erwähnt im Jahre
2014 zunächst die EU-Richtlinie zur Vorratsdatenspeicherung gekippt
hatte, musste er sich nachfolgend mit verschiedenen nationalen Varianten
der Vorratsdatenspeicherung befassen und entschied im Dezember 2016,
dass die [anlasslose Vorratsspeicherung von Verkehrs- und Standortdaten nicht mit unseren Grundrechten vereinbar ist](https://www.zeit.de/digital/datenschutz/2016-12/europaeischer-gerichtshof-vorratsdatenspeicherung-urteil).
Welche Auswirkungen dieses Urteil auf vorgeblich gesetzlich legitimierte
Grundrechtsverletzungen in Deutschland hat, bleibt abzuwarten.

Es lohnt sich, kurz nachzudenken: In Frankreich gab und gibt es die
Vorratsdatenspeicherung.  Trotzdem konnte der Anschlag auf Charlie Hebdo
nicht verhindert werden.  Besonders erschreckend ist, dass die
Terroristen verschiedenen Sicherheitsbehörden weltweit, auch in
Frankreich und Deutschland, bekannt waren.  Sie wurden in Frankreich eine
Weile überwacht, was jedoch aus
[Personalmangel](https://www.nytimes.com/2015/01/10/world/europe/french-government-questions-intelligence-lapses.html)
aufgegeben wurde.  Dies ist kein Einzelfall.  Auch bei den
[Terroranschlägen am 13. November 2015 in Paris](https://de.wikipedia.org/wiki/Terroranschl%C3%A4ge_am_13._November_2015_in_Paris)
waren [sieben der acht Attentäter bekannt, vom mutmaßlichen Drahtzieher gab es im Februar 2015 sogar ein Zeitungsinterview, in dem er mit der Vorbereitung von Anschlägen prahlt.](https://www.spiegel.de/netzwelt/web/sascha-lobo-ueber-die-irrationale-ausweitung-der-ueberwachung-a-1064508.html)
Gleiches gilt für den
[Anschlag auf den Weihnachtsmarkt 2016 in Berlin](https://de.wikipedia.org/wiki/Anschlag_auf_den_Berliner_Weihnachtsmarkt_an_der_Ged%C3%A4chtniskirche).
Wie ich in einem späteren Abschnitt aufgreifen werde, sind
[Terroristen regelmäßig einschlägig im Vorfeld bekannt][terror].

Ich verstehe nicht, wie jemand nach Vorratsdatenspeicherung rufen kann,
um Heuhaufen von Kommunikationsdaten aufzuwerfen, wenn die Nadeln ohne
die künstlichen Heuhaufen klar zu sehen sind.  (Data Mining auf diesem
Heuhaufen ist aufgrund der
[Base Rate Fallacy](https://www.schneier.com/blog/archives/2006/07/terrorists_data.html)
aussichtslos.)

Na ja, vermutlich verstehe ich das doch.  Da Vorratsdatenspeicherung und
Massenüberwachung sinnlos sind, was Terrorabwehr betrifft, geht es in
Wirklichkeit um andere Ziele, die keine derart gravierenden Eingriffe in
unsere Grundrechte rechtfertigen oder die unseren Grundrechten sogar
widersprechen und die daher nicht offen ausgesprochen werden.
Terrorbekämpfung ist einfach eine beliebte rhetorische Keule, deren
beharrliches Einhämmern uns so weit abstumpfen soll, dass wir
anlasslose, flächendeckende Überwachung als unverzichtbares Instrument
akzeptieren und Verschlüsselung, Anonymisierungstechniken und anonymes
Surfen als Werkzeuge des Bösen missverstehen.  Wenn Terrorismus als
Argument nicht ausreicht, können natürlich auch die anderen drei der
[vier Reiter der Infokalypse](https://en.wikipedia.org/wiki/Four_Horsemen_of_the_Infocalypse)
zur Begründung weiterer Grundrechtseinschnitte herhalten (neben
Terroristen sind das Pädophile, Drogenhändler und Geldwäscher).

Im Gegensatz zur Vorratsdatenspeicherung erscheinen mir gezielte
Überwachungen sowie Hausdurchsuchungen bei Anfangsverdacht um ein
Vielfaches sinnvoller als anlasslose Überwachungen gesamter
Bevölkerungen.  Sie würden auch nicht unsere Grundrechte verletzen (laut
[Artikel 10 unseres Grundgesetzes](https://de.wikipedia.org/wiki/Artikel_10_des_Grundgesetzes_f%C3%BCr_die_Bundesrepublik_Deutschland)
gibt es in Deutschland ein unverletzliches Brief-, Post- und
Fernmeldegeheimnis; ebenso garantiert
[Artikel 8 der Europäischen Menschenrechtskonvention](https://de.wikipedia.org/wiki/Europ%C3%A4ische_Menschenrechtskonvention#Artikel_8_.E2.80.93_Recht_auf_Achtung_des_Privat-_und_Familienlebens)
die Achtung des Privat- und Familienlebens, von Wohnung und
Korrespondenz).

{% include labels.md %}
