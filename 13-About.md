---
layout: page
sequence: 13
title: Über dieses Dokument
toctitle: Über dieses Dokument
description: Zu Technik und Quellen dieser Webseite
keywords: Freie Software, Emacs, Jekyll, Lizenzen
Time-stamp: "2019-07-24 20:39:36"
permalink: /About.html
---

<a href="https://fsfe.org/about/basics/freesoftware.de.html" class="nooutlink"><img src="./public/logos/created-with-free-software.png" alt="Created with free software" class="logo"></a>
Dieses Dokument wurde mit
[freier Software][freie-software] erstellt,
unter [GNU/Linux](https://www.gnu.org/gnu/linux-and-gnu.de.html) im
[GNU Emacs](https://www.gnu.org/software/emacs/).
Bis 2016 habe ich den HTML-Code mit
[LaTeX2HTML](https://www.latex2html.org/) generiert,
seit 2017 nutze ich
[Jekyll](https://jekyllrb.com/), und zwar mit einer leicht abgewandelten
[Lanyon](https://github.com/poole/lanyon)-Variante.
<br clear="left" />

----------------------------------------------------------------------

Die auf diesen Seiten verwendeten Icons stammen aus bzw. basieren auf
der [Aqua Fusion Icons Public Beta](https://store.kde.org/content/show.php?content=4815) und unterliegen
[dieser Lizenz](./public/icons/LICENSE). Die Icons und Lizenzbestimmungen
finden sich in [diesem Tar-Archiv](./public/icons.tbz).
<br clear="left" />

----------------------------------------------------------------------

<a href="https://www.gnupg.org/" class="nooutlink"><img src="./public/logos/gnupg.png" alt="GnuPG Logo" class="logo"></a>
Das GnuPG-Logo ist
eine verkleinerte Version des Logos im GnuPG-Quelltext unter der
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.de.html), (C) Free
Software Foundation.
<br clear="left" />

----------------------------------------------------------------------

<a href="https://www.mozilla.org/de/firefox/new/" class="nooutlink"><img src="./public/logos/firefox.png" alt="Firefox Logo" class="logo"></a>
Das Firefox-Logo entstammt
[der Firefox-Branding-Seite](https://www.mozilla.org/en-US/styleguide/identity/firefox/branding/)
und unterliegt der [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
<br clear="left" />

----------------------------------------------------------------------

<a href="https://www.anonym-surfen.de/software.html" class="nooutlink"><img src="./public/logos/JonDo.png" alt="JonDo Logo" class="logo"></a>
Das JonDo-Logo entstammt
[dem JAP-Quelltext](https://svn.jondos.de/svn/Jap/Jap/trunk/).
<br clear="left" />

----------------------------------------------------------------------

<a href="https://www.torproject.org/" class="nooutlink"><img src="./public/logos/tor-logo.jpg" alt="Tor Logo" class="logo"></a>
Das Tor-Logo darf nur zu
[nicht-kommerziellen Zwecken verwendet werden](https://www.torproject.org/docs/trademark-faq.html).
<br clear="left" />

----------------------------------------------------------------------

<a href="https://aktion.digitalcourage.de/weg-mit-vds" class="nooutlink"><img src="./public/logos/Digitalcourage_Weg_mit_VDS.jpg" alt="Verfassungsbeschwerde bei Digitalcourage e.V. unterzeichnen (CC BY-SA 3.0 Digitalcourage e.V.)" class="logo"></a>
Das Logo zur Verfassungsbeschwerde von Digitalcourage e.V. unterliegt
der [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
<br clear="left" />

----------------------------------------------------------------------

<a href="https://creativecommons.org/licenses/by-sa/4.0/" class="nooutlink"><img src="./public/icons/cc-by-sa-88x31.png" alt="CC BY-SA Icon" class="logo"></a>
Der Text „{{site.longtitle}}“ von
[Jens Lechtenbörger](https://www.informationelle-selbstbestimmung-im-internet.de/)
steht als
[freie und offene Bildungsressource (OER)](https://de.wikipedia.org/wiki/Open_Educational_Resources)
unter der Lizenz
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
Die Quelldateien sind in diesem
[GitLab-Repository](https://gitlab.com/oer/isii)
verfügbar.

{% include labels.md %}
