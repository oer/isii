---
layout: page
sequence: 9
title: 'De-Mail ist Quatsch'
toctitle: 'De-Mail ist Quatsch'
description: Ich rate von De-Mail ab.
keywords: De-Mail, Quatsch, Anfängerfehler
Time-stamp: "2018-02-18 17:28:35"
permalink: /De-Mail_ist_Quatsch.html
---

Im Rahmen meiner [Vorlesung „Computer
Networks“](https://www.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=2031)
wollte ich im November 2010 [De-Mail](http://www.de-mail.de/) als
deutschen Ansatz zur Absicherung von E-Mail vorstellen. Daher habe ich
mir das Dokument BSI TR 01201 Teil 3.1 in der damals aktuellen Version:
0.99.1 angesehen, das unter anderem die De-Mail zugrunde liegenden
Sicherheitsmechanismen darstellt, und ich kann mich den schon länger
existierenden Boykott-Aufrufen ([etwa vom Arbeitskreis
Vorratsdatenspeicherung](http://www.vorratsdatenspeicherung.de/images/Stellungnahme_Datenschutz_und_Datensicherheit_im_Internet.pdf))
nur anschließen. Die wissenschaftliche Darstellung der folgenden
Ausführungen ist unter dem Titel [„Zur Sicherheit von De-Mail“ in der
Zeitschrift Datenschutz und
Datensicherheit](https://www.springerprofessional.de/datenschutz-und-datensicherheit-dud-4-2011/5900570)
erschienen.

Die offizielle
[De-Mail-Seite](http://www.cio.bund.de/DE/Innovative-Vorhaben/De-Mail/de_mail_node.html)
(weitergeleitet von [http://www.de-mail.de/](http://www.de-mail.de/))
versprach am 30.10.2010:

> „De-Mail wird das rechtsverbindliche und vertrauliche Versenden von
> Dokumenten und Nachrichten über das Internet ermöglichen.“

Mittlerweile hat sich die Seite mehrfach geändert. Am 11.9.2011 wurde
„rechtsverbindliche“ durch „verbindliche“ ersetzt:

> „De-Mail wird das verbindliche und vertrauliche Versenden von
> Dokumenten und Nachrichten über das Internet ermöglichen.“

„Rechtsverbindlich“ oder „verbindlich“ wären sicherlich nützliche
Eigenschaften, aber bei den im Folgenden angegebenen, offensichtlichen
Entwurfsschwächen von De-Mail kann jede/r von uns nur hoffen, nicht
beweisen zu müssen, eine gefälschte De-Mail wirklich nicht abgeschickt
zu haben. In der Tat kommt am 8.9.2013 keiner der beiden Begriffe mehr
vor. Wie Alice und Bob sofort sehen, ist „Vertraulichkeit“ bei De-Mail
nicht gegeben. Weiterhin wird auf der Web-Seite beworben (8.9.2013):

> „Im Gegensatz zur E-Mail können bei De-Mail aber sowohl die Identität
> der Kommunikationspartner als auch der Versand und der Eingang von
> De-Mails jederzeit zweifelsfrei nachgewiesen werden. Die Inhalte einer
> De-Mail können auf ihrem Weg durch das Internet nicht mitgelesen oder
> gar verändert werden.“

Das ist leider völlig falsch.

Der Ablauf einer De-Mail-Zustellung von Alice zu Bob gestaltet sich nach
[BSI TR 01201 Teil 3.1 (Version:
1.00)](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/De_Mail/TR_De_Mail_PVD_FU.pdf)
wie folgt: Alice schreibt eine E-Mail und übergibt diese auf
abgesichertem Wege an ihren De-Mail-Provider (auf dem Weg von ihr zum
Provider kann die E-Mail von Dritten weder eingesehen noch verfälscht
werden). Der Provider (bzw. jeder Dritte mit Kontrolle über dessen
Infrastruktur) kann die Mail allerdings lesen (was bedeutet, dass keine
Vertraulichkeit gegeben ist), verändern (also ist Integrität auch nicht
garantiert) und *danach* einen Hash-Wert der
veränderten Nachricht erstellen, der beweisen soll, dass die Nachricht
von Alice kam. Erst dann gibt er die angeblich vor Modifikationen
gesicherte Nachricht auf abgesichertem Wege an den De-Mail-Provider von
Bob weiter. Auf dem Weg zwischen beiden Providern ist die Nachricht also
vor unbefugter Einsicht und Modifikation gesichert. Bobs Provider (bzw.
jeder Dritte mit Kontrolle über dessen Infrastruktur) kann die Nachricht
wieder lesen, verändern, einen neuen Hash-Wert der veränderten Nachricht
erstellen und sie dann auf gesichertem Wege an Bob ausliefern.

Wer will jetzt ernsthaft behaupten, Bob erhalte ein
„rechtsverbindliches“ Dokument von Alice?

Nochmals in aller Deutlichkeit: De-Mail verzichtet bewusst auf
sogenannte Ende-zu-Ende-Absicherung, bei der Bob wirklich sicher sein
könnte, dass die Nachricht von Alice kam und weder von Dritten
eingesehen noch verändert wurde, während das [bereits besprochene
GnuPG][gnupg] dies seit
Langem ermöglicht. Die BSI-Vorgaben erlauben immerhin, dass Alice und
Bob GnuPG-gesicherte E-Mails über De-Mail versenden – aber welchen
Vorteil liefert De-Mail dann noch? (Für mich als Privatperson keinen …)
Weiterhin ist die in Schritt 27 des Dokuments angegebene
Integritätssicherung durch Versenden der E-Mail mit ihrem Hash-Wert ein
unverzeihlicher Anfängerfehler: Der Angreifer Mallory berechnet zu der
von ihm gefälschten E-Mail natürlich einen neuen, passenden Hash-Wert.
Daher verwendet jedes ernsthafte Verfahren zur Integritätssicherung
keine reinen Hash-Werte, sondern ausgefeiltere Techniken wie
[HMACs](https://de.wikipedia.org/wiki/HMAC) oder
[digitale Signaturen](https://de.wikipedia.org/wiki/Digitale_Signatur),
die *der Absender* erstellt (und nicht etwa der Provider
nach (!) beliebiger Modifikation). Selbst unter Verwendung des höheren,
aber optionalen Schutzniveaus der „absenderbestätigten“ De-Mail-Variante
ist die Situation nur wenig besser, da der Hash-Wert in Schritt 29 zwar
durch eine digitale Signatur von Alice’ Provider geschützt wird, aber
diese Signatur stammt eben immer noch nicht von Alice. Alice kann nur
hoffen, in keinen Rechtsstreit mit ihrem Provider verwickelt zu werden …

Schließlich sei darauf hingewiesen, dass es für Privatpersonen von
zweifelhaftem Nutzen ist, wenn rechtsverbindliche Fristen mit der
Zustellung einer E-Mail in das Postfach beginnen, ohne dass sie gelesen
worden wäre. Selbst wenn Alice die De-Mail liest, darf sie, [wie der
IT-Sicherheitsverband TeleTrusT Deutschland
darstellt](https://www.teletrust.de/fileadmin/_migrated/content_uploads/2010-07-27-TeleTrusT_zu_De-Mail-Gesetzentwurf_01.pdf),
„etwaige Rechtsmittel nicht einfach durch Antwort wirksam einlegen“.
(Die weiteren, vom [vom Arbeitskreis
Vorratsdatenspeicherung](http://www.vorratsdatenspeicherung.de/images/Stellungnahme_Datenschutz_und_Datensicherheit_im_Internet.pdf)
genannten Argumente gegen De-Mail möchte ich hier gar nicht diskutieren,
etwa die Einführung einer zentralen Stelle zum Zugriff auf
De-Mail-Kommunikation, den Verzicht auf De-Mails ohne Absenderangaben
oder die Betreuung von De-Mail durch Unternehmen mit historischen
Datenskandalen.)

Also, ich möchte keine De-Mail-Adresse geschenkt bekommen. Dennoch
sollen De-Mails Geld kosten. Das wäre lächerlich, wenn es nicht
furchterregend wäre, oder?

{% include labels.md %}
